<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Penyusutan;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Tahun;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class PenyusutanReklasMasuk64Export implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nomor_lokasi;
    public $kode_kepemilikan;
    public $bidang_barang;
    public $jenis_aset;

    function __construct($args){
        $this->nomor_lokasi = $args['nomor_lokasi'];
        $this->jenis_aset = $args['jenis_aset'];
        $this->nama_lokasi = $args['nama_lokasi'];
        $this->nama_jurnal = $args['nama_jurnal'];

        $this->total_nilai_perolehan = 0;
        $this->total_akumulasi_penyusutan = 0;
        $this->total_akumulasi_penyusutan_berjalan = 0;
        $this->total_beban = 0;
        $this->total_nilai_buku = 0;

        $this->tahun_sekarang = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
    }

    public function collection() 
    {
        ini_set('max_execution_time', 1800);
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        if($this->jenis_aset == "A") {
            $data = Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->join('rincian_koreksis', 'kibs.id_aset', '=', 'rincian_koreksis.id_aset')
                        ->select('rincian_koreksis.kode_64', 'rincian_koreksis.kode_64_baru', 'rincian_koreksis.nomor_lokasi', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where('rincian_koreksis.kode_64_baru', 'not like', '1.5.4%')
                        ->where("rincian_koreksis.tahun_koreksi", '=', $tahun_laporan)
                        ->orderBy('rincian_koreksis.kode_64')
                        ->get()
                        ->toArray();
        } else {
            $data = Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->join('rincian_koreksis', 'kibs.id_aset', '=', 'rincian_koreksis.id_aset')
                        ->select('rincian_koreksis.kode_64', 'rincian_koreksis.kode_64_baru', 'rincian_koreksis.nomor_lokasi', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where('rincian_koreksis.kode_64_baru', 'like', '1.5.4%')
                        ->where("rincian_koreksis.tahun_koreksi", '=', $tahun_laporan)
                        ->orderBy('rincian_koreksis.kode_64')
                        ->get()
                        ->toArray();
        }

        $rekap_64 = array();
        // loop khusus build map 64
        foreach($data as $value) {
            $kode_64 = $value["kode_64"];
            $kode_64_baru = $value["kode_64_baru"];
            $nomor_lokasi = $value["nomor_lokasi"];

            $found = false;
            foreach($rekap_64 as $key => $as)
            {   
                if ($as["kode_64"] == $kode_64 && $as["kode_64_baru"] == $kode_64_baru) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_64)) {
                    $kode_64 = 0;
                } 

                if(is_null($kode_64_baru)) {
                    $kode_64_baru = 0;
                }

                array_push($rekap_64, array(
                    "kode_64_baru" => $kode_64_baru,
                    "kode_64" => $kode_64,
                    "nilai_perolehan" => 0,
                    "akumulasi_penyusutan" => 0,
                    "beban" => 0,
                    "akumulasi_penyusutan_berjalan" => 0,
                    "nilai_buku" => 0
                ));
            }
        }

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $this->total_nilai_perolehan += $value['nilai_pengadaan'];
            $this->total_akumulasi_penyusutan += $value['akumulasi_penyusutan'];
            $this->total_akumulasi_penyusutan_berjalan += $value['akumulasi_penyusutan_berjalan'];
            $this->total_beban += $value['beban'];
            $this->total_nilai_buku += $value['nilai_buku'];

            $kode_64 = $value["kode_64"];
            $kode_64_baru = $value["kode_64_baru"];

            foreach($rekap_64 as $key => $rekap)
            {   
                if($kode_64 != '' || !is_null($kode_64) || $kode_64 != 0) {
                    if ($rekap["kode_64"] == $kode_64 && $rekap["kode_64_baru"] == $kode_64_baru) {
                        $rekap_64[$key]["nilai_perolehan"] += $value['nilai_pengadaan'];
                        $rekap_64[$key]["akumulasi_penyusutan"] += $value['akumulasi_penyusutan'];
                        $rekap_64[$key]["beban"] += $value['beban'];
                        $rekap_64[$key]["akumulasi_penyusutan_berjalan"] += $value['akumulasi_penyusutan_berjalan'];
                        $rekap_64[$key]["nilai_buku"] += $value['nilai_buku'];
                        break;
                    }
                } else {
                    $rekap_64[$key]["nilai_perolehan"] += $value['nilai_pengadaan'];
                    $rekap_64[$key]["akumulasi_penyusutan"] += $value['akumulasi_penyusutan'];
                    $rekap_64[$key]["beban"] += $value['beban'];
                    $rekap_64[$key]["akumulasi_penyusutan_berjalan"] += $value['akumulasi_penyusutan_berjalan'];
                    $rekap_64[$key]["nilai_buku"] += $value['nilai_buku'];
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_64, 'kode_64'), SORT_ASC, $rekap_64);
        $export = collect($rekap_64);

        return $export;
    }

    public function startCell(): string
    {
        return 'B3';
    }

    public function headingRow(): int
    {
        return 3;
    }

    public function headings(): array
    {
        $headings = [
            ["KODE 64", "KODE 64 ASAL", "NILAI PEROLEHAN", "AKUMULASI PENYUSUTAN", "BEBAN", "AKUMULASI PENYUSUTAN BERJALAN", "NILAI BUKU"],
            [
                2,3,4,5,6,7,8
            ]
        ];
        return $headings;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $max = $event->sheet->getDelegate()->getHighestRow();
                ////////// set paper
                $event->sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_PORTRAIT);
                $event->sheet->getPageSetup()->setFitToWidth(1);
                $event->sheet->getPageSetup()->setFitToHeight(0);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
                $event->sheet->setShowGridlines(false);
                $event->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 4);
                $event->sheet->freezePane('I5');
                // end set paper

                /////////border heading
                $event->sheet->getStyle('A3:H3')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A4:H4')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end border heading
                // border
                $event->sheet->getStyle('A5:H'.$max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A'.($max+1).':H'.($max+1))->applyFromArray([
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end border

                // footer
                $event->sheet->getHeaderFooter()
                ->setOddFooter('&L&B '.$this->nama_jurnal.' / '. $this->nama_lokasi.' / '.$this->tahun_sekarang.' / '.$this->jenis_aset . '&R &P / &N');
                // end footer

                /////////header
                $event->sheet->getDelegate()->mergeCells('A1:H1');
                $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal . " " . $this->bidang_barang . " " . $this->nama_lokasi ." ".$this->tahun_sekarang);
                $event->sheet->getStyle('A1')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 18
                    ]
                ]);
                // end header

                ///////heading
                $event->sheet->getStyle('A3:H3')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A3:H3')->getAlignment()->setWrapText(true);
                // end heading

                //////////numbering
                $event->sheet->getDelegate()->setCellValue("A3", "No.");
                $event->sheet->getDelegate()->setCellValue("A4", "1");
                $event->sheet->getStyle('A3:A'.$max)->applyFromArray([
                    'alignment' =>[
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getColumnDimension('A')->setAutoSize(true);
                $nomor = 1;
                for($i=5;$i<=$max;$i++){
                    $event->sheet->getDelegate()->setCellValue("A".$i, $nomor);
                    $nomor++;
                }
                ///////////////end numbering

                // format text
                $event->sheet->getStyle('D4:H4')->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_TEXT ] );

                $date = date('d/m/Y');
                $baris_total = $max+1;

                $event->sheet->getDelegate()->mergeCells('A'.$baris_total.':C'.$baris_total);
                $event->sheet->getStyle('A'.$baris_total)->applyFromArray([ 'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 11
                    ]
                ]);
                $event->sheet->getDelegate()->setCellValue('A'.$baris_total, "TOTAL");
                $event->sheet->getStyle('D'.$baris_total.':H'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('D'.$baris_total.':H'.$baris_total)->applyFromArray([ 'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 11
                    ]
                ]);

                $event->sheet->getStyle('D'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('E'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('F'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('G'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('H'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getDelegate()->setCellValue('D'.$baris_total, $this->total_nilai_perolehan);
                $event->sheet->getDelegate()->setCellValue('E'.$baris_total, $this->total_akumulasi_penyusutan);
                $event->sheet->getDelegate()->setCellValue('F'.$baris_total, $this->total_beban);
                $event->sheet->getDelegate()->setCellValue('G'.$baris_total, $this->total_akumulasi_penyusutan_berjalan);
                $event->sheet->getDelegate()->setCellValue('H'.$baris_total, $this->total_nilai_buku);
                $f1 = $max+3;
                for($i = 0; $i<5; $i++) {
                    $event->sheet->getDelegate()->mergeCells('A'.$f1.':E'.$f1);
                    $event->sheet->getDelegate()->mergeCells('F'.$f1.':H'.$f1);
                    $event->sheet->getStyle('A'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);
                    $event->sheet->getStyle('F'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);

                    if($i == 0) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                        $event->sheet->getDelegate()->setCellValue('F'.$f1, "Mojokerto, ".$date);
                    }

                    if($i == 4) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                        $event->sheet->getDelegate()->setCellValue('F'.$f1, "NIP");
                    }

                    $f1++;
                }
            },
        ];
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'H' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
        ];
    }

    public function title(): string
    {
        return 'Data Penyusutan';
    }

}
