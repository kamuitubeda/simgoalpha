<?php

namespace App\Exports;

use App\Http\Controllers\API\Jurnal\PenyusutanController;
use App\Http\Controllers\API\Jurnal\Rekap_penyusutanController;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rehab;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_sub_unit;
use App\Models\Jurnal\Penyusutan;
use App\Models\Jurnal\Rekap_penyusutan;
use App\Models\Jurnal\Tahun;
use App\Models\Kamus\Sub_sub_rincian_108;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class LaporanRekapPenyusutanExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nomor_lokasi;
    public $kode_kepemilikan;
    public $bidang_barang;
    public $jenis_aset;

    function __construct($args){
        $this->nama_jurnal = $args['nama_jurnal'];

        $this->tahun_sekarang = date('Y')-1;

        $this->total_kib_a = 0;
        $this->total_kib_b = 0;
        $this->total_kib_c = 0;
        $this->total_kib_d = 0;
        $this->total_kib_e = 0;
        $this->total_kib_f = 0;
        $this->total_kib_g = 0;
        $this->total_kib_r = 0;
    }

    public function collection()
    {
        ini_set('max_execution_time', 1800);
        $rekap = array();
        $i = 0;

        $daftar_sub_unit = Kamus_sub_unit::select('nomor_sub_unit', 'nama_sub_unit')->get();
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        foreach ($daftar_sub_unit as $unit) {
            $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->select('kibs.id_aset', 'penyusutans.akumulasi_penyusutan', 'kibs.kode_108')
                ->where('kibs.nomor_lokasi', 'like', $unit['nomor_sub_unit'] . '%')
                ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan)
                ->where('kibs.saldo_barang', '>', 0)
                ->get()
                ->toArray();

            $penyusutan_kib_a = 0;
            $penyusutan_kib_b = 0;
            $penyusutan_kib_c = 0;
            $penyusutan_kib_d = 0;
            $penyusutan_kib_e = 0;
            $penyusutan_kib_f = 0;
            $penyusutan_kib_g = 0;
            $penyusutan_kib_r = 0;

            foreach ($data as $value) {
                $kode = substr($value["kode_108"], 0, 5);
                if($kode == '1.3.1') {
                    $penyusutan_kib_a += $value['akumulasi_penyusutan'];
                } else if($kode == '1.3.2') {
                    $penyusutan_kib_b += $value['akumulasi_penyusutan'];
                } else if($kode == '1.3.3') {
                    $penyusutan_kib_c += $value['akumulasi_penyusutan'];
                } else if($kode == '1.3.4') {
                    $penyusutan_kib_d += $value['akumulasi_penyusutan'];
                } else if($kode == '1.3.5') {
                    $penyusutan_kib_e += $value['akumulasi_penyusutan'];
                } else if($kode == '1.3.6') {
                    $penyusutan_kib_f += $value['akumulasi_penyusutan'];
                } else if($kode == '1.5.3') {
                    $penyusutan_kib_g += $value['akumulasi_penyusutan'];
                } else if($kode == '1.5.5') {
                    $penyusutan_kib_h += $value['akumulasi_penyusutan'];
                }
            }

            $this->total_kib_a += $penyusutan_kib_a;
            $this->total_kib_b += $penyusutan_kib_b;
            $this->total_kib_c += $penyusutan_kib_c;
            $this->total_kib_d += $penyusutan_kib_d;
            $this->total_kib_e += $penyusutan_kib_e;
            $this->total_kib_f += $penyusutan_kib_f;
            $this->total_kib_g += $penyusutan_kib_g;
            $this->total_kib_r += $penyusutan_kib_r;

            $rekap[$i++] = array(
                'nama_sub_unit' => $unit['nama_sub_unit'],
                'penyusutan_kib_a' => $penyusutan_kib_a,
                'penyusutan_kib_b' => $penyusutan_kib_b,
                'penyusutan_kib_c' => $penyusutan_kib_c,
                'penyusutan_kib_d' => $penyusutan_kib_d,
                'penyusutan_kib_e' => $penyusutan_kib_e,
                'penyusutan_kib_f' => $penyusutan_kib_f,
                'penyusutan_kib_g' => $penyusutan_kib_g,
                'penyusutan_kib_r' => $penyusutan_kib_r
            );
        }

        $export = collect($rekap);
        return $export;
    }

    public function startCell(): string
    {
        return 'B2';
    }

    public function headingRow(): int
    {
        return 2;
    }

    public function headings(): array
    {
        $heading = [
            ['SKPD', 'Penyusutan KIB A', 'Penyusutan KIB B', 'Penyusutan KIB C', 'Penyusutan KIB D', 'Penyusutan KIB E', 'Penyusutan KIB F', 'Penyusutan KIB G', 'Penyusutan KIB R'],
            [
                2,3,4,5,6,7,8,9,10
            ]
        ];

        return $heading;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $max = $event->sheet->getDelegate()->getHighestRow();
                /////set paper
                $event->sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setFitToWidth(1);
                $event->sheet->getPageSetup()->setFitToHeight(0);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
                $event->sheet->setShowGridlines(false);
                $event->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2, 3);

                $event->sheet->freezePane('K4');

                // end set paper

                // footer
                $event->sheet->getHeaderFooter()
                    ->setOddFooter('&L&B '. $this->nama_jurnal.' / '.$this->tahun_sekarang. '&R &P / &N');
                // end footer

                ////////////////Border
                $event->sheet->getStyle('A2:J2')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A3:J3')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A4:J'.$max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                //////////////endborder

                // format text
                $event->sheet->getStyle('C3:J3')->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);
                // end format text

                ////////////////numbering
                // A2
                $event->sheet->getDelegate()->setCellValue("A2", "No.");
                $event->sheet->getStyle('A2')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ]);
                // A3
                $event->sheet->getDelegate()->setCellValue("A3", "1");
                // nomor
                $nomor = 1;
                for($i=4;$i<=$max;$i++){
                    $event->sheet->getDelegate()->setCellValue("A".$i, $nomor);
                    $event->sheet->getStyle('A'.$i)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ]
                    ]);
                    $nomor++;
                }
                ////////////end numbering

                ////////column width
                //////////column B
                $event->sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(25);
                $event->sheet->getStyle('B1:B'.$max)->getAlignment()->setWrapText(true);
                //////////column C
                $event->sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('C1:C'.$max)->getAlignment()->setWrapText(true);
                //////////column D
                $event->sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('D1:D'.$max)->getAlignment()->setWrapText(true);
                //////////column E
                $event->sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('E1:E'.$max)->getAlignment()->setWrapText(true);
                //////////column F
                $event->sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('F1:F'.$max)->getAlignment()->setWrapText(true);
                //////////column G
                $event->sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('G1:G'.$max)->getAlignment()->setWrapText(true);
                //////////column H
                $event->sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('H1:H'.$max)->getAlignment()->setWrapText(true);
                //////////column I
                $event->sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('I1:I'.$max)->getAlignment()->setWrapText(true);
                //////////column J
                $event->sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(30);
                $event->sheet->getStyle('J1:J'.$max)->getAlignment()->setWrapText(true);
                ///////////end column


                /////header
                $event->sheet->getStyle('A1:J1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                ]);
                $event->sheet->getDelegate()->mergeCells('A1:J1');
                $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal ." ".$this->tahun_sekarang);
                $event->sheet->getStyle('A1')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 18
                    ]
                ]);
                /////end header

                ///////////////border total
                $f2 = $max+1;
                $event->sheet->getStyle('A'.$f2.':J'.$f2)->applyFromArray([
                    'borders' => [
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                ]);
                $event->sheet->getDelegate()->setCellValue('B'.$f2, "Total");
                $event->sheet->getStyle('B'.$f2)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                    ]
                ]);

                $event->sheet->getDelegate()->setCellValue('C'.$f2 , $this->total_kib_a);
                $event->sheet->getDelegate()->setCellValue('D'.$f2 , $this->total_kib_b);
                $event->sheet->getDelegate()->setCellValue('E'.$f2 , $this->total_kib_c);
                $event->sheet->getDelegate()->setCellValue('F'.$f2 , $this->total_kib_d);
                $event->sheet->getDelegate()->setCellValue('G'.$f2 , $this->total_kib_e);
                $event->sheet->getDelegate()->setCellValue('H'.$f2 , $this->total_kib_f);
                $event->sheet->getDelegate()->setCellValue('I'.$f2 , $this->total_kib_g);
                $event->sheet->getDelegate()->setCellValue('J'.$f2 , $this->total_kib_r);
                $event->sheet->getStyle('C'.$f2.':J'.$f2)->getNumberFormat()
                    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE);
                $event->sheet->getStyle('C'.$f2.':J'.$f2)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                    ]
                ]);
                ////end total

                $date = date('d/m/Y');
                $f1 = $max+3;
                for($i = 0; $i<5; $i++) {
                    $event->sheet->getDelegate()->mergeCells('A'.$f1.':D'.$f1);
                    $event->sheet->getDelegate()->mergeCells('G'.$f1.':J'.$f1);
                    $event->sheet->getStyle('A'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);
                    $event->sheet->getStyle('G'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ]
                    ]);

                    if($i == 0) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                        $event->sheet->getDelegate()->setCellValue('G'.$f1, "Mojokerto, ".$date);
                    }

                    if($i == 4) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                        $event->sheet->getDelegate()->setCellValue('G'.$f1, "NIP");
                    }

                    $f1++;
                }
            },
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'D' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'H' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'I' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'J' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
        ];
    }

    public function title(): string
    {
        return 'Data rekap Penyusutan';
    }

}
