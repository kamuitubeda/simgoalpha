<?php

namespace App\Exports;

use App\Models\Jurnal\Kib;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Tahun;
use App\Models\Jurnal\Penyusutan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class NewPenyusutan108Export implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

	public $nomor_lokasi;
	public $kode_kepemilikan;
    public $bidang_barang;
    public $jenis_aset;

	function __construct($args){
		$this->nomor_lokasi = $args['nomor_lokasi'];
        $this->bidang_barang = $args['bidang_barang'];
		$this->kode_kepemilikan = $args['kode_kepemilikan'];
        $this->jenis_aset = $args['jenis_aset'];
		$this->nama_lokasi = $args['nama_lokasi'];
		$this->nama_jurnal = $args['nama_jurnal'];

        $this->total_nilai_perolehan = 0;
        $this->total_akumulasi_penyusutan = 0;
        $this->total_akumulasi_penyusutan_berjalan = 0;
        $this->total_beban = 0;
        $this->total_nilai_buku = 0;
	}

    public function collection() 
    {
        ini_set('max_execution_time', 1800);

        if($this->kode_kepemilikan == '0' && $this->jenis_aset == '0'){
            $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
            ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
            ->select('penyusutans.*', 'kibs.nomor_lokasi', 'kibs.tahun_pengadaan', 'kibs.nama_barang', 'kibs.no_register', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.kode_108', 'kibs.kode_64', 'kibs.kode_kepemilikan')
            ->where("kibs.nomor_lokasi", 'like', $this->nomor_lokasi . '%')
            ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
            ->get()
            ->toArray();   
        } else if($this->kode_kepemilikan != '0' && $this->jenis_aset == '0'){
            $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
            ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
            ->select('penyusutans.*', 'kibs.nomor_lokasi', 'kibs.tahun_pengadaan', 'kibs.nama_barang', 'kibs.no_register', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.kode_108', 'kibs.kode_64', 'kibs.kode_kepemilikan')
            ->where("kibs.nomor_lokasi", 'like', $this->nomor_lokasi . '%')
            ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
            ->where("kibs.kode_kepemilikan", $this->kode_kepemilikan)
            ->get()
            ->toArray();   
        } else if($this->kode_kepemilikan == '0' && $this->jenis_aset != '0'){
            if($this->nomor_lokasi == '12.01.35.16.111.00001' || $this->nomor_lokasi == '12.01.35.16.141.00001.00001') {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    if($this->bidang_barang == "A") {
                        $kode_108 = "1.5.4.01.01.01.001";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.5.4.01.01.01.002";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.5.4.01.01.01.003";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.5.4.01.01.01.004";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.5.4.01.01.01.005";
                    }
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('penyusutans.*', 'kibs.nomor_lokasi', 'kibs.tahun_pengadaan', 'kibs.nama_barang', 'kibs.no_register', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.kode_108', 'kibs.kode_64', 'kibs.kode_kepemilikan')
                ->where("kibs.nomor_lokasi", 'like', $this->nomor_lokasi . '%')
                ->where("kibs.kode_108", 'like', $kode_108 . "%")
                ->get()
                ->toArray();   
            } else {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    $kode_108 = "1.5.4.01.01";
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('penyusutans.*', 'kibs.nomor_lokasi', 'kibs.tahun_pengadaan', 'kibs.nama_barang', 'kibs.no_register', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.kode_108', 'kibs.kode_64', 'kibs.kode_kepemilikan')
                ->where("kibs.nomor_lokasi", 'like', $this->nomor_lokasi . '%')
                ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
                ->where("kibs.kode_108", 'like', $kode_108 . "%")
                ->get()
                ->toArray();   
            }   
        } else if($this->kode_kepemilikan != '0' && $this->jenis_aset != '0') {
            if($this->nomor_lokasi == '12.01.35.16.111.00001' || $this->nomor_lokasi == '12.01.35.16.141.00001.00001') {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    if($this->bidang_barang == "A") {
                        $kode_108 = "1.5.4.01.01.01.001";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.5.4.01.01.01.002";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.5.4.01.01.01.003";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.5.4.01.01.01.004";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.5.4.01.01.01.005";
                    }
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('penyusutans.*', 'kibs.nomor_lokasi', 'kibs.tahun_pengadaan', 'kibs.nama_barang', 'kibs.no_register', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.kode_108', 'kibs.kode_64', 'kibs.kode_kepemilikan')
                ->where("kibs.nomor_lokasi", 'like', $this->nomor_lokasi . '%')
                ->where("kibs.kode_kepemilikan", $this->kode_kepemilikan)
                ->where("kibs.kode_108", 'like', $kode_108 . "%")
                ->get()
                ->toArray();   
            } else {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    $kode_108 = "1.5.4.01.01";
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('penyusutans.*', 'kibs.nomor_lokasi', 'kibs.tahun_pengadaan', 'kibs.nama_barang', 'kibs.no_register', 'kibs.merk_alamat', 'kibs.tipe', 'kibs.kode_108', 'kibs.kode_64', 'kibs.kode_kepemilikan')
                ->where("kibs.nomor_lokasi", 'like', $this->nomor_lokasi . '%')
                ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
                ->where("kibs.kode_kepemilikan", $this->kode_kepemilikan)
                ->where("kibs.kode_108", 'like', $kode_108 . "%")
                ->get()
                ->toArray();   
            }
        }

        $nama_unit = Kamus_lokasi::select("nama_lokasi")->where("nomor_lokasi", 'like', $this->nomor_lokasi . "%")->first();

        if(!is_null($nama_unit)) {
            $nama_unit = $nama_unit->nama_lokasi;
        } else {
            $nama_unit = "";
        }

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = date('Y')-1;
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            if($this->nomor_lokasi == '12.01.35.16.111.00001' || $this->nomor_lokasi == '12.01.35.16.111.00002') {
                $aset_susut[$i++] =
                array(
                    "nomor_lokasi" => $value['nomor_lokasi'],
                    "nama_lokasi" => $nama_lokasi,
                    "no_register" => $value["no_register"],
                    "kode_108" => $value["kode_108"],
                    "kode_64" => $value["kode_64"],
                    "nama_barang" => $value["nama_barang"],
                    "merk_alamat" => $value["merk_alamat"],
                    "tahun_perolehan" => $value["tahun_pengadaan"],
                    "masa_manfaat" => $value["masa_manfaat"],
                    "masa_terpakai" => $value["masa_terpakai"],
                    "masa_sisa" => $value["masa_sisa"],
                    "nilai_pengadaan" => $value["nilai_perolehan"],
                    "akumulasi_penyusutan" => $value["akumulasi_penyusutan"],
                    "beban" => $value["beban"],
                    "nilai_buku" => $value["nilai_buku"]
                );
            } else {
                $aset_susut[$i++] =
                array(
                    "no_register" => $value["no_register"],
                    "kode_108" => $value["kode_108"],
                    "kode_64" => $value["kode_64"],
                    "nama_barang" => $value["nama_barang"],
                    "merk_alamat" => $value["merk_alamat"],
                    "tahun_perolehan" => $value["tahun_pengadaan"],
                    "masa_manfaat" => $value["masa_manfaat"],
                    "masa_terpakai" => $value["masa_terpakai"],
                    "masa_sisa" => $value["masa_sisa"],
                    "nilai_pengadaan" => $value["nilai_perolehan"],
                    "akumulasi_penyusutan" => $value["akumulasi_penyusutan"],
                    "beban" => $value["beban"],
                    "nilai_buku" => $value["nilai_buku"]
                );
            }

            $this->total_nilai_perolehan += $value["nilai_perolehan"];
            $this->total_akumulasi_penyusutan += $value["akumulasi_penyusutan"];
            $this->total_akumulasi_penyusutan_berjalan += $value["akumulasi_penyusutan_berjalan"];
            $this->total_beban += $value["beban"];
            $this->total_nilai_buku += $value["nilai_buku"];
        }
        
        array_multisort(array_column($aset_susut, 'no_register'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    public function startCell(): string
    {
        return 'A3';
    }

    public function headingRow(): int
    {
        return 3;
    }

    public function headings(): array
    {   
        if($this->nomor_lokasi == '12.01.35.16.111.00001' || $this->nomor_lokasi == '12.01.35.16.111.00002') {
            return ["NOMOR_LOKASI", "NAMA LOKASI", "NO REGISTER", "KODE 108", "KODE 64", "NAMA BARANG", "MERK/ALAMAT", "TAHUN PEROLEHAN", "MASA MANFAAT", "MASA TERPAKAI", "MASA SISA", "NILAI PEROLEHAN", "AKUMULASI PENYUSUTAN", "BEBAN", "NILAI BUKU"];
        } else {
            return ["NO REGISTER", "KODE 108", "KODE 64", "NAMA BARANG", "MERK/ALAMAT", "TAHUN PEROLEHAN", "MASA MANFAAT", "MASA TERPAKAI", "MASA SISA", "NILAI PEROLEHAN", "AKUMULASI PENYUSUTAN", "BEBAN", "NILAI BUKU"];
        }
    }

    public function registerEvents(): array
    {
        if($this->nomor_lokasi == '12.01.35.16.111.00001' || $this->nomor_lokasi == '12.01.35.16.111.00002') {
            return [
                AfterSheet::class => function (AfterSheet $event) {
                    $max = $event->sheet->getDelegate()->getHighestRow();
                    $event->sheet->getStyle('A3:O3')->applyFromArray([
                        'font' => [
                            'bold' => true
                        ], 
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                'color' => ['argb' => '000000']
                            ],
                        ],
                    ]);

                    $event->sheet->getDelegate()->mergeCells('A1:O1');
                    $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal . " " . $this->bidang_barang . " " . $this->nama_lokasi ." 2019");
                    $event->sheet->getStyle('A1')->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 18
                        ]
                    ]);

                    $event->sheet->getStyle('A3:O3')->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);

                    $date = date('d/m/Y');
                    $baris_total = $max+1;
                    $event->sheet->getDelegate()->mergeCells('A'.$baris_total.':I'.$baris_total);
                    $event->sheet->getStyle('A'.$baris_total)->applyFromArray([ 'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 11
                        ] 
                    ]);
                    $event->sheet->getDelegate()->setCellValue('A'.$baris_total, "TOTAL");
                    $event->sheet->getStyle('L'.$baris_total.':O'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                    $event->sheet->getStyle('L'.$baris_total.':O'.$baris_total)->applyFromArray([ 'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 11
                        ] 
                    ]);
                    $event->sheet->getDelegate()->setCellValue('L'.$baris_total, $this->total_nilai_perolehan);
                    $event->sheet->getDelegate()->setCellValue('M'.$baris_total, $this->total_akumulasi_penyusutan);
                    $event->sheet->getDelegate()->setCellValue('N'.$baris_total, $this->total_beban);
                    $event->sheet->getDelegate()->setCellValue('O'.$baris_total, $this->total_nilai_buku);
                    $f1 = $max+3;
                    for($i = 0; $i<5; $i++) {
                        $event->sheet->getDelegate()->mergeCells('A'.$f1.':E'.$f1);
                        $event->sheet->getDelegate()->mergeCells('F'.$f1.':J'.$f1);
                        $event->sheet->getDelegate()->mergeCells('K'.$f1.':O'.$f1);
                        $event->sheet->getStyle('A'.$f1)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ],
                        ]);
                        $event->sheet->getStyle('K'.$f1)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ],
                        ]);

                        if($i == 0) {
                            $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                            $event->sheet->getDelegate()->setCellValue('K'.$f1, "Mojokerto, ".$date);
                        }

                        if($i == 4) {
                            $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                            $event->sheet->getDelegate()->setCellValue('K'.$f1, "NIP");
                        }

                        $f1++;
                    }
                },
            ];
        } else {
            return [
                AfterSheet::class => function (AfterSheet $event) {
                    $max = $event->sheet->getDelegate()->getHighestRow();
                    $event->sheet->getStyle('A3:M3')->applyFromArray([
                        'font' => [
                            'bold' => true
                        ], 
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                                'color' => ['argb' => '000000']
                            ],
                        ],
                    ]);

                    $event->sheet->getDelegate()->mergeCells('A1:M1');
                    $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal . " " . $this->bidang_barang . " " . $this->nama_lokasi ." 2019");
                    $event->sheet->getStyle('A1')->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 18
                        ]
                    ]);

                    $event->sheet->getStyle('A3:L3')->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);

                    $date = date('d/m/Y');
                    $baris_total = $max+1;
                    $event->sheet->getDelegate()->mergeCells('A'.$baris_total.':I'.$baris_total);
                    $event->sheet->getStyle('A'.$baris_total)->applyFromArray([ 'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 11
                        ] 
                    ]);
                    $event->sheet->getDelegate()->setCellValue('A'.$baris_total, "TOTAL");
                    $event->sheet->getStyle('J'.$baris_total.':M'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                    $event->sheet->getStyle('J'.$baris_total.':M'.$baris_total)->applyFromArray([ 'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        ],
                        'font' => [
                            'bold' => true,
                            'size' => 11
                        ] 
                    ]);
                    $event->sheet->getDelegate()->setCellValue('J'.$baris_total, $this->total_nilai_perolehan);
                    $event->sheet->getDelegate()->setCellValue('K'.$baris_total, $this->total_akumulasi_penyusutan);
                    $event->sheet->getDelegate()->setCellValue('L'.$baris_total, $this->total_beban);
                    $event->sheet->getDelegate()->setCellValue('M'.$baris_total, $this->total_nilai_buku);
                    $f1 = $max+3;
                    for($i = 0; $i<5; $i++) {
                        $event->sheet->getDelegate()->mergeCells('A'.$f1.':D'.$f1);
                        $event->sheet->getDelegate()->mergeCells('E'.$f1.':I'.$f1);
                        $event->sheet->getDelegate()->mergeCells('J'.$f1.':M'.$f1);
                        $event->sheet->getStyle('A'.$f1)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ],
                        ]);
                        $event->sheet->getStyle('J'.$f1)->applyFromArray([
                            'alignment' => [
                                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            ],
                        ]);

                        if($i == 0) {
                            $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                            $event->sheet->getDelegate()->setCellValue('J'.$f1, "Mojokerto, ".$date);
                        }

                        if($i == 4) {
                            $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                            $event->sheet->getDelegate()->setCellValue('J'.$f1, "NIP");
                        }

                        $f1++;
                    }
                },
            ];
        }
	}

	public function columnFormats(): array
    {
        if($this->nomor_lokasi == '12.01.35.16.111.00001' || $this->nomor_lokasi == '12.01.35.16.111.00002') {
            return [
                'C' => NumberFormat::FORMAT_TEXT,
                'L' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
                'M' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
                'N' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
                'O' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
            ];
        } else {
            return [
                'A' => NumberFormat::FORMAT_TEXT,
                'J' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
                'K' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
                'L' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
                'M' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
            ];
        }
    }

	public function title(): string
    {
        return 'Data Penyusutan';
    }

}