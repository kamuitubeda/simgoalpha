<?php

namespace App\Exports;

use App\Models\Jurnal\Kib;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_mapping_permen;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Penyusutan;
use App\Models\Jurnal\Tahun;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class LaporanPenyusutanAk108Export implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nomor_lokasi;
    public $kode_kepemilikan;
    public $bidang_barang;
    public $jenis_aset;

    function __construct($args){
        $this->nomor_lokasi = $args['nomor_lokasi'];
        $this->bidang_barang = $args['bidang_barang'];
        $this->kode_kepemilikan = $args['kode_kepemilikan'];
        $this->jenis_aset = $args['jenis_aset'];
        $this->nama_lokasi = $args['nama_lokasi'];
        $this->nama_jurnal = $args['nama_jurnal'];

        $this->total_nilai_perolehan = 0;
        $this->total_akumulasi_penyusutan = 0;
        $this->total_akumulasi_penyusutan_berjalan = 0;
        $this->total_beban = 0;
        $this->total_nilai_buku = 0;

        $this->tahun_sekarang = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
    }

    public function collection() 
    {
        ini_set('max_execution_time', 1800);
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        if($this->kode_kepemilikan == '0' && $this->jenis_aset == '0'){
            $data = Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
                        ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan)
                        ->get()
                        ->toArray();
        } else if($this->kode_kepemilikan != '0' && $this->jenis_aset == '0'){
            $data = Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
                        ->where("kibs.kode_kepemilikan", $this->kode_kepemilikan)
                        ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan)
                        ->get()
                        ->toArray();   
        } else if($this->kode_kepemilikan == '0' && $this->jenis_aset != '0'){
            if($this->nomor_lokasi == '12.01.35.16.111.00001') {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    if($this->bidang_barang == "A") {
                        $kode_108 = "1.5.4.01.01.01.001";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.5.4.01.01.01.002";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.5.4.01.01.01.003";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.5.4.01.01.01.004";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.5.4.01.01.01.005";
                    }
                }
                $data = Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("kibs.kode_108", 'like', $kode_108 . "%")
                        ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan)
                        ->get()
                        ->toArray();   
            } else {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    $kode_108 = "1.5.4.01.01";
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
                        ->where("kibs.kode_108", 'like', $kode_108 . "%")
                        ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan)
                        ->get()
                        ->toArray();   
            }   
        } else if($this->kode_kepemilikan != '0' && $this->jenis_aset != '0') {
            if($this->nomor_lokasi == '12.01.35.16.111.00001') {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    if($this->bidang_barang == "A") {
                        $kode_108 = "1.5.4.01.01.01.001";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.5.4.01.01.01.002";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.5.4.01.01.01.003";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.5.4.01.01.01.004";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.5.4.01.01.01.005";
                    }
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("kibs.kode_kepemilikan", $this->kode_kepemilikan)
                        ->where("kibs.kode_108", 'like', $kode_108 . "%")
                        ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan)
                        ->get()
                        ->toArray();   
            } else {
                if($this->jenis_aset == "A") {
                    if($this->bidang_barang == "G"){
                        $kode_108 = "1.5.3";
                    } else if($this->bidang_barang == "A"){
                        $kode_108 = "1.3.1";
                    } else if($this->bidang_barang == "B"){
                        $kode_108 = "1.3.2";
                    } else if($this->bidang_barang == "C"){
                        $kode_108 = "1.3.3";
                    } else if($this->bidang_barang == "D"){
                        $kode_108 = "1.3.4";
                    } else if($this->bidang_barang == "E"){
                        $kode_108 = "1.3.5";
                    } else if($this->bidang_barang == "F"){
                        $kode_108 = "1.3.6";
                    }
                } else if($this->jenis_aset == "R") {
                    $kode_108 = "1.5.4.01.01";
                }
                $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.akumulasi_penyusutan_berjalan', 'penyusutans.nilai_buku' )
                        ->where('kibs.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("kibs.bidang_barang", 'like', $this->bidang_barang ."%")
                        ->where("kibs.kode_kepemilikan", $this->kode_kepemilikan)
                        ->where("kibs.kode_108", 'like', $kode_108 . "%")
                        ->get()
                        ->toArray();   
            }
        }

        $rekap_108p = array();
        // loop khusus build map 64
        foreach($data as $value) {
            $kode_108 = substr($value["kode_108"],0,14);

            if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                $uraian = Kamus_mapping_permen::select("kode_2", "uraian_2")->where('permen_1', '108')
                                                                    ->where('permen_2', '108p')
                                                                    ->where('kode_1', $kode_108)
                                                                    ->first();
            }

            if(!is_null($uraian)) {
                $kode_108p = $uraian->kode_2;
                $uraian_108p = $uraian->uraian_2;
            } else {
                $kode_108p = null;
                $uraian_108p = "";
            }

            $found = false;
            foreach($rekap_108p as $key => $value)
            {   
                if ($value["kode_108p"] == $kode_108p) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108p)) {
                    array_push($rekap_108p, array(
                        "kode_108p" => 0,
                        "uraian_108p" => "",
                        "nilai_perolehan" => 0,
                        "akumulasi_penyusutan" => 0,
                        "beban" => 0,
                        "akumulasi_penyusutan_berjalan" => 0,
                        "nilai_buku" => 0
                    ));
                } else {
                    array_push($rekap_108p, array(
                        "kode_108p" => $kode_108p,
                        "uraian_108p" => $uraian_108p,
                        "nilai_perolehan" => 0,
                        "akumulasi_penyusutan" => 0,
                        "beban" => 0,
                        "akumulasi_penyusutan_berjalan" => 0,
                        "nilai_buku" => 0
                    ));
                }
            }
        }

        $nama_unit = Kamus_lokasi::select("nama_lokasi")->where("nomor_lokasi", 'like', $this->nomor_lokasi . "%")->first();

        if(!is_null($nama_unit)) {
            $nama_unit = $nama_unit->nama_lokasi;
        } else {
            $nama_unit = "";
        }

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = date('Y')-1;
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $this->total_nilai_perolehan += $value['nilai_pengadaan'];
            $this->total_akumulasi_penyusutan += $value['akumulasi_penyusutan'];
            $this->total_akumulasi_penyusutan_berjalan += $value['akumulasi_penyusutan_berjalan'];
            $this->total_beban += $value['beban'];
            $this->total_nilai_buku += $value['nilai_buku'];

            $kode_108 = substr($value["kode_108"],0,14);

            if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                $uraian = Kamus_mapping_permen::select("kode_2", "uraian_2")->where('permen_1', '108')
                                                                    ->where('permen_2', '108p')
                                                                    ->where('kode_1', $kode_108)
                                                                    ->first();
            }

            if(!is_null($uraian)) {
                $kode_108p = $uraian->kode_2;
                $uraian_108p = $uraian->uraian_2;
            } else {
                $kode_108p = null;
                $uraian_108p = "";
            }

            foreach($rekap_108p as $key => $rekap)
            {   
                if($kode_108p != '' || !is_null($kode_108p) || $kode_108p != 0) {
                    if ($rekap["kode_108p"] == $kode_108p) {
                        $rekap_108p[$key]["nilai_perolehan"] += $value['nilai_pengadaan'];
                        $rekap_108p[$key]["akumulasi_penyusutan"] += $value['akumulasi_penyusutan'];
                        $rekap_108p[$key]["beban"] += $value['beban'];
                        $rekap_108p[$key]["akumulasi_penyusutan_berjalan"] += $value['akumulasi_penyusutan_berjalan'];
                        $rekap_108p[$key]["nilai_buku"] += $value['nilai_buku'];
                        break;
                    }
                } else {
                    $rekap_108p[$key]["nilai_perolehan"] += $value['nilai_pengadaan'];
                    $rekap_108p[$key]["akumulasi_penyusutan"] += $value['akumulasi_penyusutan'];
                    $rekap_108p[$key]["beban"] += $value['beban'];
                    $rekap_108p[$key]["akumulasi_penyusutan_berjalan"] += $value['akumulasi_penyusutan_berjalan'];
                    $rekap_108p[$key]["nilai_buku"] += $value['nilai_buku'];
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108p, 'kode_108p'), SORT_ASC, $rekap_108p);
        $export = collect($rekap_108p);

        return $export;
    }

    public function startCell(): string
    {
        return 'B3';
    }

    public function headingRow(): int
    {
        return 3;
    }

    public function headings(): array
    {
        $headings = [
            ["KODE 108", "URAIAN", "NILAI PEROLEHAN", "AKUMULASI PENYUSUTAN", "BEBAN", "AKUMULASI PENYUSUTAN BERJALAN", "NILAI BUKU"],
            [
                2,3,4,5,6,7,8
            ]
        ];

        return $headings;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $max = $event->sheet->getDelegate()->getHighestRow();
                ////////// set paper
                $event->sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setFitToWidth(1);
                $event->sheet->getPageSetup()->setFitToHeight(0);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
                $event->sheet->setShowGridlines(false);
                $event->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 4);
                $event->sheet->freezePane('I5');
                // end set paper

                /////////border heading
                $event->sheet->getStyle('A3:H3')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A4:H4')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end border heading
                // border
                $event->sheet->getStyle('A5:H'.$max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A'.($max+1).':H'.($max+1))->applyFromArray([
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end border

                // footer
                $event->sheet->getHeaderFooter()
                ->setOddFooter('&L&B '.$this->nama_jurnal.' '. $this->bidang_barang.' / '. $this->nama_lokasi.' / '.$this->tahun_sekarang.' / '.$this->kode_kepemilikan.' - Pemerintah Kab / kota / ' .$this->jenis_aset . '&R &P / &N');
                // end footer

                /////////header
                $event->sheet->getDelegate()->mergeCells('A1:H1');
                $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal . " " . $this->bidang_barang . " " . $this->nama_lokasi ." ".$this->tahun_sekarang);
                $event->sheet->getStyle('A1')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 18
                    ]
                ]);
                // end header

                ///////heading
                $event->sheet->getStyle('A3:H3')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A3:H3')->getAlignment()->setWrapText(true);
                // end heading

                //////////numbering
                $event->sheet->getDelegate()->setCellValue("A3", "No.");
                $event->sheet->getDelegate()->setCellValue("A4", "1");
                $event->sheet->getStyle('A3:A'.$max)->applyFromArray([
                    'alignment' =>[
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getColumnDimension('A')->setAutoSize(true);
                $nomor = 1;
                for($i=5;$i<=$max;$i++){
                    $event->sheet->getDelegate()->setCellValue("A".$i, $nomor);
                    $nomor++;
                }
                ///////////////end numbering

                // format text
                $event->sheet->getStyle('D4:H4')->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_TEXT ] );

                ///////////////column
                // C uraian
                $event->sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(25);
                $event->sheet->getStyle('C3:C'.$max)->getAlignment()->setWrapText(true);
                // G akumulasi penyusutan berjalan
                $event->sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(25);
                $event->sheet->getStyle('G3:G'.$max)->getAlignment()->setWrapText(true);
                ///////end column

                $date = date('d/m/Y');

                $baris_total = $max+1;

                $event->sheet->getDelegate()->mergeCells('B'.$baris_total.':C'.$baris_total);
                $event->sheet->getStyle('B'.$baris_total)->applyFromArray([ 'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 11
                    ]
                ]);
                $event->sheet->getDelegate()->setCellValue('B'.$baris_total, "TOTAL");
                $event->sheet->getStyle('D'.$baris_total.':H'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('D'.$baris_total.':H'.$baris_total)->applyFromArray([ 'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 11
                    ]
                ]);

                $event->sheet->getStyle('D'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('E'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('F'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('G'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('H'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getDelegate()->setCellValue('D'.$baris_total, $this->total_nilai_perolehan);
                $event->sheet->getDelegate()->setCellValue('E'.$baris_total, $this->total_akumulasi_penyusutan);
                $event->sheet->getDelegate()->setCellValue('F'.$baris_total, $this->total_beban);
                $event->sheet->getDelegate()->setCellValue('G'.$baris_total, $this->total_akumulasi_penyusutan_berjalan);
                $event->sheet->getDelegate()->setCellValue('H'.$baris_total, $this->total_nilai_buku);

                $f1 = $max+3;
                for($i = 0; $i<5; $i++) {
                    $event->sheet->getDelegate()->mergeCells('A'.$f1.':D'.$f1);
                    $event->sheet->getDelegate()->mergeCells('F'.$f1.':H'.$f1);
                    $event->sheet->getStyle('A'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);
                    $event->sheet->getStyle('F'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);

                    if($i == 0) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                        $event->sheet->getDelegate()->setCellValue('F'.$f1, "Mojokerto, ".$date);
                    }

                    if($i == 4) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                        $event->sheet->getDelegate()->setCellValue('F'.$f1, "NIP");
                    }

                    $f1++;
                }
            },
        ];
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'E' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'F' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'G' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'H' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
        ];
    }

    public function title(): string
    {
        return 'Data Penyusutan';
    }
}
