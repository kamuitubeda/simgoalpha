<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rincian_koreksi;
use App\Models\Jurnal\Penyusutan;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Tahun;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

class PenyusutanReklasMasuk108Export implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithHeadingRow, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

	public $nomor_lokasi;
	public $kode_kepemilikan;
    public $bidang_barang;
    public $jenis_aset;

	function __construct($args){
		$this->nomor_lokasi = $args['nomor_lokasi'];
        $this->jenis_aset = $args['jenis_aset'];
		$this->nama_lokasi = $args['nama_lokasi'];
		$this->nama_jurnal = $args['nama_jurnal'];

        $this->total_nilai_perolehan = 0;
        $this->total_akumulasi_penyusutan = 0;
        $this->total_akumulasi_penyusutan_berjalan = 0;
        $this->total_beban = 0;
        $this->total_nilai_buku = 0;

        $this->tahun_sekarang = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
	}

    public function collection() 
    {
        ini_set('max_execution_time', 1800);
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        if($this->jenis_aset == "A") {
            $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->join('rincian_koreksis', 'kibs.id_aset', '=', 'rincian_koreksis.id_aset')
                        ->select(DB::raw("CONCAT(kibs.no_register, ' ') as no_register"), 'rincian_koreksis.kode_108', 'rincian_koreksis.kode_108_baru', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.nilai_buku' )
                        ->where('rincian_koreksis.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("rincian_koreksis.kode_108_baru", 'not like', '1.5.4%')
                        ->where("rincian_koreksis.tahun_koreksi", '=', $tahun_laporan)
                        ->get()
                        ->toArray();

            $data_total = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('rincian_koreksis', 'kibs.id_aset', '=', 'rincian_koreksis.id_aset')
                        ->select(DB::raw('SUM(nilai_perolehan) as total_nilai_perolehan'), DB::raw('SUM(akumulasi_penyusutan) as total_akumulasi_penyusutan'), DB::raw('SUM(akumulasi_penyusutan_berjalan) as total_akumulasi_penyusutan_berjalan'), DB::raw('SUM(beban) as total_beban'), DB::raw('SUM(penyusutans.nilai_buku) as total_nilai_buku'))
                        ->where('rincian_koreksis.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("rincian_koreksis.kode_108_baru", 'not like', '1.5.4%')
                        ->where("rincian_koreksis.tahun_koreksi", '=', $tahun_laporan)
                        ->get()
                        ->toArray();
        } else {
            $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                        ->join('rincian_koreksis', 'kibs.id_aset', '=', 'rincian_koreksis.id_aset')
                        ->select(DB::raw("CONCAT(kibs.no_register, ' ') as no_register"), 'rincian_koreksis.kode_108', 'rincian_koreksis.kode_108_baru', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.nilai_buku' )
                        ->where('rincian_koreksis.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("rincian_koreksis.kode_108_baru", 'like', '1.5.4%')
                        ->where("rincian_koreksis.tahun_koreksi", '=', $tahun_laporan)
                        ->get()
                        ->toArray();

            $data_total = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                        ->join('rincian_koreksis', 'kibs.id_aset', '=', 'rincian_koreksis.id_aset')
                        ->select(DB::raw('SUM(nilai_perolehan) as total_nilai_perolehan'), DB::raw('SUM(akumulasi_penyusutan) as total_akumulasi_penyusutan'), DB::raw('SUM(akumulasi_penyusutan_berjalan) as total_akumulasi_penyusutan_berjalan'), DB::raw('SUM(beban) as total_beban'), DB::raw('SUM(penyusutans.nilai_buku) as total_nilai_buku'))
                        ->where('rincian_koreksis.nomor_lokasi', 'like', $this->nomor_lokasi . '%')
                        ->where("rincian_koreksis.kode_108_baru", 'like', '1.5.4%')
                        ->where("rincian_koreksis.tahun_koreksi", '=', $tahun_laporan)
                        ->get()
                        ->toArray();
        }

        $aset_susut = array();
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $i = 0;
        $j = 0;

        foreach($data_total as $value) {
            $this->total_nilai_perolehan += $value["total_nilai_perolehan"];
            $this->total_akumulasi_penyusutan += $value["total_akumulasi_penyusutan"];
            $this->total_akumulasi_penyusutan_berjalan += $value["total_akumulasi_penyusutan_berjalan"];
            $this->total_beban += $value["total_beban"];
            $this->total_nilai_buku += $value["total_nilai_buku"];
        }
        
        array_multisort(array_column($data, 'no_register'), SORT_ASC, $data);
        $export = collect($data);

        return $export;
    }

    public function startCell(): string
    {
        return 'B3';
    }

    public function headingRow(): int
    {
        return 3;
    }

    public function headings(): array
    {
        $headings = [
            ["NO REGISTER", "KODE 108", "KODE 108 ASAL", "NAMA BARANG", "MERK/ALAMAT", "TAHUN PEROLEHAN", "MASA MANFAAT", "MASA TERPAKAI", "MASA SISA", "NILAI PEROLEHAN", "AKUMULASI PENYUSUTAN", "BEBAN", "NILAI BUKU"],
            [
                2,3,4,5,6,7,8,9,10,11,12,13,14
            ]
        ];

        return $headings;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $max = $event->sheet->getDelegate()->getHighestRow();
                ////////// set paper
                $event->sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setFitToWidth(1);
                $event->sheet->getPageSetup()->setFitToHeight(0);
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_FOLIO);
                $event->sheet->setShowGridlines(false);
                $event->sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(3, 4);
                $event->sheet->freezePane('O5');
                // end set paper

                /////////border heading
                $event->sheet->getStyle('A3:N3')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A4:N4')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end border heading
                // border
                $event->sheet->getStyle('A5:N'.$max)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A'.($max+1).':N'.($max+1))->applyFromArray([
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['argb' => '000000']
                        ],
                    ],
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end border

                // footer
                $event->sheet->getHeaderFooter()
                ->setOddFooter('&L&B '.$this->nama_jurnal.' / '. $this->nama_lokasi.' / '.$this->tahun_sekarang.' / '.$this->jenis_aset . '&R &P / &N');
                // end footer

                /////////header
                $event->sheet->getDelegate()->mergeCells('A1:N1');
                $event->sheet->getDelegate()->setCellValue("A1", "Laporan ".$this->nama_jurnal ." " . $this->nama_lokasi ." ".$this->tahun_sekarang);
                $event->sheet->getStyle('A1')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 18
                    ]
                ]);
                // end header

                ///////heading
                $event->sheet->getStyle('A3:N3')->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A3:N3')->getAlignment()->setWrapText(true);
                // end heading

                //////////numbering
                $event->sheet->getDelegate()->setCellValue("A3", "No.");
                $event->sheet->getDelegate()->setCellValue("A4", "1");
                $event->sheet->getStyle('A3:A'.$max)->applyFromArray([
                    'alignment' =>[
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getColumnDimension('A')->setAutoSize(true);
                $nomor = 1;
                for($i=5;$i<=$max;$i++){
                    $event->sheet->getDelegate()->setCellValue("A".$i, $nomor);
                    $nomor++;
                }
                ///////////////end numbering

                // format text
                $event->sheet->getStyle('J4:N4')->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_TEXT ] );

                // centering
                $event->sheet->getStyle('C4:D'.$max)->applyFromArray([
                    'alignment' =>[
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('G4:J'.$max)->applyFromArray([
                    'alignment' =>[
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                // end centering

                /////////////column
                // B no register
                $event->sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(25);
                $event->sheet->getStyle('B3:B'.$max)->getAlignment()->setWrapText(true);
                // c kode 108
                $event->sheet->getColumnDimension('C')->setAutoSize(false)->setWidth(15);
                $event->sheet->getStyle('C3:C'.$max)->getAlignment()->setWrapText(true);
                // D kode 108 tujuan
                $event->sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(20);
                $event->sheet->getStyle('D3:D'.$max)->getAlignment()->setWrapText(true);
                // E nama barang
                $event->sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(25);
                $event->sheet->getStyle('E3:E'.$max)->getAlignment()->setWrapText(true);
                // f merk alamat
                $event->sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(20);
                $event->sheet->getStyle('F3:F'.$max)->getAlignment()->setWrapText(true);
                // g tahun perolehan
                $event->sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(15);
                $event->sheet->getStyle('G3:G'.$max)->getAlignment()->setWrapText(true);
                // h masa manfaat
                $event->sheet->getColumnDimension('H')->setAutoSize(false)->setWidth(15);
                $event->sheet->getStyle('H3:H'.$max)->getAlignment()->setWrapText(true);
                // i masa terpakai
                $event->sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(15);
                $event->sheet->getStyle('I3:I'.$max)->getAlignment()->setWrapText(true);
                // masa sisa
                $event->sheet->getColumnDimension('J')->setAutoSize(false)->setWidth(10);
                $event->sheet->getStyle('J3:J'.$max)->getAlignment()->setWrapText(true);

                // end column


                $date = date('d/m/Y');
                $baris_total = $max+1;
                $event->sheet->getDelegate()->mergeCells('G'.$baris_total.':J'.$baris_total);
                $event->sheet->getStyle('G'.$baris_total)->applyFromArray([ 'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 11
                    ]
                ]);
                $event->sheet->getDelegate()->setCellValue('G'.$baris_total, "TOTAL");
                $event->sheet->getStyle('K'.$baris_total.':N'.$baris_total)->getNumberFormat()->applyFromArray( [ 'formatCode' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE ] );
                $event->sheet->getStyle('K'.$baris_total.':N'.$baris_total)->applyFromArray([ 'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                    ],
                    'font' => [
                        'bold' => true,
                        'size' => 11
                    ]
                ]);
                $event->sheet->getDelegate()->setCellValue('K'.$baris_total, $this->total_nilai_perolehan);
                $event->sheet->getDelegate()->setCellValue('L'.$baris_total, $this->total_akumulasi_penyusutan);
                $event->sheet->getDelegate()->setCellValue('M'.$baris_total, $this->total_beban);
                $event->sheet->getDelegate()->setCellValue('N'.$baris_total, $this->total_nilai_buku);
                $f1 = $max+3;
                for($i = 0; $i<5; $i++) {
                    $event->sheet->getDelegate()->mergeCells('A'.$f1.':D'.$f1);
                    $event->sheet->getDelegate()->mergeCells('E'.$f1.':I'.$f1);
                    $event->sheet->getDelegate()->mergeCells('J'.$f1.':N'.$f1);
                    $event->sheet->getStyle('A'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);
                    $event->sheet->getStyle('J'.$f1)->applyFromArray([
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                    ]);

                    if($i == 0) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "Mengetahui");
                        $event->sheet->getDelegate()->setCellValue('J'.$f1, "Mojokerto, ".$date);
                    }

                    if($i == 4) {
                        $event->sheet->getDelegate()->setCellValue('A'.$f1, "NIP");
                        $event->sheet->getDelegate()->setCellValue('J'.$f1, "NIP");
                    }

                    $f1++;
                }
            },
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'L' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'M' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE,
            'N' => NumberFormat::FORMAT_CURRENCY_IDR_SIMPLE
        ];
    }

    public function title(): string
    {
        return 'Data Penyusutan';
    }

}
