<?php

namespace App\Models\Kamus;
use Illuminate\Database\Eloquent\Model;

class Lokasi_bos extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lama', 'baru', 'nama'
    ];

    protected $primaryKey = 'baru';
    public $incrementing = false;
}
