<?php

namespace App\Models\Jurnal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Penyusutan extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_aset', 'nilai_perolehan', 'masa_manfaat', 'masa_terpakai', 'masa_sisa', 'beban', 'akumulasi_penyusutan', 'akumulasi_penyusutan_berjalan', 'nilai_buku'
    ];

    protected $primaryKey = 'id_aset';
    public $incrementing = false;

    public function scopeForm1($query, $nomor_lokasi, $kode_kepemilikan = false, $bidang_barang = false) {
        return Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.nilai_buku' )
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.bidang_barang', 'like', $bidang_barang . "%"); 
    }

    public function scopeForm2($query, $nomor_lokasi, $kode_kepemilikan = false, $kode_108 = false) {
        return Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
                ->join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.no_register', 'kibs.kode_108', 'kibs.kode_64', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.tahun_pengadaan as tahun_perolehan', 'penyusutans.masa_manfaat', 'penyusutans.masa_terpakai', 'penyusutans.masa_sisa', 'penyusutans.nilai_perolehan as nilai_pengadaan', 'penyusutans.akumulasi_penyusutan', 'penyusutans.beban', 'penyusutans.nilai_buku' )
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.kode_108', 'like', $kode_108 . "%"); 
    }

    public function scopeForm3($query, $nomor_lokasi, $kode_kepemilikan = false, $kode_64 = false, $kode_108 = false) {
        return Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_rekenings', 'kamus_rekenings.kode_64', '=', 'kibs.kode_64')
                        ->select('kamus_rekenings.kode_64 as kode_rekening', 'kamus_rekenings.uraian_64 as uraian', DB::raw('SUM(penyusutans.nilai_perolehan) as nilai_pengadaan'), DB::raw('SUM(penyusutans.beban) as beban'), DB::raw('SUM(penyusutans.akumulasi_penyusutan) as akumulasi_penyusutan'), DB::raw('SUM(penyusutans.akumulasi_penyusutan_berjalan) as akumulasi_penyusutan_berjalan'), DB::raw('SUM(penyusutans.nilai_buku) as nilai_buku'))
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.kode_64', 'like', $kode_64 . "%")
                ->where('kibs.kode_108', 'like', $kode_108 . "%")
                ->groupBy('kamus_rekenings.kode_64', 'kamus_rekenings.uraian_64'); 
    }

    public function scopeForm4($query, $nomor_lokasi, $kode_kepemilikan = false, $kode_64 = false) {
        return Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_rekenings', 'kamus_rekenings.kode_64', '=', 'kibs.kode_64')
                        ->select('kamus_rekenings.kode_64 as kode_rekening', 'kamus_rekenings.uraian_64 as uraian', DB::raw('SUM(penyusutans.nilai_perolehan) as nilai_pengadaan'), DB::raw('SUM(penyusutans.beban) as beban'), DB::raw('SUM(penyusutans.akumulasi_penyusutan) as akumulasi_penyusutan'), DB::raw('SUM(penyusutans.akumulasi_penyusutan_berjalan) as akumulasi_penyusutan_berjalan'), DB::raw('SUM(penyusutans.nilai_buku) as nilai_buku'))
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.kode_64', 'like', $kode_64 . "%")
                ->groupBy('kamus_rekenings.kode_64', 'kamus_rekenings.uraian_64'); 
    }

    public function scopeForm5($query, $nomor_lokasi, $kode_kepemilikan = false, $bidang_barang = false) {
        return Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_mapping_permens', 'kamus_mapping_permens.kode_1', '=', 'kibs.kode_108')
                        ->select('kamus_mapping_permens.kode_2 as kode_rekening', 'kamus_mapping_permens.uraian_2 as uraian', DB::raw('SUM(penyusutans.nilai_perolehan) as nilai_pengadaan'), DB::raw('SUM(penyusutans.beban) as beban'), DB::raw('SUM(penyusutans.akumulasi_penyusutan) as akumulasi_penyusutan'), DB::raw('SUM(penyusutans.akumulasi_penyusutan_berjalan) as akumulasi_penyusutan_berjalan'), DB::raw('SUM(penyusutans.nilai_buku) as nilai_buku'))
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.bidang_barang', 'like', $bidang_barang . "%")
                ->groupBy('kamus_mapping_permens.kode_2', 'kamus_mapping_permens.uraian_2'); 
    }

    public function scopeForm6($query, $nomor_lokasi, $kode_kepemilikan = false, $kode_108 = false) {
        return Penyusutan::join('kibs', 'kibs.id_aset', '=', 'penyusutans.id_aset')
                        ->join('kamus_mapping_permens', 'kamus_mapping_permens.kode_1', '=', 'kibs.kode_108')
                        ->select('kamus_mapping_permens.kode_2 as kode_rekening', 'kamus_mapping_permens.uraian_2 as uraian', DB::raw('SUM(penyusutans.nilai_perolehan) as nilai_pengadaan'), DB::raw('SUM(penyusutans.beban) as beban'), DB::raw('SUM(penyusutans.akumulasi_penyusutan) as akumulasi_penyusutan'), DB::raw('SUM(penyusutans.akumulasi_penyusutan_berjalan) as akumulasi_penyusutan_berjalan'), DB::raw('SUM(penyusutans.nilai_buku) as nilai_buku'))
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.kode_108', 'like', $kode_108 . "%")
                ->groupBy('kamus_mapping_permens.kode_2', 'kamus_mapping_permens.uraian_2'); 
    }

    public function scopeFilter($query, $q) {
        if($q == null) return $query;
        return $query
                ->where('tahun_pengadaan', 'like', '%'. $q .'%')
                ->orWhere('nama_barang', 'like', '%'. $q .'%')
                ->orWhere('merk_alamat', 'like', '%'. $q .'%')
                ->orWhere('no_register', 'like', '%'. $q .'%')
                ->orWhere('id_aset', 'like', '%'. $q .'%')
                ->orWhere('harga_total_plus_pajak_saldo', 'like', '%'. $q .'%');
    }

    public function scopeFilter64($query, $q) {
        if($q == null) return $query;
        return $query
                ->where('kamus_rekenings.kode_64', 'like', '%'. $q .'%')
                ->orWhere('kamus_rekenings.uraian_64', 'like', '%'. $q .'%');
    }

    public function scopeFilterAk108($query, $q) {
        if($q == null) return $query;
        return $query
                ->where('kamus_mapping_permens.kode_2', 'like', '%'. $q .'%')
                ->orWhere('kamus_mapping_permens.uraian_2', 'like', '%'. $q .'%');
    }
}
