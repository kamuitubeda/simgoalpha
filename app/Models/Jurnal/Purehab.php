<?php

namespace App\Models\Jurnal;
use Illuminate\Database\Eloquent\Model;

class Purehab extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'induk', 'anak'
    ];
}
