<?php

namespace App\Models\Jurnal;
use Illuminate\Database\Eloquent\Model;

class Rekap_penyusutan extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nomor_lokasi', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'r'
    ];

    protected $primaryKey = 'nomor_lokasi';
    public $incrementing = false;
}
