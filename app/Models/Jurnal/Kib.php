<?php

namespace App\Models\Jurnal;
use Illuminate\Database\Eloquent\Model;

class Kib extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_jurnal', 'no_key', 'pos_entri', 'id_aset', 'no_ba_penerimaan', 'no_ba_penerimaan_asal', 'nomor_lokasi', 'id_transaksi', 'tgl_catat', 'jenis_bukti', 'tgl_perolehan', 'no_bukti_perolehan', 'no_penetapan', 'pinjam_pakai', 'kode_wilayah', 'lunas', 'id_entri_asal', 'jenis_aset', 'no_register', 'no_induk', 'no_label', 'no_register_unit', 'kode_spm', 'kode_spesifikasi', 'kode_sub_sub_kelompok', 'kode_sub_kel_at', 'kode_64', 'kode_108', 'nama_barang', 'merk_alamat', 'tipe', 'kode_ruangan', 'nomor_ruangan', 'jumlah_barang', 'saldo_barang', 'saldo_gudang', 'satuan', 'harga_satuan', 'harga_total', 'harga_total_plus_pajak', 'harga_total_plus_pajak_saldo', 'pajak', 'nilai_lunas', 'prosen_susut', 'ak_penyusutan', 'waktu_susut', 'tahun_buku', 'nilai_buku', 'dasar_penilaian', 'tahun_pengadaan', 'tahun_perolehan', 'cara_perolehan', 'sumber_dana', 'perolehan_pemda', 'rencana_alokasi', 'penggunaan', 'keterangan', 'kode_kepemilikan', 'baik', 'kb', 'rb', 'sendiri', 'pihak_3', 'sengketa', 'reklas', 'kode_asal', 'aset_rehab', 'id_aset_induk', 'tgl_update', 'operator', 'bidang_barang', 'no_sertifikat', 'tgl_sertifkat', 'atas_nama_sertifikat', 'status_tanah', 'jenis_dokumen_tanah', 'panjang_tanah', 'lebar_tanah', 'luas_tanah', 'no_imb', 'tgl_imb', 'atas_nama_dokumen_gedung', 'jenis_dok_gedung', 'konstruksi', 'bahan', 'jumlah_lantai', 'luas_lantai', 'luas_bangunan', 'panjang', 'lebar', 'no_regs_induk_tanah', 'pos_entri_tanah', 'id_transaksi_tanah', 'umur_teknis', 'nilai_residu', 'nama_ruas', 'pangkal', 'ujung', 'no_bpkb', 'tgl_bpkb', 'no_stnk', 'tgl_stnk', 'no_rangka_seri', 'nopol', 'warna', 'no_mesin', 'bahan_bakar', 'cc', 'tahun_pembuatan', 'tahun_perakitan', 'no_faktur', 'tgl_faktur', 'ukuran', 'wajib_kalibrasi', 'periode_kalibrasi', 'waktu_kalibrasi', 'tgl_kalibrasi', 'pengkalibrasi', 'biaya_kalibrasi', 'terkunci', 'usul_hapus', 'tgl_usul_hapus', 'alasan_usul_hapus', 'tahun_spj', 'kunci_tmp', 'tahun_verifikasi', 'tutup_buku', 'komtabel', 'tdk_ditemukan'
    ];
    
    protected $primaryKey = 'id_aset';
    public $incrementing = false;

    public function scopeFilter($query, $q) {
        if($q == null) return $query;
        return $query
                ->where('tahun_pengadaan', 'like', '%'. $q .'%')
                ->orWhere('nama_barang', 'like', '%'. $q .'%')
                ->orWhere('merk_alamat', 'like', '%'. $q .'%')
                ->orWhere('no_register', 'like', '%'. $q .'%')
                ->orWhere('id_aset', 'like', '%'. $q .'%')
                ->orWhere('harga_total_plus_pajak_saldo', 'like', '%'. $q .'%');
   }

   public function scopeUrutkan($query, $nomor_lokasi, $kode_kepemilikan, $tahun_laporan) {
        return $query
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.tahun_pengadaan', '<=', $tahun_laporan); 
   }

   public function scopeKibA($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.luas_tanah', 'kibs.tahun_pengadaan', 'kibs.tgl_sertifikat', 'kibs.no_sertifikat', 'kibs.penggunaan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.3.1%');
   }

   public function scopeKibB($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.ukuran', 'kibs.cc', 'kibs.bahan', 'kibs.tahun_pengadaan', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.no_rangka_seri', 'kibs.no_mesin', 'kibs.nopol', 'kibs.no_bpkb', 'kibs.saldo_barang', 'kibs.satuan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.3.2%');
   }

   public function scopeKibC($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.konstruksi', 'kibs.bahan', 'kibs.jumlah_lantai', 'kibs.luas_lantai', 'kibs.no_imb', 'kibs.tgl_imb', 'kibs.luas_bangunan', 'kibs.status_tanah', 'kibs.tahun_pengadaan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.3.3%');
   }

   public function scopeKibD($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.konstruksi', 'kibs.bahan', 'kibs.panjang_tanah', 'kibs.lebar_tanah', 'kibs.luas_tanah', 'kibs.no_imb', 'kibs.tgl_imb', 'kibs.status_tanah', 'kibs.tahun_pengadaan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.3.4%');
   }

   public function scopeKibE($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.konstruksi', 'kibs.bahan', 'kibs.jumlah_lantai', 'kibs.luas_lantai', 'kibs.no_imb', 'kibs.tgl_imb', 'kibs.luas_bangunan', 'kibs.status_tanah', 'kibs.tahun_pengadaan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.3.5%');
   }

   public function scopeKibF($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.konstruksi', 'kibs.bahan', 'kibs.jumlah_lantai', 'kibs.luas_lantai', 'kibs.no_imb', 'kibs.tgl_imb', 'kibs.luas_bangunan', 'kibs.status_tanah', 'kibs.tahun_pengadaan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.3.6%');
   }

   public function scopeKibG($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.konstruksi', 'kibs.bahan', 'kibs.jumlah_lantai', 'kibs.luas_lantai', 'kibs.no_imb', 'kibs.tgl_imb', 'kibs.luas_bangunan', 'kibs.status_tanah', 'kibs.tahun_pengadaan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.5.3%');
   }

   public function scopeKibRB($query) {
        return Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi','kibs.kode_108','kibs.no_register','kibs.nama_barang','kibs.merk_alamat','kibs.ukuran','kibs.cc','kibs.bahan','kibs.tahun_pengadaan','kibs.baik','kibs.kb', 'kibs.rb', 'kibs.no_rangka_seri', 'kibs.no_mesin', 'kibs.nopol', 'kibs.no_bpkb', 'kibs.perolehan_pemda', 'kibs.saldo_barang', 'kibs.satuan', 'kibs.harga_total_plus_pajak_saldo', 'kibs.keterangan')
                ->where('kibs.kode_108', 'like', '1.5.4%');
   }

   public function scopeJurnalMasuk($query, $kode_jurnal) {
        return $data = Kib::join('kamus_lokasis', 'kibs.nomor_lokasi', '=', 'kamus_lokasis.nomor_lokasi')
                    ->select('kibs.nomor_lokasi', 'kamus_lokasis.nama_lokasi', 'kibs.kode_108', 'kibs.no_register', 'kibs.nama_barang', 'kibs.merk_alamat', 'kibs.saldo_barang', 'kibs.harga_total_plus_pajak_saldo', 'kibs.baik', 'kibs.kb', 'kibs.rb', 'kibs.keterangan')
                    ->where('kibs.kode_jurnal', $kode_jurnal);
   }

   public function scopeSekarang($query, $nomor_lokasi, $kode_kepemilikan, $tahun_laporan) {
        return $query
                ->where("kibs.nomor_lokasi", 'like', $nomor_lokasi . "%")
                ->where('kibs.kode_kepemilikan', $kode_kepemilikan)
                ->where('kibs.saldo_barang', '>', 0)
                ->where('kibs.tahun_pengadaan', '=', $tahun_laporan); 
   }

   public function scopeNewFilter($query, $q) {
        if($q == null) return $query;
        return $query
                ->where('kibs.tahun_pengadaan', 'like', '%'. $q .'%')
                ->orWhere('kibs.nama_barang', 'like', '%'. $q .'%')
                ->orWhere('kibs.merk_alamat', 'like', '%'. $q .'%')
                ->orWhere('kibs.no_register', 'like', '%'. $q .'%')
                ->orWhere('kibs.id_aset', 'like', '%'. $q .'%')
                ->orWhere('kibs.harga_total_plus_pajak_saldo', 'like', '%'. $q .'%');
   }

   public function scopeAtribut($query, $q) {
        if($q == null) return $query;
        return $query
                ->where('kibs.nama_barang', 'like', '%'. $q .'%')
                ->orWhere('kibs.merk_alamat', 'like', '%'. $q .'%')
                ->orWhere('kibs.tipe', 'like', '%'. $q .'%')
                ->orWhere('kibs.no_register', 'like', '%'. $q .'%')
                ->orWhere('kibs.id_aset', 'like', '%'. $q .'%')
                ->orWhere('kibs.kode_108', 'like', '%'. $q .'%')
                ->orWhere('kibs.kode_64', 'like', '%'. $q .'%');
   }
}
