<?php

namespace App\Models\Sinkron;
use Illuminate\Database\Eloquent\Model;

class Diknasrehab extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_aset', 'lokasi_tujuan',
    ];
}
