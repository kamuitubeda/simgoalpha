<?php

namespace App\Models\Sinkron;
use Illuminate\Database\Eloquent\Model;

class Tahun extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tahun_spj', 'tahun_laporan'
    ];
}
