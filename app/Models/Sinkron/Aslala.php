<?php

namespace App\Models\Sinkron;
use Illuminate\Database\Eloquent\Model;

class Aslala extends Model

{
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nomor_lokasi', 'no_register', 'jumlah_barang'
    ];
}
