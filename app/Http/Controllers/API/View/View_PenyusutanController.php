<?php

namespace App\Http\Controllers\API\View;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Rincian_masuk;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rehab;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_unit;
use App\Models\Kamus\Kamus_sub_unit;
use App\Models\Kamus\Kamus_masa_manfaat_tambahan;

use Illuminate\Support\Facades\DB;
use App\Models\Jurnal\Penyusutan;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Jurnal\Tahun;

use App\Http\Resources\Jurnal\Rincian_masukCollection;
use App\Http\Resources\Jurnal\KibCollection;
use App\Http\Resources\Jurnal\RehabCollection;

use App\Exports\ArsipLaporanPenyusutan108Export;
use App\Exports\LaporanPenyusutan108Export;
use App\Exports\NewPenyusutan108Export;
use App\Exports\LaporanPenyusutan64Export;
use App\Exports\LaporanPenyusutanAk108Export;
use App\Exports\PenyusutanReklasKeluar64Export;
use App\Exports\PenyusutanReklasKeluar108Export;
use App\Exports\PenyusutanReklasMasuk64Export;
use App\Exports\PenyusutanReklasMasuk108Export;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class View_penyusutanController extends BaseController
{
    public function penyusutan108(Request $request, $nomor_lokasi, $bidang_barang, $kode_kepemilikan = false, $jenis_aset = false) 
    {
        ini_set('max_execution_time', 1800);
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
        $pagination = (int)$request->header('Pagination');

        $per_page = 10;
        $page = 1;
        $sortOrder = 'asc';
        $sortField = 'tahun_pengadaan';
        $filter = null;

        if($request->query->has('per_page')) $per_page = $request->query('per_page');
        if($request->query->has('page')) $page = $request->query('page');

        if($request->query->has('sortOrder')) {
            $sortOrder = $request->query('sortOrder');
            if(empty($sortOrder)) $sortOrder = 'asc';
        }

        if($request->query->has('sortField')) {
            $sortField = $request->query('sortField');
            if(empty($sortField)) $sortField = 'tahun_pengadaan';
        }

        if($request->query->has('filter')) {
            $filter = urldecode($request->query('filter'));
            $filter = trim($filter, '""');
            if(empty($filter)) $filter = null;
        }

        if($jenis_aset == "A") {
            if($bidang_barang == "G"){
                $kode_108 = "1.5.3";
            } else if($bidang_barang == "A"){
                $kode_108 = "1.3.1";
            } else if($bidang_barang == "B"){
                $kode_108 = "1.3.2";
            } else if($bidang_barang == "C"){
                $kode_108 = "1.3.3";
            } else if($bidang_barang == "D"){
                $kode_108 = "1.3.4";
            } else if($bidang_barang == "E"){
                $kode_108 = "1.3.5";
            } else if($bidang_barang == "F"){
                $kode_108 = "1.3.6";
            }
        } else if($jenis_aset == "R") {
            if($bidang_barang == "A") {
                $kode_108 = "1.5.4.01.01.01.001";
            } else if($bidang_barang == "B"){
                $kode_108 = "1.5.4.01.01.01.002";
            } else if($bidang_barang == "C"){
                $kode_108 = "1.5.4.01.01.01.003";
            } else if($bidang_barang == "D"){
                $kode_108 = "1.5.4.01.01.01.004";
            } else if($bidang_barang == "E"){
                $kode_108 = "1.5.4.01.01.01.005";
            }
        }

        if($jenis_aset == '0') {
            if($pagination == 0) {
                $data = Penyusutan::form1($nomor_lokasi, $kode_kepemilikan, $bidang_barang)
                                    ->filter($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->get();
            } else {
                $data = Penyusutan::form1($nomor_lokasi, $kode_kepemilikan, $bidang_barang)
                                    ->filter($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->paginate($per_page);
            }
            
        } else {
            if($pagination == 0) {
                $data = Penyusutan::form2($nomor_lokasi, $kode_kepemilikan, $kode_108)
                                    ->filter($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->get();
            } else {
                $data = Penyusutan::form2($nomor_lokasi, $kode_kepemilikan, $kode_108)
                                    ->filter($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->paginate($per_page);
            }
        }

        return $data;
    }

    public function penyusutan64(Request $request, $nomor_lokasi, $bidang_barang, $kode_kepemilikan, $jenis_aset) 
    {
        ini_set('max_execution_time', 1800);
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
        $pagination = (int)$request->header('Pagination');

        $per_page = 10;
        $page = 1;
        $sortOrder = 'asc';
        $sortField = 'kode_rekening';
        $filter = null;

        if($request->query->has('per_page')) $per_page = $request->query('per_page');
        if($request->query->has('page')) $page = $request->query('page');

        if($request->query->has('sortOrder')) {
            $sortOrder = $request->query('sortOrder');
            if(empty($sortOrder)) $sortOrder = 'asc';
        }

        if($request->query->has('sortField')) {
            $sortField = $request->query('sortField');
            if(empty($sortField)) $sortField = 'kode_rekening';
        }

        if($request->query->has('filter')) {
            $filter = urldecode($request->query('filter'));
            $filter = trim($filter, '""');
            if(empty($filter)) $filter = null;
        }

        if($jenis_aset == "A" || $jenis_aset == "0") {
            if($bidang_barang == "G"){
                $kode_64 = "1.5.3";
            } else if($bidang_barang == "A"){
                $kode_64 = "1.3.1";
            } else if($bidang_barang == "B"){
                $kode_64 = "1.3.2";
            } else if($bidang_barang == "C"){
                $kode_64 = "1.3.3";
            } else if($bidang_barang == "D"){
                $kode_64 = "1.3.4";
            } else if($bidang_barang == "E"){
                $kode_64 = "1.3.5";
            } else if($bidang_barang == "F"){
                $kode_64 = "1.3.6";
            }
        } else if($jenis_aset == "R") {
            $kode_64 = "1.5.4.01.01";

            if($bidang_barang == "A") {
                $kode_108 = "1.5.4.01.01.01.001";
            } else if($bidang_barang == "B"){
                $kode_108 = "1.5.4.01.01.01.002";
            } else if($bidang_barang == "C"){
                $kode_108 = "1.5.4.01.01.01.003";
            } else if($bidang_barang == "D"){
                $kode_108 = "1.5.4.01.01.01.004";
            } else if($bidang_barang == "E"){
                $kode_108 = "1.5.4.01.01.01.005";
            }
        }

       
        if($pagination == 0) {
            if($jenis_aset == 'R') {
                $data = Penyusutan::form3($nomor_lokasi, $kode_kepemilikan, $kode_64, $kode_108)
                                ->filter($filter)
                                ->orderBy($sortField, $sortOrder)
                                ->get();
            } else {
                $data = Penyusutan::form4($nomor_lokasi, $kode_kepemilikan, $kode_64)
                                ->filter($filter)
                                ->orderBy($sortField, $sortOrder)
                                ->get();
            }
        } else {
            if($jenis_aset == 'R') {
                $data = Penyusutan::form3($nomor_lokasi, $kode_kepemilikan, $kode_64, $kode_108)
                                ->filter($filter)
                                ->orderBy($sortField, $sortOrder)
                                ->paginate($per_page);
            } else {
                $data = Penyusutan::form4($nomor_lokasi, $kode_kepemilikan, $kode_64)
                                ->filter($filter)
                                ->orderBy($sortField, $sortOrder)
                                ->paginate($per_page);
            }
        }

        return $data;
    }

    public function penyusutanAk108(Request $request, $nomor_lokasi, $bidang_barang, $kode_kepemilikan, $jenis_aset) 
    {
        ini_set('max_execution_time', 1800);
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
        $pagination = (int)$request->header('Pagination');

        $per_page = 10;
        $page = 1;
        $sortOrder = 'asc';
        $sortField = 'kode_rekening';
        $filter = null;

        if($request->query->has('per_page')) $per_page = $request->query('per_page');
        if($request->query->has('page')) $page = $request->query('page');

        if($request->query->has('sortOrder')) {
            $sortOrder = $request->query('sortOrder');
            if(empty($sortOrder)) $sortOrder = 'asc';
        }

        if($request->query->has('sortField')) {
            $sortField = $request->query('sortField');
            if(empty($sortField)) $sortField = 'kode_rekening';
        }

        if($request->query->has('filter')) {
            $filter = urldecode($request->query('filter'));
            $filter = trim($filter, '""');
            if(empty($filter)) $filter = null;
        }

        if($jenis_aset == "A") {
            if($bidang_barang == "G"){
                $kode_108 = "1.5.3";
            } else if($bidang_barang == "A"){
                $kode_108 = "1.3.1";
            } else if($bidang_barang == "B"){
                $kode_108 = "1.3.2";
            } else if($bidang_barang == "C"){
                $kode_108 = "1.3.3";
            } else if($bidang_barang == "D"){
                $kode_108 = "1.3.4";
            } else if($bidang_barang == "E"){
                $kode_108 = "1.3.5";
            } else if($bidang_barang == "F"){
                $kode_108 = "1.3.6";
            }
        } else if($jenis_aset == "R") {
            if($bidang_barang == "A") {
                $kode_108 = "1.5.4.01.01.01.001";
            } else if($bidang_barang == "B"){
                $kode_108 = "1.5.4.01.01.01.002";
            } else if($bidang_barang == "C"){
                $kode_108 = "1.5.4.01.01.01.003";
            } else if($bidang_barang == "D"){
                $kode_108 = "1.5.4.01.01.01.004";
            } else if($bidang_barang == "E"){
                $kode_108 = "1.5.4.01.01.01.005";
            }
        }

       
        if($jenis_aset == '0') {
            if($pagination == 0) {
                $data = Penyusutan::form5($nomor_lokasi, $kode_kepemilikan, $bidang_barang)
                                    ->filterAk108($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->get();
            } else {
                $data = Penyusutan::form5($nomor_lokasi, $kode_kepemilikan, $bidang_barang)
                                    ->filterAk108($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->paginate($per_page);
            }
            
        } else {
            if($pagination == 0) {
                $data = Penyusutan::form6($nomor_lokasi, $kode_kepemilikan, $kode_108)
                                    ->filterAk108($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->get();
            } else {
                $data = Penyusutan::form6($nomor_lokasi, $kode_kepemilikan, $kode_108)
                                    ->filterAk108($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->paginate($per_page);
            }
        }

        return $data;
    }

    public function exportSusutReklasKeluar64($nomor_lokasi, $jenis_aset) 
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["jenis_aset"] = $jenis_aset;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Penyusutan Aset Reklas Keluar 64";

        $nama_file = "Laporan Penyusutan Aset Reklas Keluar 64 " . $nama_lokasi ;
        $format_file = ".xlsx";

        return Excel::download(new PenyusutanReklasKeluar64Export($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function exportSusutReklasKeluar108($nomor_lokasi, $jenis_aset) 
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["jenis_aset"] = $jenis_aset;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Penyusutan Aset Reklas Keluar 108";

        $nama_file = "Laporan Penyusutan Aset Reklas Keluar 108 " . $nama_lokasi ;
        $format_file = ".xlsx";

        return Excel::download(new PenyusutanReklasKeluar108Export($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function exportSusutReklasMasuk64($nomor_lokasi, $jenis_aset) 
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["jenis_aset"] = $jenis_aset;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Penyusutan Aset Reklas Masuk 64";

        $nama_file = "Laporan Penyusutan Aset Reklas Masuk 64 " . $nama_lokasi ;
        $format_file = ".xlsx";

        return Excel::download(new PenyusutanReklasMasuk64Export($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function exportSusutReklasMasuk108($nomor_lokasi, $jenis_aset) 
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["jenis_aset"] = $jenis_aset;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Penyusutan Aset Reklas Masuk 108";

        $nama_file = "Laporan Penyusutan Aset Reklas Masuk 108 " . $nama_lokasi ;
        $format_file = ".xlsx";

        return Excel::download(new PenyusutanReklasMasuk108Export($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    //ARSIP buat testing
    public function ArsipSusut108($nomor_lokasi, $bidang_barang, $kode_kepemilikan, $jenis_aset) 
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["bidang_barang"] = $bidang_barang;
        $data["kode_kepemilikan"] = $kode_kepemilikan;
        $data["jenis_aset"] = $jenis_aset;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Penyusutan KIB";

        $nama_file = "Laporan Penyusutan Baru KIB " . $bidang_barang . " " . $nama_lokasi ;
        $format_file = ".xlsx";

        return Excel::download(new ArsipLaporanPenyusutan108Export($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }
}