<?php

namespace App\Http\Controllers\API\View;
use Validator;
use Illuminate\Http\Request;

use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Tahun;
use App\Models\Jurnal\Kib_awal;
use App\Models\Jurnal\Rehab;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_unit;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_kab_kota;
use App\Models\Kamus\Kamus_sub_unit;
use App\Http\Resources\Jurnal\KibCollection;
use App\Http\Controllers\API\BaseController as BaseController;

class View_KIBController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function kibUrutTahun(Request $request, $bidang_barang, $nomor_lokasi, $kode_kepemilikan)
    {
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
        $pagination = (int)$request->header('Pagination');

        $per_page = 10;
        $page = 1;
        $sortOrder = 'asc';
        $sortField = 'tahun_pengadaan';
        $filter = null;

        if($request->query->has('per_page')) $per_page = $request->query('per_page');
        if($request->query->has('page')) $page = $request->query('page');

        if($request->query->has('sortOrder')) {
            $sortOrder = $request->query('sortOrder');
            if(empty($sortOrder)) $sortOrder = 'asc';
        }

        if($request->query->has('sortField')) {
            $sortField = $request->query('sortField');
            if(empty($sortField)) $sortField = 'tahun_pengadaan';
        }

        if($request->query->has('filter')) {
            $filter = urldecode($request->query('filter'));
            $filter = trim($filter, '""');
            if(empty($filter)) $filter = null;
        }

        if($bidang_barang == "A") {
            $kode_108 = '1.3.1';
        } else if($bidang_barang == "B") {
            $kode_108 = '1.3.2';
        } else if($bidang_barang == "C") {
            $kode_108 = '1.3.3';
        } else if($bidang_barang == "D") {
            $kode_108 = '1.3.4';
        } else if($bidang_barang == "E") {
            $kode_108 = '1.3.5';
        } else if($bidang_barang == "F") {
            $kode_108 = '1.3.6';
        } else if($bidang_barang == "G") {
            $kode_108 = '1.5.3';
        } else if($bidang_barang == "RB") {
            $kode_108 = '1.5.4';
        }

        if($pagination === 0) {
            if($bidang_barang == "A") {
                $data = Kib::kibA()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "B") {
                $data = Kib::kibB()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "C") {
                $data = Kib::kibC()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "D") {
                $data = Kib::kibD()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "E") {
                $data = Kib::kibE()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "F") {
                $data = Kib::kibF()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "G") {
                $data = Kib::kibG()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            } else if($bidang_barang == "RB") {
                $data = Kib::kibRB()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->get();
            }
        } else {
            if($bidang_barang == "A") {
                $data = Kib::kibA()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "B") {
                $data = Kib::kibB()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "C") {
                $data = Kib::kibC()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "D") {
                $data = Kib::kibD()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "E") {
                $data = Kib::kibE()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "F") {
                $data = Kib::kibF()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "G") {
                $data = Kib::kibG()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            } else if($bidang_barang == "RB") {
                $data = Kib::kibRB()->urutkan($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                        ->newFilter($filter)
                                        ->orderBy($sortField, $sortOrder)
                                        ->paginate($per_page);
            }
        }
        
        return $data;
    }

    public function rekapRincian108(Request $request, $nomor_lokasi, $kode_kepemilikan)
    {
        $data = Kib::select('kibs.kode_108','kibs.no_register','kibs.nama_barang','kibs.merk_alamat','kibs.jumlah_barang','kibs.harga_total_plus_pajak_saldo','kibs.baik','kibs.kb','kibs.rb','kibs.keterangan')
                    ->where('nomor_lokasi', 'like', $nomor_lokasi . '%')
                    ->where('kode_kepemilikan', $kode_kepemilikan)
                    ->get();

        $data_rehab = Rehab::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->get();

        $rekap_108 = array();

        // loop khusus build map 108
        foreach($data as $value) {
            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $rehab = true;
                        break;
                    }
                }
            }

            $kode_108 = $value["kode_108"];

            if(!is_null($kode_108)) {
                $kode_108 = substr($value["kode_108"], 0, 11);
                $uraian = Rincian_108::select("uraian_rincian")->where('rincian', $kode_108)->first();
            }

            if(!is_null($uraian)) {
                $uraian_108 = $uraian->uraian_rincian;
            }

            $found = false;
            foreach($rekap_108 as $key => $value)
            {   
                if ($value["kode_108"] == $kode_108) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108)) {
                    if(!$rehab) {
                        array_push($rekap_108, array(
                            "kode_108" => 0,
                            "uraian_108" => 0,
                            "nilai" => 0
                        ));
                    }
                } else {
                    array_push($rekap_108, array(
                        "kode_108" => $kode_108,
                        "uraian_108" => $uraian_108,
                        "nilai" => 0
                    ));
                }
            }
        }

        //untuk penjumlahan data
        foreach($data as $value) {
            $kode_108 = substr($value["kode_108"], 0, 11);

            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $kode_108_induk = Kib::select("kode_108")->where("id_aset", $value_rehab["aset_induk_id"])->first();
                        if(!empty($kode_108_induk)) {
                            $kode_108 = $kode_108_induk->kode_108;
                            $kode_108 = substr($value["kode_108"], 0, 11);
                            break;
                        }
                    }
                }
            }

            foreach($rekap_108 as $key => $rekap)
            {   
                if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                    if ($rekap["kode_108"] == $kode_108) {
                        $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108, 'kode_108'), SORT_ASC, $rekap_108);
        $export = collect($rekap_108);

        return $export;
    }

    public function rekapSubRincian108(Request $request, $nomor_lokasi, $kode_kepemilikan)
    {
        $data = Kib::select('kibs.kode_108','kibs.no_register','kibs.nama_barang','kibs.merk_alamat','kibs.jumlah_barang','kibs.harga_total_plus_pajak_saldo','kibs.baik','kibs.kb','kibs.rb','kibs.keterangan')
                    ->where('nomor_lokasi', 'like', $nomor_lokasi . '%')
                    ->where('kode_kepemilikan', $kode_kepemilikan)
                    ->get();

        $data_rehab = Rehab::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->get();

        $rekap_108 = array();

        // loop khusus build map 108
        foreach($data as $value) {
            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $rehab = true;
                        break;
                    }
                }
            }

            $kode_108 = $value["kode_108"];

            if(!is_null($kode_108)) {
                $kode_108 = substr($value["kode_108"], 0, 14);
                $uraian = Sub_rincian_108::select("uraian_sub_rincian")->where('sub_rincian', $kode_108)->first();
            }

            if(!is_null($uraian)) {
                $uraian_108 = $uraian->uraian_sub_rincian;
            }

            $found = false;
            foreach($rekap_108 as $key => $value)
            {   
                if ($value["kode_108"] == $kode_108) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108)) {
                    if(!$rehab) {
                        array_push($rekap_108, array(
                            "kode_108" => 0,
                            "uraian_108" => 0,
                            "nilai" => 0
                        ));
                    }
                } else {
                    array_push($rekap_108, array(
                        "kode_108" => $kode_108,
                        "uraian_108" => $uraian_108,
                        "nilai" => 0
                    ));
                }
            }
        }

        foreach($data as $value) {
            $kode_108 = substr($value["kode_108"], 0, 14);

            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $kode_108_induk = Kib::select("kode_108")->where("id_aset", $value_rehab["aset_induk_id"])->first();
                        if(!empty($kode_108_induk)) {
                            $kode_108 = $kode_108_induk->kode_108;
                            $kode_108 = substr($value["kode_108"], 0, 14);
                            break;
                        }
                    }
                }
            }

            foreach($rekap_108 as $key => $rekap)
            {   
                if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                    if ($rekap["kode_108"] == $kode_108) {
                        $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108, 'kode_108'), SORT_ASC, $rekap_108);

        $export = collect($rekap_108);
        return $export;
    }

    public function rekap108(Request $request, $nomor_lokasi, $kode_kepemilikan)
    {
        $data = Kib::select('kibs.kode_108','kibs.no_register','kibs.nama_barang','kibs.merk_alamat','kibs.saldo_barang','kibs.harga_total_plus_pajak_saldo','kibs.baik','kibs.kb','kibs.rb','kibs.keterangan')
                    ->where('nomor_lokasi', 'like', $nomor_lokasi . '%')
                    ->where('kode_kepemilikan', $kode_kepemilikan)
                    ->get();

        $data_rehab = Rehab::where('nomor_lokasi', 'like', $nomor_lokasi . '%')->get();

        $rekap_108 = array();

        // loop khusus build map 108
        foreach($data as $value) {
            $rehab = false;
            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $rehab = true;
                        break;
                    }
                }
            }

            $kode_108 = $value["kode_108"];

            if(!is_null($kode_108)) {
                $uraian = Sub_sub_rincian_108::select("uraian_sub_sub_rincian")->where('sub_sub_rincian', $kode_108)->first();
            }

            if(!is_null($uraian)) {
                $uraian_108 = $uraian->uraian_sub_sub_rincian;
            } else {
                $uraian_108 = '';
            }

            $found = false;
            foreach($rekap_108 as $key => $value)
            {   
                if ($value["kode_108"] == $kode_108) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_108)) {
                    if(!$rehab) {
                        array_push($rekap_108, array(
                            "kode_108" => 0,
                            "uraian_108" => 0,
                            "nilai" => 0
                        ));
                    }
                } else {
                    array_push($rekap_108, array(
                        "kode_108" => $kode_108,
                        "uraian_108" => $uraian_108,
                        "nilai" => 0
                    ));
                }
            }
        }

        foreach($data as $value) {
            $kode_108 = $value["kode_108"];

            if(!empty($data_rehab)) {
                foreach ($data_rehab as $value_rehab) {
                    if($value_rehab["rehab_id"] == $value["id_aset"]) {
                        $kode_108_induk = Kib::select("kode_108")->where("id_aset", $value_rehab["aset_induk_id"])->first();
                        if(!empty($kode_108_induk)) {
                            $kode_108 = $kode_108_induk->kode_108;
                            break;
                        }
                    }
                }
            }

            foreach($rekap_108 as $key => $rekap)
            {   
                if($kode_108 != '' || !is_null($kode_108) || $kode_108 != 0) {
                    if ($rekap["kode_108"] == $kode_108) {
                        $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_108[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_108, 'kode_108'), SORT_ASC, $rekap_108);

        $export = collect($rekap_108);
        return $export;
    }

    public function rekap64(Request $request, $nomor_lokasi, $kode_kepemilikan)
    {
        ini_set('max_execution_time', 1800);
        $data = Kib::select('kibs.kode_64','kibs.no_register','kibs.nama_barang','kibs.merk_alamat','kibs.saldo_barang','kibs.harga_total_plus_pajak_saldo','kibs.baik','kibs.kb','kibs.rb','kibs.keterangan')
                    ->where('nomor_lokasi', 'like', $nomor_lokasi . '%')
                    ->where('kode_kepemilikan', $kode_kepemilikan)
                    ->get();

        $rekap_64 = array();

        // loop khusus build map 64
        foreach($data as $value) {
            $kode_64 = $value["kode_64"];

            if($kode_64 != '' || !is_null($kode_64) || $kode_64 != 0) {
                $uraian = Kamus_rekening::select("uraian_64")->where('kode_64', $kode_64)->first();
            }

            if(!is_null($uraian)) {
                $uraian_64 = $uraian->uraian_64;
            }

            $found = false;
            foreach($rekap_64 as $key => $value)
            {   
                if ($value["kode_64"] == $kode_64) {
                    $found = true;
                    break;
                }
            }

            if(!$found) {
                if(is_null($kode_64)) {
                    array_push($rekap_64, array(
                        "kode_64" => 0,
                        "uraian_64" => 0,
                        "nilai" => 0
                    ));
                } else {
                    array_push($rekap_64, array(
                        "kode_64" => $kode_64,
                        "uraian_64" => $uraian_64,
                        "nilai" => 0
                    ));
                }
            }
        }

        //loop khusus penjumlahan data
        foreach($data as $value) {
            $kode_64 = $value["kode_64"];

            foreach($rekap_64 as $key => $rekap)
            {   
                if($kode_64 != '' || !is_null($kode_64) || $kode_64 != 0) {
                    if ($rekap["kode_64"] == $kode_64) {
                        $rekap_64[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                        $found = true;
                        break;
                    }
                } else {
                    $rekap_64[$key]["nilai"] += $value["harga_total_plus_pajak_saldo"];
                    $found = true;
                    break;
                }
            }
        }

        array_multisort(array_column($rekap_64, 'kode_64'), SORT_ASC, $rekap_64);

        $export = collect($rekap_64);
        return $export;
    }

    // class kib saldo awal
    public function exportLaporanKibAwal($bidang_barang, $nomor_lokasi, $kode_kepemilikan)
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["kode_kepemilikan"] = $kode_kepemilikan;
        $data["nama_lokasi"] = $nama_lokasi;
        $data["bidang_barang"] = $bidang_barang;

        $data["nama_jurnal"] = "Saldo KIB";

        $nama_file = "Saldo Awal KIB " . $bidang_barang . " " . $nama_lokasi ;
        $format_file = ".xlsx";

        return Excel::download(new KibAwalExport($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }
    // end class kib saldo awal

    // kib awal saldo inventaris
    public function exportLaporanKibAwalInv($nomor_lokasi, $kode_kepemilikan)
    {
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();

        $data["nomor_lokasi"] = $nomor_lokasi;
        $data["kode_kepemilikan"] = $kode_kepemilikan;
        $data["nama_lokasi"] = $nama_lokasi;

        $data["nama_jurnal"] = "Buku Inventaris";

        $nama_file = "Laporan Kib Awal Saldo Inventaris" . $nama_lokasi;
        $format_file = ".xlsx";

        return Excel::download(new LaporanInventaris($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    // laporan rekap perolehan satu kabupaten
    public function exportLaporanRekapPerolehan()
    {
        ini_set('max_execution_time', 1800);

        $data = array();

        $data["nama_jurnal"] = "Rekap Laporan Nilai Perolehan Semua";

        $nama_file = "Rekap Perolehan Semua";
        $format_file = ".xlsx.xlsx";

        return Excel::download(new LaporanRekapPerolehanExport($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    // laporan rekap penyusutan satu kabupaten
    public function exportLaporanRekapPenyusutan()
    {
        ini_set('max_execution_time', 1800);

        $data = array();

        $data["nama_jurnal"] = "Rekap Laporan Penyusutan Semua";

        $nama_file = "Rekap Penyusutan Semua";
        $format_file = ".xlsx.xlsx";

        return Excel::download(new LaporanRekapPenyusutanExport($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function laporanKibAFKabupaten()
    {
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '-1');
        ini_set("pcre.backtrack_limit", "1000000000");

        $data = array();

        $nama_kabupaten = Kamus_kab_kota::select('nama_kab')->first()->nama_kab;

        $data["nama_aset"] = "Rekap Laporan KIB Kabupaten ".$nama_kabupaten;

        $nama_file = "Rekap Laporan KIB " .$nama_kabupaten;
        $format_file = ".xlsx.xlsx";

        return Excel::download(new laporanKibAFExport($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function laporanKibGKabupaten()
    {
        ini_set('max_execution_time', 1800);

        $data = array();

        $nama_kabupaten = Kamus_kab_kota::select('nama_kab')->first()->nama_kab;

        $data["nama_aset"] = "Rekap Laporan Aset Tak Berwujud Kabupaten ".$nama_kabupaten;

        $nama_file = "Rekap Laporan Aset Tak Berwujud Kabupaten " .$nama_kabupaten;
        $format_file = ".xlsx.xlsx";

        return Excel::download(new laporanKibGExport($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function laporanKibAsetLain()
    {
        ini_set('max_execution_time', 1800);

        $data = array();

        $nama_kabupaten = Kamus_kab_kota::select('nama_kab')->first()->nama_kab;

        $data["nama_aset"] = "Rekap Laporan Aset Lain-lain Kabupaten ".$nama_kabupaten;

        $nama_file = "Rekap Laporan Aset Lain-lain Kabupaten " .$nama_kabupaten;
        $format_file = ".xlsx.xlsx";

        return Excel::download(new laporanKibLain($data), $nama_file . $format_file, \Maatwebsite\Excel\Excel::XLSX);
    }
}
