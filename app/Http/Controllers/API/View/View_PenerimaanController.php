<?php

namespace App\Http\Controllers\API\View;
use Validator;
use Illuminate\Http\Request;

use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Tahun;
use App\Models\Jurnal\Kib_awal;
use App\Models\Kamus\Kamus_unit;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_kab_kota;
use App\Models\Kamus\Kamus_sub_unit;
use App\Http\Resources\Jurnal\KibCollection;
use App\Http\Controllers\API\BaseController as BaseController;

class View_PenerimaanController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function penerimaan(Request $request, $kode_jurnal, $nomor_lokasi, $kode_kepemilikan)
    {
        $tahun_laporan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
        $pagination = (int)$request->header('Pagination');

        $per_page = 10;
        $page = 1;
        $sortOrder = 'asc';
        $sortField = 'tahun_pengadaan';
        $filter = null;

        if($request->query->has('per_page')) $per_page = $request->query('per_page');
        if($request->query->has('page')) $page = $request->query('page');

        if($request->query->has('sortOrder')) {
            $sortOrder = $request->query('sortOrder');
            if(empty($sortOrder)) $sortOrder = 'asc';
        }

        if($request->query->has('sortField')) {
            $sortField = $request->query('sortField');
            if(empty($sortField)) $sortField = 'tahun_pengadaan';
        }

        if($request->query->has('filter')) {
            $filter = urldecode($request->query('filter'));
            $filter = trim($filter, '""');
            if(empty($filter)) $filter = null;
        }

        if($pagination === 0) {
            $data = Kib::jurnalMasuk($kode_jurnal)->sekarang($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                    ->newFilter($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->get();
        } else {
            $data = Kib::jurnalMasuk($kode_jurnal)->sekarang($nomor_lokasi, $kode_kepemilikan, $tahun_laporan)
                                    ->newFilter($filter)
                                    ->orderBy($sortField, $sortOrder)
                                    ->paginate($per_page);
        }
        
        return $data;
    }
}
