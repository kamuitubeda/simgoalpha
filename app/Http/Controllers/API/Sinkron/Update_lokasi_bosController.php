<?php

namespace App\Http\Controllers\API\Sinkron;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Kib_awal;
use App\Models\Jurnal\Kib;
use App\Models\Kamus\Lokasi_bos;
use Validator;

class Update_lokasi_bosController extends BaseController
{
    public function update(Request $request)
    {
        ini_set('memory_limit', '-1');

        $kosong = array();
        $updated = array();

        $asets = Kib::where('nomor_lokasi', 'like', '12.01.35.16.111.00002%')->where('tahun_pengadaan', '<>', 2019)->get();

        foreach ($asets as $value) {
            $value = json_decode(json_encode($value), true);
            $input = $value;
            $lokasi_asal = $value['nomor_lokasi'];

            $lokasi = Lokasi_bos::select('baru')->where('lama', 'like', $lokasi_asal)->first();

            if(!is_null($lokasi)) {
                $input['nomor_lokasi'] = $lokasi->baru;

                Kib::where('id_aset', $value['id_aset'])->update($input);
                Kib_awal::where('id_aset', $value['id_aset'])->update($input);
                array_push($updated, $value['id_aset']);
            } else {
                array_push($kosong, $value['id_aset']);
            }
        }

        if(empty($kosong)) {
            return array('sukses', $updated);
        } else {
            return array('gagal', $kosong);
        }
    }
}