<?php

namespace App\Http\Controllers\API\Sinkron;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Jurnal;
use App\Models\Sinkron\Aslala;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rincian_koreksi;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Sinkron\Migrasi;
use Validator;

class Migrasi_reklasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aslala(Request $request)
    {
        ini_set('memory_limit', '-1');
        $input = array();
        $jurnal = array();
        $kosong = array();
        $migrated = array();

        $data_reklas = Aslala::get();

        foreach ($data_reklas as $value) {
            $value = json_decode(json_encode($value), true);
            
            $nomor_lokasi = $value["nomor_lokasi"];
            $no_register = $value["no_register"];
            $jumlah_barang = $value["jumlah_barang"];
            $tahun_spj = 2019;
            $kode_jurnal = '306';

            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first();
            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            }

            $max_no_ba = Jurnal::select('no_ba_penerimaan')
                        ->where('nomor_lokasi', $nomor_lokasi)
                        ->where('tahun_spj', $tahun_spj)
                        ->where('kode_jurnal', $kode_jurnal)
                        ->orderBy('no_key', 'DESC')
                        ->first();

            if(empty($max_no_ba)) {
                $nomor = 1;
                $no_ba_penerimaan = "0001";
            } else {
                $max_no_ba = $max_no_ba->no_ba_penerimaan;
                $no_ba_penerimaan = intval($max_no_ba);
                ++$no_ba_penerimaan;

                $nomor = $no_ba_penerimaan;
                $no_ba_penerimaan = strval($no_ba_penerimaan);
                $s = strlen($no_ba_penerimaan);
                if($s == 1) {
                    $no_ba_penerimaan = "000".$no_ba_penerimaan;
                } else if($s == 2) {
                    $no_ba_penerimaan = "00".$no_ba_penerimaan;
                } else if($s == 3) {
                    $no_ba_penerimaan = "0".$no_ba_penerimaan;
                }
            }

            $no_key = $nomor_lokasi . "." . $kode_jurnal .".". $no_ba_penerimaan . "." . $tahun_spj;

            $jurnal["no_ba_penerimaan"] = $no_ba_penerimaan;
            $jurnal["no_key"] = $no_key;
            $jurnal["kode_jurnal"] = $kode_jurnal;
            $jurnal["nomor_lokasi"] = $nomor_lokasi;
            $jurnal["terkunci"] = "1";
            $jurnal["tahun_spj"] = $tahun_spj;

            $validator = Validator::make($jurnal, [
                'no_key' => 'required',
                'nomor_lokasi' => 'required',
                'tahun_spj' => 'required'
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());       
            }

            Jurnal::create($jurnal);

            $aset = Kib::where('no_register', 'like', $no_register . '%')
                    ->where('nomor_lokasi', 'like', $nomor_lokasi . '%')
                    ->first();

            if(!is_null($aset)) {
                $data_aset = json_decode(json_encode($aset), true);
                $input = $data_aset;
            } else {
                array_push($kosong, $no_register);
            }

            $kode_sb_koreksi = "05";
            $saldo_barang = $aset->saldo_barang;
            $harga = $aset->harga_total_plus_pajak_saldo;
            if($saldo_barang <= 0) {
                $sisa = 0;
                $nilai = 0;
                $nilai_sisa = 0;
            } else {
                $sisa = $saldo_barang - $jumlah_barang;
                $nilai = $jumlah_barang/$saldo_barang * $harga;
                $nilai_sisa = $sisa/$saldo_barang * $harga;
            }

            if($aset->bidang_barang == "A") {
                $kode_64_baru = '1.5.4.01.01';
                $kode_108_baru = '1.5.4.01.01.01.001';
            } else if($aset->bidang_barang == "B") {
                $kode_64_baru = '1.5.4.01.01';
                $kode_108_baru = '1.5.4.01.01.01.002';
            } else if($aset->bidang_barang == "C") {
                $kode_64_baru = '1.5.4.01.01';
                $kode_108_baru = '1.5.4.01.01.01.003';
            } else if($aset->bidang_barang == "D") {
                $kode_64_baru = '1.5.4.01.01';
                $kode_108_baru = '1.5.4.01.01.01.004';
            } else if($aset->bidang_barang == "E") {
                $kode_64_baru = '1.5.4.01.01';
                $kode_108_baru = '1.5.4.01.01.01.005';
            } else if($aset->bidang_barang == "G") {
                $kode_64_baru = '1.5.4.01.01';
                $kode_108_baru = '1.5.4.01.01.01.005';
            }

            $data_koreksi["nomor_lokasi"] = $nomor_lokasi;
            $data_koreksi["kode_koreksi"] = $kode_jurnal;
            $data_koreksi["kode_sb_koreksi"] = $kode_sb_koreksi;
            $data_koreksi["no_key"] = $no_key;
            $data_koreksi["bidang_barang"] = $aset->bidang_barang;
            $data_koreksi["id_aset"] = $aset->id_aset;
            $data_koreksi["no_ba_penerimaan"] = $no_ba_penerimaan;
            $data_koreksi["no_register"] = $no_register;
            $data_koreksi["no_label"] = $aset->no_label;
            $data_koreksi["nama_barang"] = $aset->nama_barang;
            $data_koreksi["merk_alamat"] = $aset->merk_alamat;
            $data_koreksi["kode_108"] = $aset->kode_108;
            $data_koreksi["kode_108_baru"] = $kode_108_baru;
            $data_koreksi["kode_64"] = $aset->kode_64;
            $data_koreksi["kode_64_baru"] = $kode_64_baru;

            $data_koreksi["kode_kepemilikan"] = $aset->kode_kepemilikan;
            $data_koreksi["jumlah_barang"] = $jumlah_barang;
            $data_koreksi["nilai"] = $nilai;
            $data_koreksi["keterangan"] = "Usulan RB";
            $data_koreksi["tahun_koreksi"] = 2019;

            $temp = $nomor_lokasi.".".$tahun_spj.".".$kode_jurnal.".".$kode_108_baru;
            $max_id_aset = Kib::select('id_aset')->where('id_aset', 'like', '%'.$temp.'%')->orderBy('id_aset', 'DESC')->first();

            if(is_null($max_id_aset)) {
                $id_aset_index = "00001";
            } else {
                $max_id_aset = $max_id_aset->id_aset;

                if(empty($max_id_aset)) {
                    $id_aset_index = "00001";
                } else {
                    $id_aset_index = str_replace($temp, '', $max_id_aset);
                    $id_aset_index = str_replace('.', '', $id_aset_index);

                    $id_aset_index = intval($id_aset_index);
                    ++$id_aset_index;

                    $id_aset_index = strval($id_aset_index);
                    $s = strlen($id_aset_index);
                    if($s == 1) {
                        $id_aset_index = "0000".$id_aset_index;
                    } else if($s == 2) {
                        $id_aset_index = "000".$id_aset_index;
                    } else if($s == 3) {
                        $id_aset_index = "00".$id_aset_index;
                    } else if($s == 4) {
                        $id_aset_index = "0".$id_aset_index;
                    }
                }
            }

            $id_aset = $temp.".".$id_aset_index;

            $data_koreksi["no_register_baru"] = $id_aset;
            Rincian_koreksi::create($data_koreksi);           

            $input["kode_jurnal"] = $kode_jurnal;
            $input["no_key"] = $no_key;
            $input["nomor_lokasi"] = $nomor_lokasi;
            $input["id_aset"] = $id_aset;
            $input["tahun_spj"] = $tahun_spj;
            $input["tahun_perolehan"] = $tahun_spj;
            $input["kode_108"] = $kode_108_baru;
            $input["kode_64"] = $kode_64_baru;
            $input["jumlah_barang"] = $jumlah_barang;
            $input["saldo_barang"] = $jumlah_barang;
            $input["saldo_gudang"] = $jumlah_barang;
            $input["harga_total"] = $nilai;
            $input["harga_total_plus_pajak"] = $nilai;
            $input["harga_total_plus_pajak_saldo"] = $nilai;
            $change["keterangan"] = 'Hasil reklas RB sebagian';

            $input["baik"] = 0;
            $input["kb"] = 0;
            $input["rb"] = $jumlah_barang;

            $change["saldo_barang"] = $sisa;
            $change["saldo_gudang"] = $sisa;
            $change["harga_total"] = $nilai_sisa;
            $change["harga_total_plus_pajak"] = $nilai_sisa;
            $change["harga_total_plus_pajak_saldo"] = $nilai_sisa;
            $change["baik"] = $sisa;
            $change["sendiri"] = $sisa;
            $change["operator"] = 'SISFO';
            $change["keterangan"] = "Direklas sebagian ke aset rusak berat";

            $date = date('Y-m-d H:i:s');

            $input["created_at"] = $date;
            $input["updated_at"] = $date;

            if(!is_null($aset)) {
                array_push($migrated, $input);
                Kib::where("id_aset", $data_aset["id_aset"])->update($change);
                Kib::create($input);
            }
        }
        
        if(!empty($kosong)) {
            return $this->sendResponse($kosong, 'Migrasi data reklas sekda gagal.');
        } else {
            return $this->sendResponse($migrated, 'Sukses migrasi data reklas sekda.');
        }
    }

    public function sync(Request $request)
    {
        ini_set('memory_limit', '-1');
        $input = array();
        $jurnal = array();
        $kosong = array();
        $migrated = array();

        $data_reklas = Kib::where('reklas', 1)->get();

        foreach ($data_reklas as $value) {
            $value = json_decode(json_encode($value), true);
            
            $nomor_lokasi = $value["nomor_lokasi"];
            $no_register = $value["no_register"];
            $tahun_spj = 2019;
            $kode_jurnal = '336';
            $kode_sb_koreksi = "05";

            $kode_108_baru = $value["kode_108"];
            $kode_64_baru = $value["kode_64"];
            $kode_108 = $value["kode_asal"];

            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', $kode_108)->first();

            if(is_null($kode_64)) {
                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', (substr($kode_108, 0, 14)))->first();           
                if(is_null($kode_64)) {
                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', (substr($kode_108, 0, 11)))->first();
                }            
            }

            if(!is_null($kode_64)) {
                $kode_64 = $kode_64->kode_64;                
            }


            $data_koreksi["nomor_lokasi"] = $nomor_lokasi;
            $data_koreksi["kode_koreksi"] = $kode_jurnal;
            $data_koreksi["kode_sb_koreksi"] = $kode_sb_koreksi;
            $data_koreksi["no_key"] = $value["no_key"];
            $data_koreksi["bidang_barang"] = $value["bidang_barang"];
            $data_koreksi["id_aset"] = $value["id_aset"];
            $data_koreksi["no_ba_penerimaan"] = $value["no_ba_penerimaan"];
            $data_koreksi["no_register"] = $value["no_register"];
            $data_koreksi["no_label"] = $value["no_label"];
            $data_koreksi["nama_barang"] = $value["nama_barang"];
            $data_koreksi["merk_alamat"] = $value["merk_alamat"];
            $data_koreksi["kode_108"] = $kode_108;
            $data_koreksi["kode_108_baru"] = $kode_108_baru;
            $data_koreksi["kode_64"] = $kode_64;
            $data_koreksi["kode_64_baru"] = $kode_64_baru;

            $data_koreksi["kode_kepemilikan"] = $value["kode_kepemilikan"];
            $data_koreksi["jumlah_barang"] = $value["saldo_barang"];
            $data_koreksi["nilai"] = $value["harga_total_plus_pajak_saldo"];
            $data_koreksi["keterangan"] = "Koreksi Pengadaan";
            $data_koreksi["tahun_koreksi"] = 2019;

            if(!is_null($kode_64)) {
                array_push($migrated, $data_koreksi);
                Rincian_koreksi::create($data_koreksi);
            } else {
                array_push($kosong, $value["id_aset"]);
            }
        }
        
        if(!empty($kosong)) {
            return $this->sendResponse($kosong, 'Sinkronisasi data reklas gagal.');
        } else {
            return $this->sendResponse($migrated, 'Sukses sinkronisasi data reklas.');
        }
    }
}