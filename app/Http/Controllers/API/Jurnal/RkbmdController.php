<?php

namespace App\Http\Controllers\API\Jurnal;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Kib;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_sub_unit;
use App\Models\Kamus\Kamus_unit;
use Validator;

class RkbmdController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getListBy($nomor_lokasi)
    {
        //$pagination = (int)$request->header('Pagination');
        //$tahun_spj = date('Y') - 1;
        if(strlen($nomor_lokasi) <= 16) {
            $nama_lokasi = Kamus_unit::select('nama_unit')->where('nomor_unit', $nomor_lokasi)->first()->nama_unit;
        } else if(strlen($nomor_lokasi) > 16 && strlen($nomor_lokasi) <= 21) {
            $nama_lokasi = Kamus_sub_unit::select('nama_sub_unit')->where('nomor_sub_unit', $nomor_lokasi)->first()->nama_sub_unit;
        } else {
            $nama_lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', $nomor_lokasi)->first()->nama_lokasi;
        }
        $data = array();
        $jumlah_mobil = 0;
        $jumlah_motor = 0;

        $kode_motor = '1.3.2.02.01.04';
        $kode_mobil = '1.3.2.02.01.01';

        $kompi = Kib::selectRaw('kibs.kode_108, sum(kibs.saldo_barang) as jumlah')
            ->where('kibs.nomor_lokasi', 'like', $nomor_lokasi . '%')
            ->whereIn('kibs.kode_108', ['1.3.2.10.01.02.001', '1.3.2.10.01.02.002', '1.3.2.10.01.02.003', '1.3.2.10.01.03.003'])
            ->groupBy('kibs.kode_108')
            ->get()->toArray();

        $mobil = Kib::selectRaw('kibs.kode_108, sum(kibs.saldo_barang) as jumlah')
            ->where('kibs.nomor_lokasi', 'like', $nomor_lokasi . '%')
            ->where('kibs.kode_108', 'like', '1.3.2.02.01.01' . '%')
            ->groupBy('kibs.kode_108')
            ->get()->toArray();

        $motor = Kib::selectRaw('kibs.kode_108, sum(kibs.saldo_barang) as jumlah')
            ->where('kibs.nomor_lokasi', 'like', $nomor_lokasi . '%')
            ->where('kibs.kode_108', 'like', '1.3.2.02.01.04' . '%')
            ->groupBy('kibs.kode_108')
            ->get()->toArray();

        foreach ($mobil as $value) {
            $jumlah_mobil += $value['jumlah'];
        }

        $data_mobil = array(
            'nomor_lokasi' => $nomor_lokasi,
            'nama_lokasi' => $nama_lokasi,
            'kode_barang' => $kode_mobil,
            'uraian' => 'Kendaraan Dinas Bermotor Perorangan',
            'jumlah' => $jumlah_mobil
        );

        array_push($data, $data_mobil);

        foreach ($motor as $value) {
            $jumlah_motor += $value['jumlah'];
        }

        $data_motor = array(
            'nomor_lokasi' => $nomor_lokasi,
            'nama_lokasi' => $nama_lokasi,
            'kode_barang' => $kode_motor,
            'uraian' => 'Kendaraan Bermotor Beroda Dua',
            'jumlah' => $jumlah_motor
        );

        array_push($data, $data_motor);

        foreach ($kompi as $value) {
            if($value['kode_108'] == '1.3.2.10.01.02.001') {
                $uraian_108 = 'PC Unit';
            } else if($value['kode_108'] == '1.3.2.10.01.02.002') {
                $uraian_108 = 'Laptop';
            } else if($value['kode_108'] == '1.3.2.10.01.02.003') {
                $uraian_108 = 'Notebook';
            } else if($value['kode_108'] == '1.3.2.10.01.03.003') {
                $uraian_108 = 'Printer';
            } 

            $temp = array(
                'nomor_lokasi' => $nomor_lokasi,
                'nama_lokasi' => $nama_lokasi,
                'kode_barang' => $value['kode_108'],
                'uraian' => $uraian_108,
                'jumlah' => $value['jumlah']
            );
            array_push($data, $temp);
        }

        $export = collect($data);

        return $this->sendResponse($export, 'Aset retrieved successfully.');
    }

    public function getAll()
    {
        ini_set('max_execution_time', 1800);
        
        $units = Kamus_unit::select('nomor_unit', 'nama_unit')->get()->toArray();
        $data = array();

        foreach ($units as $value) {
            $nomor_unit = $value['nomor_unit'];
            $nama_unit = $value['nama_unit'];

            $jumlah_mobil = 0;
            $jumlah_motor = 0;

            $kode_motor = '1.3.2.02.01.04';
            $kode_mobil = '1.3.2.02.01.01';

            $kompi = Kib::selectRaw('kibs.kode_108, sum(kibs.saldo_barang) as jumlah')
                ->where('kibs.nomor_lokasi', 'like', $nomor_unit . '%')
                ->whereIn('kibs.kode_108', ['1.3.2.10.01.02.001', '1.3.2.10.01.02.002', '1.3.2.10.01.02.003', '1.3.2.10.01.03.003'])
                ->groupBy('kibs.kode_108')
                ->get()->toArray();

            $mobil = Kib::selectRaw('kibs.kode_108, sum(kibs.saldo_barang) as jumlah')
                ->where('kibs.nomor_lokasi', 'like', $nomor_unit . '%')
                ->where('kibs.kode_108', 'like', '1.3.2.02.01.01' . '%')
                ->groupBy('kibs.kode_108')
                ->get()->toArray();

            $motor = Kib::selectRaw('kibs.kode_108, sum(kibs.saldo_barang) as jumlah')
                ->where('kibs.nomor_lokasi', 'like', $nomor_unit . '%')
                ->where('kibs.kode_108', 'like', '1.3.2.02.01.04' . '%')
                ->groupBy('kibs.kode_108')
                ->get()->toArray();

            foreach ($mobil as $value) {
                $jumlah_mobil += $value['jumlah'];
            }

            $data_mobil = array(
                'nomor_unit' => $nomor_unit,
                'nama_unit' => $nama_unit,
                'kode_barang' => $kode_mobil,
                'uraian' => 'Kendaraan Dinas Bermotor Perorangan',
                'jumlah' => $jumlah_mobil
            );

            array_push($data, $data_mobil);

            foreach ($motor as $value) {
                $jumlah_motor += $value['jumlah'];
            }

            $data_motor = array(
                'nomor_unit' => $nomor_unit,
                'nama_unit' => $nama_unit,
                'kode_barang' => $kode_motor,
                'uraian' => 'Kendaraan Bermotor Beroda Dua',
                'jumlah' => $jumlah_motor
            );

            array_push($data, $data_motor);

            foreach ($kompi as $value) {
                if($value['kode_108'] == '1.3.2.10.01.02.001') {
                    $uraian_108 = 'PC Unit';
                } else if($value['kode_108'] == '1.3.2.10.01.02.002') {
                    $uraian_108 = 'Laptop';
                } else if($value['kode_108'] == '1.3.2.10.01.02.003') {
                    $uraian_108 = 'Notebook';
                } else if($value['kode_108'] == '1.3.2.10.01.03.003') {
                    $uraian_108 = 'Printer';
                } 

                $temp = array(
                    'nomor_unit' => $nomor_unit,
                    'nama_unit' => $nama_unit,
                    'kode_barang' => $value['kode_108'],
                    'uraian' => $uraian_108,
                    'jumlah' => $value['jumlah']
                );
                array_push($data, $temp);
            }
        }

        $export = collect($data);
        return $this->sendResponse($export, 'Aset RKBMD retrieved successfully.');
    }

}