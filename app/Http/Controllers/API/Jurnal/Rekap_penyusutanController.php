<?php

namespace App\Http\Controllers\API\Jurnal;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Tahun;
use App\Models\Jurnal\Penyusutan;
use App\Models\Jurnal\Rekap_penyusutan;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Masa_tambahan;
use App\Http\Resources\Jurnal\RehabCollection;
use App\Http\Resources\Jurnal\KibCollection;
use Validator;

class Rekap_penyusutanController extends BaseController
{

    public function generate()
    {
        $daftar_lokasi = Kamus_lokasi::select('nomor_lokasi')->get()->toArray();

        foreach ($daftar_lokasi as $value) {
            $data = array(
                "nomor_lokasi" => $value['nomor_lokasi'],
                "a" => 0,
                "b" => 0,
                "c" => 0,
                "d" => 0,
                "e" => 0,
                "f" => 0,
                "g" => 0,
                "r" => 0
            );

            Rekap_penyusutan::create($data);
        }

        return $daftar_lokasi;
    }

    public function add($nomor_lokasi, $kib, $data)
    {
        $rekap = Rekap_penyusutan::where('nomor_lokasi', $nomor_lokasi)->first();
        $data_a = 0;
        $data_b = 0;
        $data_c = 0;
        $data_d = 0;
        $data_e = 0;
        $data_f = 0;
        $data_g = 0;
        $data_r = 0;

        if ($kib == 'b') {
            $data_b = $data;
        } else if ($kib == 'c') {
            $data_c = $data;
        } else if ($kib == 'd') {
            $data_d = $data;
        } else if ($kib == 'e') {
            $data_e = $data;
        } else if ($kib == 'f') {
            $data_f = $data;
        } else if ($kib == 'g') {
            $data_g = $data;
        } else if ($kib == 'r') {
            $data_r = $data;
        }
        
        if(!empty($rekap)) {
            $rekap->a += $data_a;
            $rekap->b += $data_b;
            $rekap->c += $data_c;
            $rekap->d += $data_d;
            $rekap->e += $data_e;
            $rekap->f += $data_f;
            $rekap->g += $data_g;
            $rekap->r += $data_r;

            $array = json_decode(json_encode($rekap), true);
            $update = Rekap_penyusutan::where('nomor_lokasi', $nomor_lokasi)->update($array);
        } 
    }

    public function substract($nomor_lokasi, $kib, $data)
    {
        $rekap = Rekap_penyusutan::where('nomor_lokasi', $nomor_lokasi)->first();
        $data_a = 0;
        $data_b = 0;
        $data_c = 0;
        $data_d = 0;
        $data_e = 0;
        $data_f = 0;
        $data_g = 0;
        $data_r = 0;

        if ($kib == 'b') {
            $data_b = $data;
        } else if ($kib == 'c') {
            $data_c = $data;
        } else if ($kib == 'd') {
            $data_d = $data;
        } else if ($kib == 'e') {
            $data_e = $data;
        } else if ($kib == 'f') {
            $data_f = $data;
        } else if ($kib == 'g') {
            $data_g = $data;
        } else if ($kib == 'r') {
            $data_r = $data;
        }
        
        if(!empty($rekap)) {
            $rekap->a += $data_a;
            $rekap->b += $data_b;
            $rekap->c += $data_c;
            $rekap->d += $data_d;
            $rekap->e += $data_e;
            $rekap->f += $data_f;
            $rekap->g += $data_g;
            $rekap->r += $data_r;

            $array = json_decode(json_encode($rekap), true);
            $update = Rekap_penyusutan::where('nomor_lokasi', $nomor_lokasi)->update($array);
        } 
    }
}