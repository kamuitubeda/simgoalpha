<?php

namespace App\Http\Controllers\API\Jurnal;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Tahun;
use App\Models\Jurnal\Penyusutan;
use App\Models\Jurnal\Rekap_perolehan;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Masa_tambahan;
use App\Http\Resources\Jurnal\RehabCollection;
use App\Http\Resources\Jurnal\KibCollection;
use Validator;

class Rekap_perolehanController extends BaseController
{

    public function plus($id_aset)
    {
        $aset = Kib::where('id_aset', $id_aset)->first();
        $rekap = Rekap_perolehan::where('nomor_lokasi', $nomor_lokasi)->first();

        $kib = substr($aset->kode_108, 0, 5);

        if($kib == '1.3.1') {
            $rekap->a += $data;
        } else if ($kib == '1.3.2') {
            $rekap->b += $data;
        } else if ($kib == '1.3.3') {
            $rekap->c += $data;
        } else if ($kib == '1.3.4') {
            $rekap->d += $data;
        } else if ($kib == '1.3.5') {
            $rekap->e += $data;
        } else if ($kib == '1.3.6') {
            $rekap->f += $data;
        } else if ($kib == '1.5.3') {
            $rekap->g += $data;
        } else if ($kib == '1.5.5') {
            $rekap->r += $data;
        }

        $update = Rekap_perolehan::where('nomor_lokasi', $nomor_lokasi)->update($rekap);
    }

    public function minus($id_aset)
    {
        $aset = Kib::where('id_aset', $id_aset)->first();
        $rekap = Rekap_perolehan::where('nomor_lokasi', $nomor_lokasi)->first();

        $kib = substr($aset->kode_108, 0, 5);
        
        if($kib == '1.3.1') {
            $rekap->a -= $data;
        } else if ($kib == '1.3.2') {
            $rekap->b -= $data;
        } else if ($kib == '1.3.3') {
            $rekap->c -= $data;
        } else if ($kib == '1.3.4') {
            $rekap->d -= $data;
        } else if ($kib == '1.3.5') {
            $rekap->e -= $data;
        } else if ($kib == '1.3.6') {
            $rekap->f -= $data;
        } else if ($kib == '1.5.3') {
            $rekap->g -= $data;
        } else if ($kib == '1.5.5') {
            $rekap->r -= $data;
        }

        $update = Rekap_perolehan::where('nomor_lokasi', $nomor_lokasi)->update($rekap);
    }
}