<?php

namespace App\Http\Controllers\API\Jurnal;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Controllers\API\Jurnal\Rekap_penyusutanController;
use App\Models\Jurnal\Rehab;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rincian_masuk;
use App\Models\Jurnal\Tahun;
use App\Models\Jurnal\Penyusutan;
use App\Models\Jurnal\Rekap_penyusutan;
use App\Models\Kamus\Sub_sub_rincian_108;
use App\Models\Kamus\Rincian_108;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Masa_tambahan;
use App\Http\Resources\Jurnal\RehabCollection;
use App\Http\Resources\Jurnal\KibCollection;
use Validator;

class PenyusutanController extends BaseController
{
    //untuk generate penyusutan secara umum
    public function global(Request $request)
    {
        $pagination = (int)$request->header('Pagination');
        $input = $request->all();

        ini_set('max_execution_time', 0);

        $data = Kib::where("saldo_barang", '>', 0)
        ->where('tahun_spj', '=', 2020)
        ->get()
        ->toArray();   

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        $email = Auth::guard('api')->user()->email;
        if($email == 'sup@mojokerto.go.id') {
            $tahun_admin = (int)Tahun::select('tahun_admin')->first()->tahun_admin;
            $tahun_acuan = $tahun_admin;
        }
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;
            $kdp = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom) {
                continue;
            }

            $kode = substr($value["kode_108"], 0, 5);
            if($kode == '1.3.1') {
                $kib = 'a';
            } else if($kode == '1.3.2') {
                $kib = 'b';
            } else if($kode == '1.3.3') {
                $kib = 'c';
            } else if($kode == '1.3.4') {
                $kib = 'd';
            } else if($kode == '1.3.5') {
                $kib = 'e';
            } else if($kode == '1.3.6') {
                $kib = 'f';
            } else if($kode == '1.5.3') {
                $kib = 'g';
            } else if($kode == '1.5.4') {
                $kib = 'r';
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if($value['bidang_barang'] == "A") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if($value['bidang_barang'] == "B") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['bidang_barang'] == "C" || $value['bidang_barang'] == "D") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['bidang_barang'] == "G") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(strpos($value['bidang_barang'], 'E') !== false) {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(strpos($value['bidang_barang'], 'F') !== false) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                    $kdp = true;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($aset_induk) {
                //untuk mengambil aset rehab berdasarkan id aset induk
                $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                ->where('rehabs.aset_induk_id', $value['id_aset'])
                ->orderBy('kibs.tahun_pengadaan', 'asc')->get();

                //$rehabs = Rehab::where("aset_induk_id", $value["id_aset"])->orderBy('tahun_rehab', 'asc')->get();

                if(!empty($rehabs)) {
                    $detail_penyusutan = $rehabs->toArray();
                }

                $count = sizeof($detail_penyusutan);

                $status_aset = 1;

                $jumlah_barang_tmp = $value["saldo_barang"];
                $tahun_pengadaan_tmp = intval($value["tahun_pengadaan"]);
                $nilai_pengadaan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $persen = 0;
                $penambahan_nilai = 0;
                $masa_tambahan = 0;
                $masa_terpakai_tmp = 0;
                $sisa_masa_tmp = $masa_manfaat;
                $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
                $akumulasi_penyusutan_tmp = 0;
                $akumulasi_penyusutan_berjalan_tmp = 0;
                $nilai_buku_tmp = 0;
                $index = 0;
                $k = 0;

                for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                    $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                    if($th == $tahun_rehab) {
                        $jumlah_renov_tahunx = 0;
                        $tahun_pembanding = 0;

                        if($tahun_pengadaan_tmp == $tahun_rehab) {
                            $nilai_buku_tmp = $nilai_pengadaan_tmp;
                        }

                        for ($x = 0; $x < $count; $x++) {
                             $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                             if($tahun_pembanding === $tahun_rehab) {
                                $jumlah_renov_tahunx++;
                             }
                        }

                        if($jumlah_renov_tahunx == 1) {
                            $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++$masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                            if($sisa_masa_tmp > 0) {
                                $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            } else {
                                $penyusutan_per_tahun_tmp = 0;
                            }
                            
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            --$sisa_masa_tmp;
                            $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                            $aset_susut[$i++] =
                            array(
                                "id_aset" => $value["id_aset"],
                                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                                "nilai_buku" => $nilai_buku
                            );

                            if($index < $count-1) {
                                $index++;
                            }
                        } else {
                            $penambahan_nilai = 0;
                            for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                                $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                                if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                    $index++;
                                }
                            }

                            $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++ $masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $penambahan_nilai;
                            $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            $nilai_pengadaan_tmp += $penambahan_nilai;

                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                            if($index < $count-1) {
                                $index++;
                            }
                        }
                    } else {
                        ++$masa_terpakai_tmp;
                        --$sisa_masa_tmp;

                        $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                        $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                        $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                        if($masa_terpakai_tmp > $masa_manfaat) {
                            $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                            $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                            $nilai_buku_tmp = 0;
                            $sisa_masa_tmp = 0;
                        } 
                    }
                }
                
                $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
                $jumlah_barang += $jumlah_barang_tmp;

                if($value['bidang_barang'] == "A" || $kib_e == true || $kdp == true) {
                    $akumulasi_penyusutan_tmp = 0;
                    $akumulasi_penyusutan_berjalan_tmp = 0;
                    $penyusutan_per_tahun_tmp = 0;
                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                    $masa_manfaat = 0;
                    $akumulasi_penyusutan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_pengadaan_tmp;
                }

                $induk = Kib::where('id_aset', 'like', $value['id_aset'])->first();

                $induk->ak_penyusutan = $akumulasi_penyusutan_tmp;
                $induk->nilai_buku = $nilai_buku_tmp;

                $induk = json_decode(json_encode($induk), true);

                $penyusutan = array(
                    "id_aset" => $value['id_aset'],
                    "nilai_perolehan" => $nilai_pengadaan_tmp,
                    "masa_manfaat" => $masa_manfaat,
                    "masa_terpakai" => $masa_terpakai_tmp,
                    "masa_sisa" => $sisa_masa_tmp,
                    "beban" => $penyusutan_per_tahun_tmp,
                    "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                    "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan_tmp,
                    "nilai_buku" => $nilai_buku_tmp
                );

                $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->first();
                $rp = new Rekap_penyusutanController();

                if(!is_null($laporan_penyusutan)) {
                    $akumulasi = $laporan_penyusutan->akumulasi_penyusutan_berjalan;

                    //$rp->substract($value['nomor_lokasi'], $kib, $akumulasi);  
                    $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->update($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan_tmp);
                } else {
                    $susutkan = Penyusutan::create($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan_tmp);
                }

                $Kib_induk = Kib::where('id_aset', $value['id_aset'])->update($induk);

                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                    "nilai_buku" => $nilai_buku_tmp
                );
            } else {
                $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
                $masa_sisa = $masa_manfaat - $masa_terpakai;
                $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $status_aset = 0;
                $jumlah_barang_tmp = $value["saldo_barang"];

                if($value['bidang_barang'] == "A" || $kib_e == true || $kdp == true) {
                    $akumulasi_penyusutan = 0;
                    $akumulasi_penyusutan_berjalan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_perolehan_tmp;
                } else {
                    if($masa_terpakai > $masa_manfaat) {
                        $nilai_perolehan = $nilai_perolehan_tmp;
                        $akumulasi_penyusutan = $nilai_perolehan_tmp;
                        $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                        $masa_sisa = 0;
                        $beban = 0;
                        $nilai_buku = 0;
                    } else {
                        $nilai_perolehan = $nilai_perolehan_tmp;
                        $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                        $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                        $beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
                        $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                    }
                }

                $jumlah_barang += $jumlah_barang_tmp;

                $induk = Kib::where('id_aset', 'like', $value['id_aset'])->first();

                $induk->ak_penyusutan = $akumulasi_penyusutan;
                $induk->nilai_buku = $nilai_buku;

                $induk = json_decode(json_encode($induk), true);

                $penyusutan = array(
                    "id_aset" => $value['id_aset'],
                    "nilai_perolehan" => $nilai_perolehan_tmp,
                    "masa_manfaat" => $masa_manfaat,
                    "masa_terpakai" => $masa_terpakai,
                    "masa_sisa" => $masa_sisa,
                    "beban" => $beban,
                    "akumulasi_penyusutan" => $akumulasi_penyusutan,
                    "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan,
                    "nilai_buku" => $nilai_buku
                );

                $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->first();
                $rp = new Rekap_penyusutanController();

                if(!is_null($laporan_penyusutan)) {
                    $akumulasi = $laporan_penyusutan->akumulasi_penyusutan_berjalan;

                    //$rp->substract($value['nomor_lokasi'], $kib, $akumulasi);  
                    $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->update($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan);
                } else {
                    $susutkan = Penyusutan::create($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan);
                }

                $Kib_induk = Kib::where('id_aset', $value['id_aset'])->update($induk);

                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan,
                    "nilai_buku" => $nilai_buku
                );
            }
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    //untuk generate penyusutan secara umum
    public function local(Request $request, $nomor_lokasi)
    {
        $pagination = (int)$request->header('Pagination');
        $input = $request->all();

        ini_set('max_execution_time', 0);

        $data = Kib::where("nomor_lokasi", 'like', $nomor_lokasi . '%')
        ->where("saldo_barang", '>', 0)
        ->get()
        ->toArray();   

        $nama_unit = Kamus_lokasi::select("nama_lokasi")->where("nomor_lokasi", 'like', $nomor_lokasi . "%")->first();

        if(!is_null($nama_unit)) {
            $nama_unit = $nama_unit->nama_lokasi;
        } else {
            $nama_unit = "";
        }

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;
        $email = Auth::guard('api')->user()->email;
        if($email == 'sup@mojokerto.go.id') {
            $tahun_admin = (int)Tahun::select('tahun_admin')->first()->tahun_admin;
            $tahun_acuan = $tahun_admin;
        }
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $kode = substr($value["kode_108"], 0, 5);
            if($kode == '1.3.1') {
                $kib = 'a';
            } else if($kode == '1.3.2') {
                $kib = 'b';
            } else if($kode == '1.3.3') {
                $kib = 'c';
            } else if($kode == '1.3.4') {
                $kib = 'd';
            } else if($kode == '1.3.5') {
                $kib = 'e';
            } else if($kode == '1.3.6') {
                $kib = 'f';
            } else if($kode == '1.5.3') {
                $kib = 'g';
            } else if($kode == '1.5.5') {
                $kib = 'r';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;
            $kdp = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom) {
                continue;
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if($value['bidang_barang'] == "A") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if($value['bidang_barang'] == "B") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['bidang_barang'] == "C" || $value['bidang_barang'] == "D") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['bidang_barang'] == "G") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(strpos($value['bidang_barang'], 'E') !== false) {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(strpos($value['bidang_barang'], 'F') !== false) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                    $kdp = true;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($aset_induk) {
                //untuk mengambil aset rehab berdasarkan id aset induk
                $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                ->where('rehabs.aset_induk_id', $value['id_aset'])
                ->orderBy('kibs.tahun_pengadaan', 'asc')->get();

                //$rehabs = Rehab::where("aset_induk_id", $value["id_aset"])->orderBy('tahun_rehab', 'asc')->get();

                if(!empty($rehabs)) {
                    $detail_penyusutan = $rehabs->toArray();
                }

                $count = sizeof($detail_penyusutan);

                $status_aset = 1;

                $jumlah_barang_tmp = $value["saldo_barang"];
                $tahun_pengadaan_tmp = intval($value["tahun_pengadaan"]);
                $nilai_pengadaan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $persen = 0;
                $penambahan_nilai = 0;
                $masa_tambahan = 0;
                $masa_terpakai_tmp = 0;
                $sisa_masa_tmp = $masa_manfaat;
                $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
                $akumulasi_penyusutan_tmp = 0;
                $akumulasi_penyusutan_berjalan_tmp = 0;
                $nilai_buku_tmp = 0;
                $index = 0;
                $k = 0;

                for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                    $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                    if($th == $tahun_rehab) {
                        $jumlah_renov_tahunx = 0;
                        $tahun_pembanding = 0;

                        if($tahun_pengadaan_tmp == $tahun_rehab) {
                            $nilai_buku_tmp = $nilai_pengadaan_tmp;
                        }

                        for ($x = 0; $x < $count; $x++) {
                             $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                             if($tahun_pembanding === $tahun_rehab) {
                                $jumlah_renov_tahunx++;
                             }
                        }

                        if($jumlah_renov_tahunx == 1) {
                            $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++$masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                            if($sisa_masa_tmp > 0) {
                                $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            } else {
                                $penyusutan_per_tahun_tmp = 0;
                            }
                            
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            --$sisa_masa_tmp;
                            $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                            $aset_susut[$i++] =
                            array(
                                "id_aset" => $value["id_aset"],
                                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                                "nilai_buku" => $nilai_buku
                            );

                            if($index < $count-1) {
                                $index++;
                            }
                        } else {
                            $penambahan_nilai = 0;
                            for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                                $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                                if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                    $index++;
                                }
                            }

                            $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;
                            //$persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++ $masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $penambahan_nilai;
                            $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            $nilai_pengadaan_tmp += $penambahan_nilai;

                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                            if($index < $count-1) {
                                $index++;
                            }
                        }
                    } else {
                        ++$masa_terpakai_tmp;
                        --$sisa_masa_tmp;

                        $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                        $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                        $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                        if($masa_terpakai_tmp > $masa_manfaat) {
                            $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                            $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                            $nilai_buku_tmp = 0;
                            $sisa_masa_tmp = 0;
                        } 
                    }
                }
                
                $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
                $jumlah_barang += $jumlah_barang_tmp;

                if($value['bidang_barang'] == "A" || $kib_e == true || $kdp == true) {
                    $akumulasi_penyusutan_tmp = 0;
                    $akumulasi_penyusutan_berjalan_tmp = 0;
                    $penyusutan_per_tahun_tmp = 0;
                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                    $masa_manfaat = 0;
                    $akumulasi_penyusutan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_pengadaan_tmp;
                }

                $induk = Kib::where('id_aset', 'like', $value['id_aset'])->first();

                $induk->ak_penyusutan = $akumulasi_penyusutan_tmp;
                $induk->nilai_buku = $nilai_buku_tmp;

                $induk = json_decode(json_encode($induk), true);

                $penyusutan = array(
                    "id_aset" => $value['id_aset'],
                    "nilai_perolehan" => $nilai_pengadaan_tmp,
                    "masa_manfaat" => $masa_manfaat,
                    "masa_terpakai" => $masa_terpakai_tmp,
                    "masa_sisa" => $sisa_masa_tmp,
                    "beban" => $penyusutan_per_tahun_tmp,
                    "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                    "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan_tmp,
                    "nilai_buku" => $nilai_buku_tmp
                );

                $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->first();
                $rp = new Rekap_penyusutanController();

                if(!is_null($laporan_penyusutan)) {
                    $akumulasi = $laporan_penyusutan->akumulasi_penyusutan_berjalan;

                    //$rp->substract($value['nomor_lokasi'], $kib, $akumulasi);  
                    $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->update($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan_tmp);
                } else {
                    $susutkan = Penyusutan::create($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan_tmp);
                }

                $Kib_induk = Kib::where('id_aset', $value['id_aset'])->update($induk);

                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                    "nilai_buku" => $nilai_buku_tmp
                );
            } else {
                $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
                $masa_sisa = $masa_manfaat - $masa_terpakai;
                $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $status_aset = 0;
                $jumlah_barang_tmp = $value["saldo_barang"];

                if($value['bidang_barang'] == "A" || $kib_e == true || $kdp == true) {
                    $akumulasi_penyusutan = 0;
                    $akumulasi_penyusutan_berjalan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_perolehan_tmp;
                } else {
                    if($masa_terpakai > $masa_manfaat) {
                        $nilai_perolehan = $nilai_perolehan_tmp;
                        $akumulasi_penyusutan = $nilai_perolehan_tmp;
                        $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                        $masa_sisa = 0;
                        $beban = 0;
                        $nilai_buku = 0;
                    } else {
                        $nilai_perolehan = $nilai_perolehan_tmp;
                        $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                        $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                        $beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
                        $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                    }
                }

                $jumlah_barang += $jumlah_barang_tmp;

                $induk = Kib::where('id_aset', 'like', $value['id_aset'])->first();

                $induk->ak_penyusutan = $akumulasi_penyusutan;
                $induk->nilai_buku = $nilai_buku;

                $induk = json_decode(json_encode($induk), true);

                $penyusutan = array(
                    "id_aset" => $value['id_aset'],
                    "nilai_perolehan" => $nilai_perolehan_tmp,
                    "masa_manfaat" => $masa_manfaat,
                    "masa_terpakai" => $masa_terpakai,
                    "masa_sisa" => $masa_sisa,
                    "beban" => $beban,
                    "akumulasi_penyusutan" => $akumulasi_penyusutan,
                    "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan,
                    "nilai_buku" => $nilai_buku
                );

                $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->first();
                $rp = new Rekap_penyusutanController();

                if(!is_null($laporan_penyusutan)) {
                    $akumulasi = $laporan_penyusutan->akumulasi_penyusutan_berjalan;

                    //$rp->substract($value['nomor_lokasi'], $kib, $akumulasi);  
                    $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->update($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan);
                } else {
                    $susutkan = Penyusutan::create($penyusutan);
                    //$rp->add($value['nomor_lokasi'], $kib, $akumulasi_penyusutan_berjalan);
                }

                $Kib_induk = Kib::where('id_aset', $value['id_aset'])->update($induk);

                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan,
                    "nilai_buku" => $nilai_buku
                );
            }
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    //untuk generate penyusutan secara umum
    public function sync(Request $request)
    {
        $pagination = (int)$request->header('Pagination');
        $input = $request->all();

        ini_set('max_execution_time', 0);

        $induk = Rehab::select('aset_induk_id as id_aset')->distinct()->get()->toArray();

        // $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
        //         ->select('penyusutans.id_aset')
        //         ->where('penyusutans.nilai_perolehan', '<>', 'kibs.harga_total_plus_pajak_saldo')
        //         ->get();

        $data = DB::select(DB::raw('select k.* from penyusutans p
                                    join kibs k on k.id_aset = p.id_aset
                                    left join rehabs r on r.aset_induk_id = p.id_aset
                                    where p.nilai_perolehan <> k.harga_total_plus_pajak_saldo
                                    and r.aset_induk_id is null'));

        if(empty($data)) {
            return 'sudah sinkron';
        }

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $value = (array)$value;

            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;
            $kdp = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong) {
                $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->delete();
            }

            if($aset_rehab || $pakai_habis || $ekstrakom) {
                continue;
            }

            $kode = substr($value["kode_108"], 0, 5);
            if($kode == '1.3.1') {
                $kib = 'a';
            } else if($kode == '1.3.2') {
                $kib = 'b';
            } else if($kode == '1.3.3') {
                $kib = 'c';
            } else if($kode == '1.3.4') {
                $kib = 'd';
            } else if($kode == '1.3.5') {
                $kib = 'e';
            } else if($kode == '1.3.6') {
                $kib = 'f';
            } else if($kode == '1.5.3') {
                $kib = 'g';
            } else if($kode == '1.5.4') {
                $kib = 'r';
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if($value['bidang_barang'] == "A") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if($value['bidang_barang'] == "B") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['bidang_barang'] == "C" || $value['bidang_barang'] == "D") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['bidang_barang'] == "G") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(strpos($value['bidang_barang'], 'E') !== false) {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(strpos($value['bidang_barang'], 'F') !== false) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                    $kdp = true;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
            $masa_sisa = $masa_manfaat - $masa_terpakai;
            $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
            $status_aset = 0;
            $jumlah_barang_tmp = $value["saldo_barang"];

            if($value['bidang_barang'] == "A" || $kib_e == true || $kdp == true) {
                $akumulasi_penyusutan = 0;
                $akumulasi_penyusutan_berjalan = 0;
                $beban = 0;
                $nilai_buku = $nilai_perolehan_tmp;
            } else {
                if($masa_terpakai > $masa_manfaat) {
                    $nilai_perolehan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                    $masa_sisa = 0;
                    $beban = 0;
                    $nilai_buku = 0;
                } else {
                    $nilai_perolehan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                    $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                    $beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
                    $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                }
            }

            $jumlah_barang += $jumlah_barang_tmp;

            $induk = Kib::where('id_aset', 'like', $value['id_aset'])->first();

            $induk->ak_penyusutan = $akumulasi_penyusutan;
            $induk->nilai_buku = $nilai_buku;

            $induk = json_decode(json_encode($induk), true);

            $penyusutan = array(
                "id_aset" => $value['id_aset'],
                "nilai_perolehan" => $nilai_perolehan_tmp,
                "masa_manfaat" => $masa_manfaat,
                "masa_terpakai" => $masa_terpakai,
                "masa_sisa" => $masa_sisa,
                "beban" => $beban,
                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan,
                "nilai_buku" => $nilai_buku
            );

            $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->first();

            if(!is_null($laporan_penyusutan)) {
                $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->update($penyusutan);
            } else {
                $susutkan = Penyusutan::create($penyusutan);
            }

            $Kib_induk = Kib::where('id_aset', $value['id_aset'])->update($induk);

            $aset_susut[$i++] =
            array(
                "id_aset" => $value["id_aset"],
                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                "nilai_buku" => $nilai_buku
            );
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    //untuk generate penyusutan antara induk dan rehab
    public function gabungkan($induk_id, $rehab_id)
    {
        //GENERATE PENYUSUTAN
        $aset_rehab = Kib::where('id_aset', $rehab_id)->first();
        $aset_induk = Kib::where('id_aset', $induk_id)->first();

        $tahun_acuan = (int)Tahun::select('tahun_spj')->first()->tahun_spj;
        $email = Auth::guard('api')->user()->email;
        if($email == 'sup@mojokerto.go.id') {
            $tahun_admin = (int)Tahun::select('tahun_admin')->first()->tahun_admin;
            $tahun_acuan = $tahun_admin;
        }
        $jumlah_barang = 0;
        $jumlah_rehab = 0;
        $masa_terpakai = 0;
        $kdp = false;

        $kode = substr($aset_induk->kode_108, 0, 5);
        if($kode == '1.3.1') {
            $kib = 'a';
        } else if($kode == '1.3.2') {
            $kib = 'b';
        } else if($kode == '1.3.3') {
            $kib = 'c';
        } else if($kode == '1.3.4') {
            $kib = 'd';
        } else if($kode == '1.3.5') {
            $kib = 'e';
        } else if($kode == '1.3.6') {
            $kib = 'f';
        } else if($kode == '1.5.3') {
            $kib = 'g';
        } else if($kode == '1.5.5') {
            $kib = 'r';
        }

        $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $aset_induk->kode_64)->first();
        if(!is_null($mm)) {
            if($mm->masa_manfaat == 0) {
                $masa_manfaat = 0;
                $mm_induk = 0;
            } else {
                $masa_manfaat = (int)$mm->masa_manfaat;
                $mm_induk = (int)$mm->masa_manfaat;
            }
        } else {
            if($aset_induk->bidang_barang == "A") {
                $masa_manfaat = 0;
                $mm_induk = 0;
            } else if($aset_induk->bidang_barang == "B") {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if($aset_induk->bidang_barang == "C" || $aset_induk->bidang_barang == "D") {
                $masa_manfaat = 50;
                $mm_induk = 50;
            } else if($aset_induk->bidang_barang == "G") {
                $masa_manfaat = 4;
                $mm_induk = 4;
            } else if(strpos($aset_induk->bidang_barang, 'E') !== false) {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if(strpos($aset_induk->bidang_barang, 'F') !== false) {
                $masa_manfaat = 0;
                $mm_induk = 0;
                $kdp = true;
            }
        }

        if($masa_manfaat == 0) {
            if($aset_induk->kode_108 == '1.5.4.01.01.01.002') {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if($aset_induk->kode_108 == '1.5.4.01.01.01.003') {
                $masa_manfaat = 50;
                $mm_induk = 50;
            } else if($aset_induk->kode_108 == '1.5.4.01.01.01.004') {
                $masa_manfaat = 40;
                $mm_induk = 40;
            } else if($aset_induk->kode_108 == '1.5.4.01.01.01.005') {
                $masa_manfaat = 5;
                $mm_induk = 5;
            }
        }

        $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                        ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.saldo_barang', 'kibs.tahun_pengadaan', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                        ->where('rehabs.aset_induk_id', $induk_id)
                        ->orderBy('kibs.tahun_pengadaan', 'asc')->get();

        if(!empty($rehabs)) {
            $detail_penyusutan = $rehabs->toArray();
        }

        $count = sizeof($detail_penyusutan);

        $status_aset = 1;

        $jumlah_barang_tmp = $aset_induk->saldo_barang;
        $tahun_pengadaan_tmp = intval($aset_induk->tahun_pengadaan);
        $nilai_pengadaan_tmp = floatval($aset_induk->harga_total_plus_pajak_saldo);
        $persen = 0;
        $penambahan_nilai = 0;
        $masa_tambahan = 0;
        $masa_terpakai_tmp = 0;
        $sisa_masa_tmp = $masa_manfaat;
        $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
        $akumulasi_penyusutan_tmp = 0;
        $akumulasi_penyusutan_berjalan_tmp = 0;
        $nilai_buku_tmp = 0;
        $index = 0;
        $k = 0;

        for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
            $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
            if($th == $tahun_rehab) {
                $jumlah_renov_tahunx = 0;
                $tahun_pembanding = 0;

                if($tahun_pengadaan_tmp == $tahun_rehab) {
                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                }

                for ($x = 0; $x < $count; $x++) {
                     $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                     if($tahun_pembanding === $tahun_rehab) {
                        $jumlah_renov_tahunx++;
                     }
                }

                if($jumlah_renov_tahunx == 1) {
                    $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                    $persen = (int)$persen;
                    $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                    if(is_null($kode_64)) {
                        $kode_108 = substr($kode_108, 0, 14);
                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                        if(is_null($kode_64)) {
                            $kode_108 = substr($kode_108, 0, 11);
                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                        } else {
                            $kode_64 = $kode_64->kode_64;
                        }
                    } else {
                        $kode_64 = $kode_64->kode_64;
                    }

                    $kode_64 = substr($kode_64, 0, 8);
                    $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                    if(!is_null($masa_tambah)) {
                        $masa_tambahan = $masa_tambah->masa_tambahan;
                    }

                    if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                        $sisa_masa_tmp += $masa_tambahan;
                        if($sisa_masa_tmp > $mm_induk) {
                            $masa_manfaat = $mm_induk;
                            $sisa_masa_tmp = $mm_induk;
                        } else {
                            $masa_manfaat = $sisa_masa_tmp;
                        }
                        $masa_terpakai_tmp = 1;
                    } else {
                        ++$masa_terpakai_tmp;
                    }

                    $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                    if($sisa_masa_tmp > 0) {
                        $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                    } else {
                        $penyusutan_per_tahun_tmp = 0;
                    }
                    
                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                    $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                    --$sisa_masa_tmp;
                    $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                    if($index < $count-1) {
                        $index++;
                    }
                } else {
                    $penambahan_nilai = 0;
                    for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                        $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                        if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                            $index++;
                        }
                    }

                    $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                    $persen = (int)$persen;
                    $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                    if(is_null($kode_64)) {
                        $kode_108 = substr($kode_108, 0, 14);
                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                        if(is_null($kode_64)) {
                            $kode_108 = substr($kode_108, 0, 11);
                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                        } else {
                            $kode_64 = $kode_64->kode_64;
                        }
                    } else {
                        $kode_64 = $kode_64->kode_64;
                    }

                    $kode_64 = substr($kode_64, 0, 8);
                    $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                    if(!is_null($masa_tambah)) {
                        $masa_tambahan = $masa_tambah->masa_tambahan;
                    }

                    if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                        $sisa_masa_tmp += $masa_tambahan;
                        if($sisa_masa_tmp > $mm_induk) {
                            $masa_manfaat = $mm_induk;
                            $sisa_masa_tmp = $mm_induk;
                        } else {
                            $masa_manfaat = $sisa_masa_tmp;
                        }
                        $masa_terpakai_tmp = 1;
                    } else {
                        ++ $masa_terpakai_tmp;
                    }

                    $nilai_buku_tmp += $penambahan_nilai;
                    $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                    $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                    $nilai_pengadaan_tmp += $penambahan_nilai;

                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;
                    
                    if($index < $count-1) {
                        $index++;
                    }
                }
            } else {
                ++$masa_terpakai_tmp;
                --$sisa_masa_tmp;

                $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                if($masa_terpakai_tmp > $masa_manfaat) {
                    $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                    $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                    $penyusutan_per_tahun_tmp = 0;
                    $nilai_buku_tmp = 0;
                    $sisa_masa_tmp = 0;
                } 
            }
        }
        
        $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
        $jumlah_barang += $jumlah_barang_tmp;

        $aset_induk->ak_penyusutan = $akumulasi_penyusutan_tmp;
        $aset_induk->nilai_buku = $nilai_buku_tmp;

        $induk = json_decode(json_encode($aset_induk), true);

        //UPDATE TABEL PENYUSUTAN
        $penyusutan = array(
            "id_aset" => $induk_id,
            "nilai_perolehan" => $nilai_pengadaan_tmp,
            "masa_manfaat" => $masa_manfaat,
            "masa_terpakai" => $masa_terpakai_tmp,
            "masa_sisa" => $sisa_masa_tmp,
            "beban" => $penyusutan_per_tahun_tmp,
            "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
            "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan_tmp,
            "nilai_buku" => $nilai_buku_tmp,
        );

        $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $induk_id)->first();
        $rp = new Rekap_penyusutanController();

        if(!is_null($laporan_penyusutan)) {
            //$rp->substract($aset_induk->nomor_lokasi, $kib, $akumulasi);  
            $susutkan = Penyusutan::where("id_aset", 'like', $induk_id)->update($penyusutan);
            //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);
        } else {
            $susutkan = Penyusutan::create($penyusutan);
            //$rp->add($aset_induk->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);
        }

        $rp = new Rekap_penyusutanController();
        //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);

        $Kib_induk = Kib::where('id_aset', $induk_id)->update($induk);

        return $this->sendResponse($aset_induk->toArray(), 'Aset induk rehab sukses digabungkan penyusutannya');
    }

    //untuk generate penyusutan secara umum
    public function kibe(Request $request)
    {
        $pagination = (int)$request->header('Pagination');
        $input = $request->all();

        ini_set('max_execution_time', 0);

        $data = DB::select(DB::raw('select k.* from kibs k left join penyusutans p on k.id_aset = p.id_aset
                                    where k.kode_jurnal not like "EKSTRAKOMPTABEL%"
                                    and k.kode_jurnal not like "PAKAI%"
                                    and k.saldo_barang > 0
                                    and k.id_aset not in (select rehab_id from rehabs)
                                    and k.kode_108 like "1.3.5%"
                                    and p.id_aset is null'));

        if(empty($data)) {
            return 'sudah sinkron';
        }

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = (int)Tahun::select('tahun_laporan')->first()->tahun_laporan;

        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $value = (array)$value;

            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;
            $kdp = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong) {
                $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->delete();
            }

            if($aset_rehab || $pakai_habis || $ekstrakom) {
                continue;
            }

            $kode = substr($value["kode_108"], 0, 5);
            if($kode == '1.3.1') {
                $kib = 'a';
            } else if($kode == '1.3.2') {
                $kib = 'b';
            } else if($kode == '1.3.3') {
                $kib = 'c';
            } else if($kode == '1.3.4') {
                $kib = 'd';
            } else if($kode == '1.3.5') {
                $kib = 'e';
            } else if($kode == '1.3.6') {
                $kib = 'f';
            } else if($kode == '1.5.3') {
                $kib = 'g';
            } else if($kode == '1.5.4') {
                $kib = 'r';
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if($value['bidang_barang'] == "A") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if($value['bidang_barang'] == "B") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['bidang_barang'] == "C" || $value['bidang_barang'] == "D") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['bidang_barang'] == "G") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(strpos($value['bidang_barang'], 'E') !== false) {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(strpos($value['bidang_barang'], 'F') !== false) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                    $kdp = true;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
            $masa_sisa = $masa_manfaat - $masa_terpakai;
            $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
            $status_aset = 0;
            $jumlah_barang_tmp = $value["saldo_barang"];

            if($value['bidang_barang'] == "A" || $kib_e == true || $kdp == true) {
                $akumulasi_penyusutan = 0;
                $akumulasi_penyusutan_berjalan = 0;
                $beban = 0;
                $nilai_buku = $nilai_perolehan_tmp;
            } else {
                if($masa_terpakai > $masa_manfaat) {
                    $nilai_perolehan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                    $masa_sisa = 0;
                    $beban = 0;
                    $nilai_buku = 0;
                } else {
                    $nilai_perolehan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                    $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                    $beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
                    $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                }
            }

            $jumlah_barang += $jumlah_barang_tmp;

            $induk = Kib::where('id_aset', 'like', $value['id_aset'])->first();

            $induk->ak_penyusutan = $akumulasi_penyusutan;
            $induk->nilai_buku = $nilai_buku;

            $induk = json_decode(json_encode($induk), true);

            $penyusutan = array(
                "id_aset" => $value['id_aset'],
                "nilai_perolehan" => $nilai_perolehan_tmp,
                "masa_manfaat" => $masa_manfaat,
                "masa_terpakai" => $masa_terpakai,
                "masa_sisa" => $masa_sisa,
                "beban" => $beban,
                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan,
                "nilai_buku" => $nilai_buku
            );

            $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->first();

            if(!is_null($laporan_penyusutan)) {
                $susutkan = Penyusutan::where("id_aset", 'like', $value['id_aset'])->update($penyusutan);
            } else {
                $susutkan = Penyusutan::create($penyusutan);
            }

            $Kib_induk = Kib::where('id_aset', $value['id_aset'])->update($induk);

            $aset_susut[$i++] =
            array(
                "id_aset" => $value["id_aset"],
                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                "nilai_buku" => $nilai_buku
            );
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    //untuk generate penyusutan satu aset
    public function generate($id_aset) {
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = (int)Tahun::select('tahun_spj')->first()->tahun_spj;
        // $email = Auth::guard('api')->user()->email;
        // if($email == 'sup@mojokerto.go.id') {
        //     $tahun_acuan = (int)Tahun::select('tahun_admin')->first()->tahun_admin;
        //     $tahun_spj = $tahun_acuan;
        // }
        $masa_terpakai;
        $masa_aktif_habis = false;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;
        $kdp = false;

        $aset = Kib::where('id_aset', $id_aset)->first();

        if(!is_null($aset)) {
            $kode = substr($aset->kode_108, 0, 5);
            if($kode == '1.3.1') {
                $kib = 'a';
            } else if($kode == '1.3.2') {
                $kib = 'b';
            } else if($kode == '1.3.3') {
                $kib = 'c';
            } else if($kode == '1.3.4') {
                $kib = 'd';
            } else if($kode == '1.3.5') {
                $kib = 'e';
            } else if($kode == '1.3.6') {
                $kib = 'f';
                $kdp = true;
            } else if($kode == '1.5.3') {
                $kib = 'g';
            } else if($kode == '1.5.5') {
                $kib = 'r';
            }
        }

        if($aset->kode_64 == '' || is_null($aset->kode_64)) {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_108", 'like', $aset->kode_108)->first();
        } else {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $aset->kode_64)->first();
        }
        
        $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $aset->nomor_lokasi)->first();

        if(!is_null($lokasi)) {
            $nama_lokasi = $lokasi->nama_lokasi;
        } else {
            $nama_lokasi = '-';
        }

        $saldo_kosong = false;
        $aset_induk = false;
        $aset_rehab = false;
        $kib_e = false;

        if($aset->saldo_barang == 0) {
            $saldo_kosong = true;
        }

        $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
        $sub_64 = substr($aset->kode_64, 0, 8);

        if(in_array($sub_64, $kode_e)) {
            if($aset->kode_64 == "1.3.5.01.12" || $aset->kode_64 == "1.3.5.01.14") {
                $kib_e = false;
            } else {
                $kib_e = true;
            }
        }

        $pakai_habis = false;
        $ekstrakom = false;

        if($aset->pos_entri == "PAKAI_HABIS") {
            $pakai_habis = true;
        }

        if($aset->pos_entri == "EKSTRAKOMPTABEL") {
            $ekstrakom = true;
        }

        $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $id_aset)->first();
        $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $id_aset)->first();

        if(!is_null($induk)) {
            $aset_induk = true;
        }

        if(!is_null($anak)) {
            $aset_rehab = true;
        }

        if($saldo_kosong) {
            Penyusutan::where('id_aset', $id_aset)->delete();
        }

        if($aset_rehab || $pakai_habis || $ekstrakom) {
            return "aset rehab, pakai habis, atau ekstrakom";
        }

        if(!is_null($mm)) {
            if($mm->masa_manfaat == 0) {
                $masa_manfaat = 0;
                $mm_induk = 0;
            } else {
                $masa_manfaat = (int)$mm->masa_manfaat;
                $mm_induk = (int)$mm->masa_manfaat;
            }
        } else {
            if($aset->bidang_barang == "A") {
                $masa_manfaat = 0;
                $mm_induk = 0;
            } else if($aset->bidang_barang == "B") {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if($aset->bidang_barang == "C" || $aset->bidang_barang == "D") {
                $masa_manfaat = 50;
                $mm_induk = 50;
            } else if($aset->bidang_barang == "G") {
                $masa_manfaat = 4;
                $mm_induk = 4;
            } else if(strpos($aset->bidang_barang, 'E') !== false) {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if(strpos($aset->bidang_barang, 'F') !== false) {
                $masa_manfaat = 0;
                $mm_induk = 0;
            }
        }

        if($masa_manfaat == 0) {
            if($aset->kode_108 == '1.5.4.01.01.01.002') {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if($aset->kode_108 == '1.5.4.01.01.01.003') {
                $masa_manfaat = 50;
                $mm_induk = 50;
            } else if($aset->kode_108 == '1.5.4.01.01.01.004') {
                $masa_manfaat = 40;
                $mm_induk = 40;
            } else if($aset->kode_108 == '1.5.4.01.01.01.005') {
                $masa_manfaat = 5;
                $mm_induk = 5;
            }
        }

        if($aset_induk) {
            //untuk mengambil aset rehab berdasarkan id aset induk
            $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
            ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
            ->where('rehabs.aset_induk_id', $id_aset)
            ->orderBy('kibs.tahun_pengadaan', 'asc')->get();

            //$rehabs = Rehab::where("aset_induk_id", $value["id_aset"])->orderBy('tahun_rehab', 'asc')->get();

            if(!empty($rehabs)) {
                $detail_penyusutan = $rehabs->toArray();
            }

            $count = sizeof($detail_penyusutan);

            $status_aset = 1;

            $jumlah_barang_tmp = $aset->saldo_barang;
            $tahun_pengadaan_tmp = intval($aset->tahun_pengadaan);
            $nilai_pengadaan_tmp = floatval($aset->harga_total_plus_pajak_saldo);
            $persen = 0;
            $penambahan_nilai = 0;
            $masa_tambahan = 0;
            $masa_terpakai_tmp = 0;
            $sisa_masa_tmp = $masa_manfaat;
            $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
            $akumulasi_penyusutan_tmp = 0;
            $akumulasi_penyusutan_berjalan_tmp = 0;
            $nilai_buku_tmp = 0;
            $index = 0;
            $k = 0;

            for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                if($th == $tahun_rehab) {
                    $jumlah_renov_tahunx = 0;
                    $tahun_pembanding = 0;

                    if($tahun_pengadaan_tmp == $tahun_rehab) {
                        $nilai_buku_tmp = $nilai_pengadaan_tmp;
                    }

                    for ($x = 0; $x < $count; $x++) {
                         $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                         if($tahun_pembanding === $tahun_rehab) {
                            $jumlah_renov_tahunx++;
                         }
                    }

                    if($jumlah_renov_tahunx == 1) {
                        $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                        $persen = (int)$persen;
                        $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                        if(is_null($kode_64)) {
                            $kode_108 = substr($kode_108, 0, 14);
                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 11);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }
                        } else {
                            $kode_64 = $kode_64->kode_64;
                        }

                        $kode_64 = substr($kode_64, 0, 8);
                        $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                        if(!is_null($masa_tambah)) {
                            $masa_tambahan = $masa_tambah->masa_tambahan;
                        }

                        if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                            $sisa_masa_tmp += $masa_tambahan;
                            if($sisa_masa_tmp > $mm_induk) {
                                $masa_manfaat = $mm_induk;
                                $sisa_masa_tmp = $mm_induk;
                            } else {
                                $masa_manfaat = $sisa_masa_tmp;
                            }
                            $masa_terpakai_tmp = 1;
                        } else {
                            ++$masa_terpakai_tmp;
                        }

                        $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                        if($sisa_masa_tmp > 0) {
                            $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                        } else {
                            $penyusutan_per_tahun_tmp = 0;
                        }
                        
                        $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                        $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                        --$sisa_masa_tmp;
                        $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                        $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                        if($index < $count-1) {
                            $index++;
                        }
                    } else {
                        $penambahan_nilai = 0;
                        for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                            $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                            if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                $index++;
                            }
                        }

                        $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                        $persen = (int)$persen;
                        $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                        $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                        if(is_null($kode_64)) {
                            $kode_108 = substr($kode_108, 0, 14);
                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 11);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }
                        } else {
                            $kode_64 = $kode_64->kode_64;
                        }

                        $kode_64 = substr($kode_64, 0, 8);
                        $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                        if(!is_null($masa_tambah)) {
                            $masa_tambahan = $masa_tambah->masa_tambahan;
                        }

                        if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                            $sisa_masa_tmp += $masa_tambahan;
                            if($sisa_masa_tmp > $mm_induk) {
                                $masa_manfaat = $mm_induk;
                                $sisa_masa_tmp = $mm_induk;
                            } else {
                                $masa_manfaat = $sisa_masa_tmp;
                            }

                            $masa_terpakai_tmp = 1;
                        } else {
                            ++ $masa_terpakai_tmp;
                        }

                        $nilai_buku_tmp += $penambahan_nilai;
                        $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                        $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                        $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                        $nilai_pengadaan_tmp += $penambahan_nilai;

                        $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                        if($sisa_masa_tmp > 0) {
                            --$sisa_masa_tmp;
                        }
                        
                        if($index < $count-1) {
                            $index++;
                        }
                    }
                } else {
                    ++$masa_terpakai_tmp;
                    --$sisa_masa_tmp;

                    $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                    $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                    $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                    if($masa_terpakai_tmp > $masa_manfaat) {
                        $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                        $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                        $penyusutan_per_tahun_tmp = 0;
                        $nilai_buku_tmp = 0;
                        $sisa_masa_tmp = 0;
                        $masa_aktif_habis = true;
                    } 
                }
            }
            
            if(!$masa_aktif_habis) {
                $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
            }
            
            $jumlah_barang += $jumlah_barang_tmp;

            if($kib == 'a' || $kib_e == true || $kdp == true) {
                $akumulasi_penyusutan_tmp = 0;
                $akumulasi_penyusutan_berjalan_tmp = 0;
                $penyusutan_per_tahun_tmp = 0;
                $nilai_buku_tmp = $nilai_pengadaan_tmp;
                $masa_manfaat = 0;
                $akumulasi_penyusutan = 0;
                $beban = 0;
                $nilai_buku = $nilai_pengadaan_tmp;
            }

            $data = Kib::where('id_aset', 'like', $id_aset)->first();

            $data->ak_penyusutan = $akumulasi_penyusutan_tmp;
            $data->nilai_buku = $nilai_buku_tmp;

            $induk = json_decode(json_encode($data), true);

            $penyusutan = array(
                "id_aset" => $id_aset,
                "nilai_perolehan" => $nilai_pengadaan_tmp,
                "masa_manfaat" => $masa_manfaat,
                "masa_terpakai" => $masa_terpakai_tmp,
                "masa_sisa" => $sisa_masa_tmp,
                "beban" => $penyusutan_per_tahun_tmp,
                "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan_tmp,
                "nilai_buku" => $nilai_buku_tmp
            );

            $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $id_aset)->first();
            $rp = new Rekap_penyusutanController();

            if(!is_null($laporan_penyusutan)) {
                //$rp->substract($aset->nomor_lokasi, $kib, $akumulasi);  
                $susutkan = Penyusutan::where("id_aset", 'like', $id_aset)->update($penyusutan);
                //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);
            } else {
                $susutkan = Penyusutan::create($penyusutan);
                //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);
            }

            $Kib_induk = Kib::where('id_aset', $id_aset)->update($induk);
            return $this->sendResponse($data->toArray(), 'Penyusutan generated successfully.');
        } else {
            $masa_terpakai = $tahun_acuan - $aset->tahun_pengadaan + 1;
            $masa_sisa = $masa_manfaat - $masa_terpakai;
            $nilai_perolehan = floatval($aset->harga_total_plus_pajak_saldo);
            $status_aset = 0;
            $jumlah_barang_tmp = $aset->saldo_barang;

            if($aset->bidang_barang == "A" || $kib_e == true || $kdp == true) {
                $akumulasi_penyusutan = 0;
                $akumulasi_penyusutan_berjalan = 0;
                $masa_sisa = 0;
                $beban = 0;
                $nilai_buku = $nilai_perolehan;
            } else {
                if($masa_terpakai > $masa_manfaat) {
                    $akumulasi_penyusutan = $nilai_perolehan;
                    $akumulasi_penyusutan_berjalan = $nilai_perolehan;
                    $masa_sisa = 0;
                    $beban = 0;
                    $nilai_buku = 0;
                } else {
                    $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan;
                    $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan;
                    $beban = 1/$masa_manfaat*$nilai_perolehan; 
                    $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                }
            }

            $jumlah_barang += $jumlah_barang_tmp;

            $data = Kib::where('id_aset', 'like', $id_aset)->first();

            $data->ak_penyusutan = $akumulasi_penyusutan;
            $data->nilai_buku = $nilai_buku;

            $induk = json_decode(json_encode($data), true);

            $penyusutan = array(
                "id_aset" => $id_aset,
                "nilai_perolehan" => $nilai_perolehan,
                "masa_manfaat" => $masa_manfaat,
                "masa_terpakai" => $masa_terpakai,
                "masa_sisa" => $masa_sisa,
                "beban" => $beban,
                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                "akumulasi_penyusutan_berjalan" => $akumulasi_penyusutan_berjalan,
                "nilai_buku" => $nilai_buku
            );

            $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $id_aset)->first();
            $rp = new Rekap_penyusutanController();

            if(!is_null($laporan_penyusutan)) {
                $akumulasi = $laporan_penyusutan->akumulasi_penyusutan_berjalan;

                //$rp->substract($aset->nomor_lokasi, $kib, $akumulasi);  
                $susutkan = Penyusutan::where("id_aset", 'like', $id_aset)->update($penyusutan);
                //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);
            } else {
                $susutkan = Penyusutan::create($penyusutan);
                //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_penyusutan_berjalan);
            }

            $Kib_induk = Kib::where('id_aset', $id_aset)->update($induk);
            return $this->sendResponse($data->toArray(), 'Penyusutan generated successfully.');
        }   
    }

    //untuk generate aset baru
    public function baru($id_aset) {
        $aset = Kib::where('id_aset', $id_aset)->first();
        $mm = Kamus_rekening::select('masa_manfaat')->where('kode_64', 'like', $aset->kode_64.'%')->first();

        $tahun_spj = (int)Tahun::select('tahun_spj')->first()->tahun_spj;
        $tahun_pengadaan = $aset->tahun_pengadaan;
        $kdp = false;

        $kode = substr($aset->kode_108, 0, 5);
        if($kode == '1.3.1') {
            $kib = 'a';
        } else if($kode == '1.3.2') {
            $kib = 'b';
        } else if($kode == '1.3.3') {
            $kib = 'c';
        } else if($kode == '1.3.4') {
            $kib = 'd';
        } else if($kode == '1.3.5') {
            $kib = 'e';
        } else if($kode == '1.3.6') {
            $kib = 'f';
            $kdp = true;
        } else if($kode == '1.5.3') {
            $kib = 'g';
        } else if($kode == '1.5.5') {
            $kib = 'r';
        }

        if(!is_null($mm)) {
            if($mm->masa_manfaat == 0) {
                $masa_manfaat = 0;
                $mm_induk = 0;
            } else {
                $masa_manfaat = (int)$mm->masa_manfaat;
                $mm_induk = (int)$mm->masa_manfaat;
            }
        } else {
            $kode = substr($aset->kode_108, 0, 14);
            $mm = Kamus_rekening::select('masa_manfaat')->where('kode_64', 'like', $kode . '%')-first();
            if(is_null($mm)) {
                $kode = substr($aset->kode_108, 0, 11);
                $mm = Kamus_rekening::select('masa_manfaat')->where('kode_64', 'like', $kode . '%')-first();

                if(is_null($mm)) {
                    $kode = substr($aset->kode_108, 0, 5);
                    if($kode == '1.3.1') {
                        $masa_manfaat = 0;
                        $mm_induk = 0;
                    } else if($kode == '1.3.2') {
                        $masa_manfaat = 5;
                        $mm_induk = 5;
                    } else if($kode == '1.3.3') {
                        $masa_manfaat = 50;
                        $mm_induk = 50;
                    } else if($kode == '1.3.4') {
                        $masa_manfaat = 50;
                        $mm_induk = 50;
                    } else if($kode == '1.3.5') {
                        $masa_manfaat = 0;
                        $mm_induk = 0;
                    } else if($kode == '1.3.6') {
                        $masa_manfaat = 0;
                        $mm_induk = 0;
                    } else if($kode == '1.5.3') {
                        $masa_manfaat = 4;
                        $mm_induk = 4;
                    } 
                }
            }            
        }

        if($masa_manfaat == 0) {
            if($aset->kode_108 == '1.5.4.01.01.01.002') {
                $masa_manfaat = 5;
                $mm_induk = 5;
            } else if($aset->kode_108 == '1.5.4.01.01.01.003') {
                $masa_manfaat = 50;
                $mm_induk = 50;
            } else if($aset->kode_108 == '1.5.4.01.01.01.004') {
                $masa_manfaat = 40;
                $mm_induk = 40;
            } else if($aset->kode_108 == '1.5.4.01.01.01.005') {
                $masa_manfaat = 5;
                $mm_induk = 5;
            }
        }

        $pakai_habis = false;
        $rehab = false;
        $ekstrakom = false;

        if($aset->pos_entri == "PAKAI_HABIS") {
            $pakai_habis = true;
        }

        if($aset->pos_entri == "EKSTRAKOMPTABEL") {
            $ekstrakom = true;
        }

        if($aset->pos_entri == "rehab" || $aset->aset_rehab == 1) {
            $rehab = true;
        }

        if($pakai_habis || $ekstrakom || $rehab) {
            return "Aset Rehab, Pakai Habis, Ekstrakom";
        } else {
            if(!is_null($masa_manfaat)) {
                $nilai_pengadaan = (double) $aset->harga_total_plus_pajak;
                $masa_terpakai = ($tahun_spj - $tahun_pengadaan) + 1;
                $masa_sisa = $masa_manfaat - $masa_terpakai;

                if($masa_manfaat == 0) {
                    $beban = 0;
                    $akumulasi = 0;
                    $akumulasi_berjalan = 0;
                    $nilai_buku = $nilai_pengadaan;
                    $prosen_susut = 0;
                    $masa_sisa = $masa_manfaat;
                    $masa_terpakai = 0;
                } else {
                    $beban = $nilai_pengadaan/$masa_manfaat;
                    $akumulasi = $beban*($masa_terpakai-1);
                    $akumulasi_berjalan = $beban*$masa_terpakai;
                    $nilai_buku = $nilai_pengadaan - $akumulasi_berjalan;
                    $prosen_susut = 1/$masa_manfaat*100;
                }

                $aset->waktu_susut = $masa_manfaat;
                $aset->prosen_susut = $prosen_susut;
                $aset->ak_penyusutan = $akumulasi_berjalan;
                $aset->tahun_buku = $aset->tahun_spj;
                $aset->nilai_buku = $nilai_buku;

                $penyusutan = array(
                    "id_aset" => $id_aset,
                    "nilai_perolehan" => $nilai_pengadaan,
                    "masa_manfaat" => $masa_manfaat,
                    "masa_terpakai" => $masa_terpakai,
                    "masa_sisa" => $masa_sisa,
                    "beban" => $beban,
                    "akumulasi_penyusutan" => $akumulasi,
                    "akumulasi_penyusutan_berjalan" => $akumulasi_berjalan,
                    "nilai_buku" => $nilai_buku
                );

                $data = json_decode(json_encode($aset), true);
                $update = Kib::where('id_aset', $id_aset)->update($data);

                $laporan_penyusutan = Penyusutan::where("id_aset", 'like', $id_aset)->first();

                if(!is_null($laporan_penyusutan)) {
                    $susutkan = Penyusutan::where("id_aset", 'like', $id_aset)->update($penyusutan);
                } else {
                    $susutkan = Penyusutan::create($penyusutan);
                }

                $rp = new Rekap_penyusutanController();
                //$rp->add($aset->nomor_lokasi, $kib, $akumulasi_berjalan);

                return $this->sendResponse($aset->toArray(), 'Aset sukses disusutkan');
            }
        }
    }

    //untuk hapus penyusutan aset beserta rekap penyusutannya
    public function remove($id_aset) {
        $penyusutan = Penyusutan::where("id_aset", 'like', $id_aset)->first();
        $aset = Kib::where('id_aset', $id_aset)->first();

        if(is_null($aset) || empty($aset)) {
            $aset = Rincian_masuk::where('id_aset', $id_aset)->first();
        }

        if(is_null($penyusutan) || empty($penyusutan)) {
            $penyusutan = $aset;
        }

        $deleted = Penyusutan::where('id_aset', 'like', $id_aset)->delete();
        return $this->sendResponse($penyusutan->toArray(), 'Penyusutan deleted successfully.');
    }

     //untuk hapus penyusutan aset beserta rekap penyusutannya
    public function cek($lokasi) {
        ini_set('max_execution_time', 1800);
        $rekap = array();
        $i = 0;

        $data = Penyusutan::join('kibs', 'penyusutans.id_aset', '=', 'kibs.id_aset')
            ->select('kibs.id_aset', 'penyusutans.akumulasi_penyusutan_berjalan', 'kibs.kode_108')
            ->where('kibs.nomor_lokasi', 'like', $lokasi . '%')
            ->get()
            ->toArray();

        $rekap = Rekap_penyusutan::where('nomor_lokasi', 'like', $lokasi . '%')->get()->toArray();

        $penyusutan_kib_a = 0;
        $penyusutan_kib_b = 0;
        $penyusutan_kib_c = 0;
        $penyusutan_kib_d = 0;
        $penyusutan_kib_e = 0;
        $penyusutan_kib_f = 0;
        $penyusutan_kib_g = 0;
        $penyusutan_kib_r = 0;

        $rekap_a = 0;
        $rekap_b = 0;
        $rekap_c = 0;
        $rekap_d = 0;
        $rekap_e = 0;
        $rekap_f = 0;
        $rekap_g = 0;
        $rekap_r = 0;

        foreach ($data as $value) {
            $kode = substr($value["kode_108"], 0, 5);
            if($kode == '1.3.1') {
                $penyusutan_kib_a += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.3.2') {
                $penyusutan_kib_b += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.3.3') {
                $penyusutan_kib_c += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.3.4') {
                $penyusutan_kib_d += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.3.5') {
                $penyusutan_kib_e += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.3.6') {
                $penyusutan_kib_f += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.5.3') {
                $penyusutan_kib_g += $value['akumulasi_penyusutan_berjalan'];
            } else if($kode == '1.5.4') {
                $penyusutan_kib_r += $value['akumulasi_penyusutan_berjalan'];
            }
        }

        foreach ($rekap as $r) {
            $rekap_a += $r['a'];
            $rekap_b += $r['b'];
            $rekap_c += $r['c'];
            $rekap_d += $r['d'];
            $rekap_e += $r['e'];
            $rekap_f += $r['f'];
            $rekap_g += $r['g'];
            $rekap_r += $r['r'];
        }

        $data_penyusutan = array(
            'a' => $penyusutan_kib_a,
            'r_a' => $rekap_a,
            'b' => $penyusutan_kib_b,
            'r_b' => $rekap_b,
            'c' => $penyusutan_kib_c,
            'r_c' => $rekap_c,
            'd' => $penyusutan_kib_d,
            'r_d' => $rekap_d,
            'e' => $penyusutan_kib_e,
            'r_e' => $rekap_e,
            'f' => $penyusutan_kib_f,
            'r_f' => $rekap_f,
            'g' => $penyusutan_kib_g,
            'r_g' => $rekap_g,
            'r' => $penyusutan_kib_r,
            'r_r' => $rekap_r
        );

        return $data_penyusutan;
    }
}