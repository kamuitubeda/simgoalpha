<?php

namespace App\Http\Controllers\API\Jurnal;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Tahun;
use Validator;

class Ganti_tahunController extends BaseController
{
    public function getAdmin()
    {
        $tahun = Tahun::first();

        $tahun_admin = $tahun->tahun_admin;
        $data = json_decode(json_encode($tahun_admin), true);

        return $this->sendResponse($data, 'Tahun admin adalah ' . $data);
    }

    public function setAdmin(Request $request)
    {
        $input = $request->all();
        $tahun_update = $input['tahun'];

        $tahun = Tahun::first();

        $tahun->tahun_admin = $tahun_update;
        $data = json_decode(json_encode($tahun), true);

        $tahun->update($data);
        return $this->sendResponse($data, 'Tahun admin telah diganti');
    }
}