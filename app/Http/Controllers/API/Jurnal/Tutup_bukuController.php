<?php

namespace App\Http\Controllers\API\Jurnal;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Jurnal\Rincian_masuk;
use App\Models\Jurnal\Kib;
use App\Models\Jurnal\Rehab;
use App\Models\Kamus\Kamus_rekening;
use App\Models\Kamus\Kamus_lokasi;
use App\Models\Kamus\Kamus_unit;
use App\Models\Kamus\Kamus_sub_unit;
use App\Models\Kamus\Masa_tambahan;
use App\Models\Kamus\Kamus_masa_manfaat_tambahan;
use App\Http\Resources\Jurnal\Rincian_masukCollection;
use App\Http\Resources\Jurnal\KibCollection;
use App\Http\Resources\Jurnal\RehabCollection;

use App\Exports\LaporanPenyusutan108Export;
use App\Exports\LaporanPenyusutan64Export;
use App\Exports\LaporanPenyusutanAk108Export;
use App\Exports\PenyusutanReklasKeluar64Export;
use App\Exports\PenyusutanReklasKeluar108Export;
use App\Exports\PenyusutanReklasMasuk64Export;
use App\Exports\PenyusutanReklasMasuk108Export;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class Tutup_bukuController extends BaseController
{
    public function susutkan_index()
    {
        ini_set('max_execution_time', 1800);

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = date('Y')-1;
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        $data = Kib::where('saldo_barang', '>', 0)
                ->get()
                ->toArray();

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom) {
                continue;
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if(substr($value['kode_108'], 0, 5) == "1.3.1") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.2") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.3" || substr($value['kode_108'], 0, 5) == "1.3.4") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if(substr($value['kode_108'], 0, 5) == "1.5.3") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.5") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($aset_induk) {
                //untuk mengambil aset rehab berdasarkan id aset induk
                $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                ->where('rehabs.aset_induk_id', $value['id_aset'])
                ->orderBy('kibs.tahun_pengadaan', 'asc')->get();

                //$rehabs = Rehab::where("aset_induk_id", $value["id_aset"])->orderBy('tahun_rehab', 'asc')->get();

                if(!empty($rehabs)) {
                    $detail_penyusutan = $rehabs->toArray();
                }

                $count = sizeof($detail_penyusutan);

                $status_aset = 1;

                $jumlah_barang_tmp = $value["saldo_barang"];
                $tahun_pengadaan_tmp = intval($value["tahun_pengadaan"]);
                $nilai_pengadaan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $persen = 0;
                $penambahan_nilai = 0;
                $masa_tambahan = 0;
                $masa_terpakai_tmp = 0;
                $sisa_masa_tmp = $masa_manfaat;
                $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
                $akumulasi_penyusutan_tmp = 0;
                $akumulasi_penyusutan_berjalan_tmp = 0;
                $nilai_buku_tmp = 0;
                $index = 0;
                $k = 0;

                for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                    $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                    if($th == $tahun_rehab) {
                        $jumlah_renov_tahunx = 0;
                        $tahun_pembanding = 0;

                        if($tahun_pengadaan_tmp == $tahun_rehab) {
                            $nilai_buku_tmp = $nilai_pengadaan_tmp;
                        }

                        for ($x = 0; $x < $count; $x++) {
                             $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                             if($tahun_pembanding === $tahun_rehab) {
                                $jumlah_renov_tahunx++;
                             }
                        }

                        if($jumlah_renov_tahunx == 1) {
                            $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++$masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                            if($sisa_masa_tmp > 0) {
                                $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            } else {
                                $penyusutan_per_tahun_tmp = 0;
                            }
                            
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            --$sisa_masa_tmp;
                            $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                            if($index < $count-1) {
                                $index++;
                            }
                        } else {
                            $penambahan_nilai = 0;
                            for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                                $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                                if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                    $index++;
                                }
                            }

                            $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++ $masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $penambahan_nilai;
                            $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            $nilai_pengadaan_tmp += $penambahan_nilai;

                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;
                            // if($sisa_masa_tmp > 0) {
                            //     --$sisa_masa_tmp;
                            // }
                            
                            if($index < $count-1) {
                                $index++;
                            }
                        }
                    } else {
                        ++$masa_terpakai_tmp;
                        --$sisa_masa_tmp;
                        // if($sisa_masa_tmp > 0) {
                        //     --$sisa_masa_tmp;
                        // }

                        $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                        $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                        $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                        if($masa_terpakai_tmp > $masa_manfaat) {
                            $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                            $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                            $nilai_buku_tmp = 0;
                            $sisa_masa_tmp = 0;
                        } 
                    }
                }
                
                $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
                $jumlah_barang += $jumlah_barang_tmp;

                if(substr($value['kode_108'], 0, 5) == "1.3.1" || $kib_e == true) {
                    $akumulasi_penyusutan_tmp = 0;
                    $akumulasi_penyusutan_berjalan_tmp = 0;
                    $penyusutan_per_tahun_tmp = 0;
                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                    $akumulasi_penyusutan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_pengadaan_tmp;
                }

                //untuk generate penyusutan
                $susut = array();
                $susut['ak_penyusutan'] = $akumulasi_penyusutan_berjalan_tmp;
                $susut['nilai_buku'] = $nilai_buku_tmp;
                $update_kib = Kib::where('id_aset', $value["id_aset"])->update($susut);
                
                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                    "nilai_buku" => $nilai_buku_tmp
                );
            } else {
                $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
                $masa_sisa = $masa_manfaat - $masa_terpakai;
                $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $status_aset = 0;
                $jumlah_barang_tmp = $value["saldo_barang"];

                if($masa_terpakai > $masa_manfaat) {
                    $nilai_perolehan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                    $masa_sisa = 0;
                    $beban = 0;
                    $nilai_buku = 0;
                } else {
                    $nilai_perolehan = $nilai_perolehan_tmp;
                    $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                    $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                    $beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
                    $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
                }

                if(substr($value['kode_108'], 0, 5) == "1.3.1" || $kib_e == true) {
                    $akumulasi_penyusutan = 0;
                    $akumulasi_penyusutan_berjalan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_perolehan_tmp;
                }

                //untuk generate penyusutan
                $susut = array();
                $susut['ak_penyusutan'] = $akumulasi_penyusutan_berjalan;
                $susut['nilai_buku'] = $nilai_buku;
                $update_kib = Kib::where('id_aset', $value["id_aset"])->update($susut);

                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan,
                    "nilai_buku" => $nilai_buku
                );
            }
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    public function susutkan()
    {
        ini_set('max_execution_time', 1800);

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = date('Y')-1;
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        $data = Kib::where('saldo_barang', '>', 0)
                ->whereNull('ak_penyusutan')
                ->get()
                ->toArray();

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom || $aset_induk) {
                continue;
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if(substr($value['kode_108'], 0, 5) == "1.3.1") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.2") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.3" || substr($value['kode_108'], 0, 5) == "1.3.4") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if(substr($value['kode_108'], 0, 5) == "1.5.3") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.5") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            
            $masa_terpakai = $tahun_acuan - $value["tahun_pengadaan"] + 1;
            $masa_sisa = $masa_manfaat - $masa_terpakai;
            $nilai_perolehan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
            $status_aset = 0;
            $jumlah_barang_tmp = $value["saldo_barang"];

            if($masa_terpakai > $masa_manfaat) {
                $nilai_perolehan = $nilai_perolehan_tmp;
                $akumulasi_penyusutan = $nilai_perolehan_tmp;
                $akumulasi_penyusutan_berjalan = $nilai_perolehan_tmp;
                $masa_sisa = 0;
                $beban = 0;
                $nilai_buku = 0;
            } else {
                $nilai_perolehan = $nilai_perolehan_tmp;
                $akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
                $akumulasi_penyusutan_berjalan = ($masa_terpakai/$masa_manfaat)*$nilai_perolehan_tmp;
                $beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
                $nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
            }

            if(substr($value['kode_108'], 0, 5) == "1.3.1" || $kib_e == true) {
                $akumulasi_penyusutan = 0;
                $akumulasi_penyusutan_berjalan = 0;
                $beban = 0;
                $nilai_buku = $nilai_perolehan_tmp;
            }

            //untuk generate penyusutan
            $susut = array();
            $susut['ak_penyusutan'] = $akumulasi_penyusutan_berjalan;
            $susut['nilai_buku'] = $nilai_buku;
            $update_kib = Kib::where('id_aset', $value["id_aset"])->update($susut);

            $aset_susut[$i++] =
            array(
                "id_aset" => $value["id_aset"],
                "akumulasi_penyusutan" => $akumulasi_penyusutan,
                "nilai_buku" => $nilai_buku
            );
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }

    public function susutkan_induk()
    {
        ini_set('max_execution_time', 0);

        $aset_susut = array();
        $i = 0;
        $j = 0;
        $jumlah_barang = 0;
        $tahun_acuan = date('Y')-1;
        $masa_terpakai;
        $total_nilai_perolehan = 0;
        $total_akumulasi_penyusutan = 0;
        $total_akumulasi_penyusutan_berjalan = 0;
        $total_beban = 0;
        $total_nilai_buku = 0;
        $total_rehab = 0;
        $jumlah_rehab = 0;

        $data = Kib::where('saldo_barang', '>', 0)
                ->whereNull('ak_penyusutan')
                ->get()
                ->toArray();

        // loop khusus menjumlahkan nilai berdasarkan kesamaan kode rekening 64
        foreach($data as $value) {
            $mm = Kamus_rekening::select("masa_manfaat")->where("kode_64", 'like', $value["kode_64"])->first();
            $lokasi = Kamus_lokasi::select('nama_lokasi')->where('nomor_lokasi', 'like', $value['nomor_lokasi'])->first();

            if(!is_null($lokasi)) {
                $nama_lokasi = $lokasi->nama_lokasi;
            } else {
                $nama_lokasi = '-';
            }

            $saldo_kosong = false;
            $aset_induk = false;
            $aset_rehab = false;
            $kib_e = false;

            if($value["saldo_barang"] == 0) {
                $saldo_kosong = true;
            }

            $kode_e = array("1.3.5.01",  "1.3.5.02",  "1.3.5.04",  "1.3.5.06",  "1.3.5.07");
            $sub_64 = substr($value["kode_64"], 0, 8);

            if(in_array($sub_64, $kode_e)) {
                if($value["kode_64"] == "1.3.5.01.12" || $value["kode_64"] == "1.3.5.01.14") {
                    $kib_e = false;
                } else {
                    $kib_e = true;
                }
            }

            $pakai_habis = false;
            $ekstrakom = false;

            if($value["pos_entri"] == "PAKAI_HABIS") {
                $pakai_habis = true;
            }

            if($value["pos_entri"] == "EKSTRAKOMPTABEL") {
                $ekstrakom = true;
            }

            $induk = Rehab::select("aset_induk_id")->where("aset_induk_id", 'like', $value["id_aset"])->first();
            $anak = Rehab::select("rehab_id")->where("rehab_id", 'like', $value["id_aset"])->first();

            if(!is_null($induk)) {
                $aset_induk = true;
            }

            if(!is_null($anak)) {
                $aset_rehab = true;
            }

            if($saldo_kosong || $aset_rehab || $pakai_habis || $ekstrakom) {
                continue;
            }

            if(!is_null($mm)) {
                if($mm->masa_manfaat == 0) {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else {
                    $masa_manfaat = (int)$mm->masa_manfaat;
                    $mm_induk = (int)$mm->masa_manfaat;
                }
            } else {
                if(substr($value['kode_108'], 0, 5) == "1.3.1") {
                    $masa_manfaat = 0;
                    $mm_induk = 0;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.2") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.3" || substr($value['kode_108'], 0, 5) == "1.3.4") {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if(substr($value['kode_108'], 0, 5) == "1.5.3") {
                    $masa_manfaat = 4;
                    $mm_induk = 4;
                } else if(substr($value['kode_108'], 0, 5) == "1.3.5") {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($masa_manfaat == 0) {
                if($value['kode_108'] == '1.5.4.01.01.01.002') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                } else if($value['kode_108'] == '1.5.4.01.01.01.003') {
                    $masa_manfaat = 50;
                    $mm_induk = 50;
                } else if($value['kode_108'] == '1.5.4.01.01.01.004') {
                    $masa_manfaat = 40;
                    $mm_induk = 40;
                } else if($value['kode_108'] == '1.5.4.01.01.01.005') {
                    $masa_manfaat = 5;
                    $mm_induk = 5;
                }
            }

            if($aset_induk) {
                //untuk mengambil aset rehab berdasarkan id aset induk
                $rehabs = Rehab::join('kibs', 'kibs.id_aset', '=', 'rehabs.rehab_id')
                ->select('rehabs.rehab_id', 'kibs.tahun_pengadaan as tahun_rehab', 'kibs.harga_total_plus_pajak_saldo as nilai_rehab', 'kibs.kode_108 as kode_rek_rehab', 'rehabs.tambah_manfaat')
                ->where('rehabs.aset_induk_id', $value['id_aset'])
                ->orderBy('kibs.tahun_pengadaan', 'asc')->get();

                //$rehabs = Rehab::where("aset_induk_id", $value["id_aset"])->orderBy('tahun_rehab', 'asc')->get();

                if(!empty($rehabs)) {
                    $detail_penyusutan = $rehabs->toArray();
                }

                $count = sizeof($detail_penyusutan);

                $status_aset = 1;

                $jumlah_barang_tmp = $value["saldo_barang"];
                $tahun_pengadaan_tmp = intval($value["tahun_pengadaan"]);
                $nilai_pengadaan_tmp = floatval($value["harga_total_plus_pajak_saldo"]);
                $persen = 0;
                $penambahan_nilai = 0;
                $masa_tambahan = 0;
                $masa_terpakai_tmp = 0;
                $sisa_masa_tmp = $masa_manfaat;
                $penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
                $akumulasi_penyusutan_tmp = 0;
                $akumulasi_penyusutan_berjalan_tmp = 0;
                $nilai_buku_tmp = 0;
                $index = 0;
                $k = 0;

                for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
                    $tahun_rehab = intval($detail_penyusutan[$index]["tahun_rehab"]);
                    if($th == $tahun_rehab) {
                        $jumlah_renov_tahunx = 0;
                        $tahun_pembanding = 0;

                        if($tahun_pengadaan_tmp == $tahun_rehab) {
                            $nilai_buku_tmp = $nilai_pengadaan_tmp;
                        }

                        for ($x = 0; $x < $count; $x++) {
                             $tahun_pembanding = intval($detail_penyusutan[$x]["tahun_rehab"]);

                             if($tahun_pembanding === $tahun_rehab) {
                                $jumlah_renov_tahunx++;
                             }
                        }

                        if($jumlah_renov_tahunx == 1) {
                            $persen = $detail_penyusutan[$index]["nilai_rehab"]/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++$masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $detail_penyusutan[$index]["nilai_rehab"];

                            if($sisa_masa_tmp > 0) {
                                $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            } else {
                                $penyusutan_per_tahun_tmp = 0;
                            }
                            
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            --$sisa_masa_tmp;
                            $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilai_rehab"];
                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                            if($index < $count-1) {
                                $index++;
                            }
                        } else {
                            $penambahan_nilai = 0;
                            for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
                                $penambahan_nilai += $detail_penyusutan[$index]["nilai_rehab"];
                                if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
                                    $index++;
                                }
                            }

                            $persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;

                            $persen = (int)$persen;
                            $kode_108 = $detail_penyusutan[$index]["kode_rek_rehab"];

                            $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                            if(is_null($kode_64)) {
                                $kode_108 = substr($kode_108, 0, 14);
                                $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();

                                if(is_null($kode_64)) {
                                    $kode_108 = substr($kode_108, 0, 11);
                                    $kode_64 = Kamus_rekening::select('kode_64')->where('kode_108', 'like', $kode_108 . '%')->first();
                                } else {
                                    $kode_64 = $kode_64->kode_64;
                                }
                            } else {
                                $kode_64 = $kode_64->kode_64;
                            }

                            $kode_64 = substr($kode_64, 0, 8);
                            $masa_tambah = Masa_tambahan::where('kode_64', $kode_64)->where('minim', '<', $persen)->orderBy('minim', 'desc')->first();

                            if(!is_null($masa_tambah)) {
                                $masa_tambahan = $masa_tambah->masa_tambahan;
                            }

                            if($detail_penyusutan[$index]["tambah_manfaat"] == 1) {
                                $sisa_masa_tmp += $masa_tambahan;
                                if($sisa_masa_tmp > $mm_induk) {
                                    $masa_manfaat = $mm_induk;
                                    $sisa_masa_tmp = $mm_induk;
                                } else {
                                    $masa_manfaat = $sisa_masa_tmp;
                                }
                                $masa_terpakai_tmp = 1;
                            } else {
                                ++ $masa_terpakai_tmp;
                            }

                            $nilai_buku_tmp += $penambahan_nilai;
                            $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
                            $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                            $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
                            $nilai_pengadaan_tmp += $penambahan_nilai;

                            $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;
                            // if($sisa_masa_tmp > 0) {
                            //     --$sisa_masa_tmp;
                            // }
                            
                            if($index < $count-1) {
                                $index++;
                            }
                        }
                    } else {
                        ++$masa_terpakai_tmp;
                        --$sisa_masa_tmp;
                        // if($sisa_masa_tmp > 0) {
                        //     --$sisa_masa_tmp;
                        // }

                        $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
                        $nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;
                        $akumulasi_penyusutan_berjalan_tmp += $penyusutan_per_tahun_tmp;

                        if($masa_terpakai_tmp > $masa_manfaat) {
                            $akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
                            $akumulasi_penyusutan_berjalan_tmp = $nilai_pengadaan_tmp;
                            $nilai_buku_tmp = 0;
                            $sisa_masa_tmp = 0;
                        } 
                    }
                }
                
                $akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;
                $jumlah_barang += $jumlah_barang_tmp;

                if(substr($value['kode_108'], 0, 5) == "1.3.1" || $kib_e == true) {
                    $akumulasi_penyusutan_tmp = 0;
                    $akumulasi_penyusutan_berjalan_tmp = 0;
                    $penyusutan_per_tahun_tmp = 0;
                    $nilai_buku_tmp = $nilai_pengadaan_tmp;
                    $akumulasi_penyusutan = 0;
                    $beban = 0;
                    $nilai_buku = $nilai_pengadaan_tmp;
                }

                //untuk generate penyusutan
                $susut = array();
                $susut['ak_penyusutan'] = $akumulasi_penyusutan_berjalan_tmp;
                $susut['nilai_buku'] = $nilai_buku_tmp;
                $update_kib = Kib::where('id_aset', $value["id_aset"])->update($susut);
                
                $aset_susut[$i++] =
                array(
                    "id_aset" => $value["id_aset"],
                    "akumulasi_penyusutan" => $akumulasi_penyusutan_tmp,
                    "nilai_buku" => $nilai_buku_tmp
                );
            } 
        }
        
        array_multisort(array_column($aset_susut, 'id_aset'), SORT_ASC, $aset_susut);
        $export = collect($aset_susut);

        return $export;
    }
}